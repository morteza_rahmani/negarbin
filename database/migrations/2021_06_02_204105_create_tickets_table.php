<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unsigned()->constrained('users')->cascadeOnDelete();
            $table->foreignId('parent_id')->nullable()->unsigned()->constrained('tickets')->cascadeOnDelete();
            $table->string('title');
            $table->text('message');
            $table->boolean('is_response')->default(0);
            $table->string('status')->nullable();
            $table->foreignId('to_user_id')->nullable()->unsigned()->constrained('users')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
