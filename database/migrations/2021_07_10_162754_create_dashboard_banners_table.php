<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard_banners', function (Blueprint $table) {
            $table->string('banner1_image')->nullable();
            $table->string('banner1_link')->nullable();
            $table->string('banner2_image')->nullable();
            $table->string('banner2_link')->nullable();
            $table->string('banner3_image')->nullable();
            $table->string('banner3_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard_banners');
    }
}
