<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->string('name');
            $table->string('slug');
            $table->string('thumbnail')->nullable();
            $table->text('content')->nullable();
            $table->text('summary')->nullable();
            $table->string('file');
            $table->integer('price');
            $table->string('format');
            $table->string('size');
            $table->tinyInteger('status')->default(1);
            $table->integer('views')->default(1);
            $table->integer('downloads')->default(0);
            $table->tinyInteger('score')->default(0);
            $table->integer('votes')->default(0);
            $table->integer('purchases')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
