<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->string('banner1_image')->nullable();
            $table->string('banner1_username')->nullable();
            $table->string('banner1_user_avatar')->nullable();
            $table->string('banner1_user_url')->nullable();
            $table->string('banner2_image')->nullable();
            $table->string('banner2_link')->nullable();
            $table->string('banner3_image')->nullable();
            $table->string('banner3_link')->nullable();
            $table->string('banner4_image')->nullable();
            $table->string('banner4_link')->nullable();
            $table->string('banner5_image')->nullable();
            $table->string('banner5_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
