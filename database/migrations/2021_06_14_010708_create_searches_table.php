<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function (Blueprint $table) {
            $table->string('keyword1')->nullable();
            $table->string('keyword2')->nullable();
            $table->string('keyword3')->nullable();
            $table->string('keyword4')->nullable();
            $table->string('keyword5')->nullable();
            $table->string('keyword6')->nullable();
            $table->string('keyword7')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searches');
    }
}
