<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->nullOnDelete();
            $table->string('title')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('thumbnail')->nullable();
            $table->string('thumbnail_alt')->nullable();
            $table->text('content');
            $table->text('summary')->nullable();
            $table->string('tags')->nullable();
            $table->string('keywords');
            $table->integer('views')->default(1);
            $table->tinyInteger('score')->default(0);
            $table->integer('votes')->default(0);
            $table->boolean('published')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
