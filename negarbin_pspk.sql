-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 28, 2021 at 01:16 PM
-- Server version: 5.7.34
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `negarbin_pspk`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail_alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` int(11) NOT NULL DEFAULT '1',
  `score` tinyint(4) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `title`, `name`, `slug`, `thumbnail`, `thumbnail_alt`, `content`, `summary`, `tags`, `keywords`, `views`, `score`, `votes`, `published`, `created_at`, `updated_at`) VALUES
(1, 1, 'نگاربین شروع به کار کرد', 'نگاربین شروع به کار کرد', 'started-negarbin', 'https://dl.negarbin.com/storage/photos/thumbnails/articles/Adding-custom-status-to-Dokan-order.png', 'نگاربین فروشگاه فایل های گرافیکی', '<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.', 'نگاربین, فایل گرافیکی', 'نگاربین, فایل گرافیکی', 1, 0, 0, 1, '2021-07-11 20:32:58', '2021-07-11 20:40:14');

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

CREATE TABLE `awards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `award_user`
--

CREATE TABLE `award_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `award_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner1_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner1_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner1_user_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner1_user_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner2_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner2_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner3_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner3_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner3_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner4_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner4_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner4_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner5_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner5_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner5_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner1_image`, `banner1_username`, `banner1_user_avatar`, `banner1_user_url`, `banner2_image`, `banner2_link`, `banner3_image`, `banner3_link`, `banner3_icon`, `banner4_image`, `banner4_link`, `banner4_icon`, `banner5_image`, `banner5_link`, `banner5_icon`) VALUES
('615c7f77bf6e6.jpg', 'alirezaf2b', '615c7f77f1430.jpg', 'f', '615c7f9f7b8c7.jpg', 'http://art8.ir', '615c7fb9a5e02.jpg', 'f', NULL, '616463395f896.jpg', 'f', 'icon-brush', '615c7fc98cb08.jpg', 'f', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(10, 9, 5, '2021-10-02 23:53:45', '2021-10-02 23:53:45');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depth` tinyint(4) NOT NULL DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `slug`, `icon`, `depth`, `priority`, `created_at`, `updated_at`) VALUES
(1, NULL, 'تصاویر استوک', 'stock', 'icon-camera2', 0, 2, '2021-07-09 22:29:40', '2021-07-09 22:29:40'),
(6, 1, 'حیوانات و طبیعت', 'animals', NULL, 1, NULL, '2021-07-16 22:44:04', '2021-07-16 22:44:45'),
(7, 1, 'دکوراسیون', 'interior', NULL, 1, NULL, '2021-07-16 22:44:57', '2021-07-16 23:11:37'),
(8, 1, 'بک گراند و تکسچر', 'background', NULL, 1, 5, '2021-07-16 22:45:14', '2021-07-16 22:45:14'),
(9, 1, 'نقشی و طراحی', 'illustration', NULL, 1, NULL, '2021-07-16 22:57:37', '2021-07-16 22:57:37'),
(10, 1, 'غذاها و میوه ها', 'food', NULL, 1, NULL, '2021-07-16 22:57:56', '2021-07-16 22:57:56'),
(11, 1, 'گل و گیاه', 'flower', NULL, 1, NULL, '2021-07-16 22:58:17', '2021-07-16 22:58:17'),
(12, 1, 'مشاغل', 'jobs', NULL, 1, 3, '2021-07-16 22:58:50', '2021-07-16 22:58:50'),
(13, 1, 'اشیا و المان ها', 'objects', NULL, 1, NULL, '2021-07-16 23:00:04', '2021-07-16 23:00:04'),
(14, 1, 'خودرو و حمل و نقل', 'transportation', NULL, 1, NULL, '2021-07-16 23:00:21', '2021-07-16 23:00:21'),
(15, 1, 'ورزشی', 'sport', NULL, 1, 6, '2021-07-16 23:00:43', '2021-07-16 23:00:43'),
(16, 1, 'تکنولوژی', 'technology', NULL, 1, NULL, '2021-07-16 23:01:03', '2021-07-16 23:01:03'),
(17, 1, 'انتزاعی', 'abstract', NULL, 1, 2, '2021-07-16 23:01:16', '2021-07-16 23:01:16'),
(18, 1, 'شهر کشور و ساختمان', 'city', NULL, 1, 7, '2021-07-16 23:01:30', '2021-08-21 17:54:15'),
(19, 1, 'مذهبی', 'religious', NULL, 1, 4, '2021-07-16 23:01:59', '2021-07-16 23:01:59'),
(21, 1, 'مردم', 'people', NULL, 1, 8, '2021-07-16 23:06:50', '2021-08-21 17:53:22'),
(22, 21, 'زن', 'woman', NULL, 2, 2, '2021-07-16 23:07:06', '2021-07-16 23:07:06'),
(23, 21, 'مرد', 'man', NULL, 2, 3, '2021-07-16 23:07:19', '2021-07-16 23:07:19'),
(24, 21, 'کودک', 'child', NULL, 2, 1, '2021-07-16 23:10:41', '2021-07-16 23:10:41'),
(25, 21, 'خانواده', 'family', NULL, 2, 4, '2021-07-16 23:11:04', '2021-07-16 23:11:04'),
(26, 1, 'متفرقه', 'other', NULL, 1, 6, '2021-07-16 23:11:27', '2021-08-21 17:54:42'),
(27, NULL, 'وکتور', 'وکتور', NULL, 0, 1, '2021-07-27 13:22:05', '2021-07-27 13:22:05'),
(28, 27, 'حیوانات و کاراکتر ها', 'حیوانات-و-کاراکتر-ها', NULL, 1, NULL, '2021-07-27 13:22:24', '2021-10-13 10:33:44'),
(29, 27, 'آبجکت ها و المان های وب', 'آبجکت-ها-و-المان-های-وب', NULL, 1, 0, '2021-07-27 13:22:49', '2021-10-13 10:34:52'),
(30, 27, 'اینفوگرافی', 'infography', NULL, 1, NULL, '2021-10-05 20:30:55', '2021-10-05 20:30:55'),
(31, 27, 'وکتورهای مناسبتی', 'وکتورهای-مناسبتی', NULL, 1, NULL, '2021-10-13 10:34:27', '2021-10-13 10:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`product_id`, `category_id`) VALUES
(2, 1),
(5, 1),
(7, 1),
(10, 1),
(11, 1),
(15, 1),
(15, 21),
(15, 23),
(9, 27),
(9, 28),
(9, 29),
(17, 1),
(17, 6),
(17, 7),
(17, 8),
(18, 8),
(18, 9),
(18, 10),
(18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `score` tinyint(4) DEFAULT NULL,
  `commentable_id` int(11) NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_id`, `user_id`, `name`, `email`, `content`, `status`, `score`, `commentable_id`, `commentable_type`, `created_at`, `updated_at`) VALUES
(2, NULL, 1, NULL, NULL, 'تصویر بسیار عالی', 1, 5, 2, 'App\\Models\\Product', '2021-07-09 23:34:45', '2021-07-09 23:34:45'),
(3, 2, 1, NULL, NULL, 'از نظر شما متشکریم', 1, NULL, 2, 'App\\Models\\Product', '2021-07-11 18:33:21', '2021-07-11 18:33:21'),
(6, NULL, 2, NULL, NULL, 'نظر درست کار میکنه ؟', 1, 5, 7, 'App\\Models\\Product', '2021-07-11 18:49:22', '2021-07-11 18:49:38'),
(7, NULL, 2, NULL, NULL, 'نظر کاربر بعد از تایید', 1, 5, 7, 'App\\Models\\Product', '2021-07-11 18:50:23', '2021-07-11 18:50:53'),
(8, NULL, 2, NULL, NULL, 'چک کردن میانگین امتیاز دهی', 1, 2, 7, 'App\\Models\\Product', '2021-07-11 18:52:11', '2021-07-11 23:24:07'),
(9, NULL, 1, NULL, NULL, 'خوب نبود', 1, 1, 7, 'App\\Models\\Product', '2021-07-12 13:55:20', '2021-07-12 13:55:20'),
(10, NULL, 1, NULL, NULL, 'اصلا خوب نبود', 1, 1, 7, 'App\\Models\\Product', '2021-07-12 13:56:24', '2021-07-12 13:56:24'),
(11, NULL, 1, NULL, NULL, 'اصلا اصلا خوب نبود', 1, 1, 7, 'App\\Models\\Product', '2021-07-12 13:56:38', '2021-07-12 13:56:38'),
(12, NULL, NULL, 'علیرضا', 'sdzxcvv@yahoo.com', 'خوب نبوددددددددد', 1, 1, 7, 'App\\Models\\Product', '2021-07-12 13:57:52', '2021-07-12 13:58:05'),
(13, NULL, 2, NULL, NULL, 'موکاپ خوبی بود', 1, 5, 9, 'App\\Models\\Product', '2021-07-14 15:30:47', '2021-07-14 15:30:53'),
(14, NULL, NULL, 'Teereebus', 'TramFoola@supmail.xyz', '<a href=http://cialiswwshop.com/>cialis generic tadalafil</a>', 2, NULL, 1, 'App\\Models\\Article', '2021-07-27 20:12:50', '2021-07-27 20:12:50'),
(15, NULL, NULL, 'Josephtib', 'aroutablure@mail.com', 'Certainly.', 2, NULL, 1, 'App\\Models\\Article', '2021-07-29 22:17:15', '2021-07-29 22:17:15'),
(16, NULL, NULL, 'skyreveryJoymn', 'malinoleg91@mail.ru', '[url=https://www.skyrevery.com/destinations/private-jet-saint-petersburg/]Saint-Petersburg Private Jet Charter [/url]        -  more information on our website [url=https://skyrevery.com]skyrevery.com[/url] \r\n[url=https://skyrevery.com/]Private jet rental[/url] at SkyRevery allows you to use such valuable resource as time most efficiently. \r\nYou are the one who decides where and when your private jet will fly. It is possible to organize and perform a flight between any two civil airports worldwide round the clock. In airports, private jet passengers use special VIP terminals where airport formalities are minimized, and all handling is really fast – you come just 30 minutes before  the estimated time of the departure of the rented private jet. \r\nWhen you need [url=https://skyrevery.com/]private jet charter[/url] now, we can organise your flight with departure in 3 hours from confirmation.', 2, NULL, 1, 'App\\Models\\Article', '2021-07-31 14:16:51', '2021-07-31 14:16:51'),
(17, NULL, NULL, 'DavidExery', 'nacilsanftute@mail.com', 'It agree, your idea is brilliant\r\n [url=https://gay0day.com/top-rated/]gay0day[/url]', 2, NULL, 1, 'App\\Models\\Article', '2021-08-02 22:30:01', '2021-08-02 22:30:01'),
(18, NULL, NULL, 'BradleyCax', 'parkjalibizppah@mail.com', 'Quite right! It seems to me it is excellent idea. I agree with you.', 2, NULL, 1, 'App\\Models\\Article', '2021-08-02 23:36:19', '2021-08-02 23:36:19'),
(19, NULL, NULL, 'JeffreyTip', 'tapmagassefo@mail.com', 'Let\'s return to a theme\r\n [url=https://gay-teens.cc]gay-teens.cc[/url]', 2, NULL, 1, 'App\\Models\\Article', '2021-08-03 04:45:43', '2021-08-03 04:45:43'),
(20, NULL, NULL, 'Robertmutle', 'roughralediffbral@mail.com', 'Yes, really. I agree with told all above.\r\n  [url=https://big-tits-tube.org]big-tits-tube[/url]', 2, NULL, 1, 'App\\Models\\Article', '2021-08-03 09:57:12', '2021-08-03 09:57:12'),
(21, NULL, NULL, 'CharlesBam', 'dantlongbullcentbobs@mail.com', 'I apologise, but, in my opinion, you are not right. I can prove it. Write to me in PM, we will talk.', 2, NULL, 1, 'App\\Models\\Article', '2021-08-03 12:34:22', '2021-08-03 12:34:22'),
(22, NULL, NULL, 'WilliamTum', 'zuptoilozere@mail.com', 'I congratulate, the excellent answer.', 2, NULL, 1, 'App\\Models\\Article', '2021-08-03 20:04:10', '2021-08-03 20:04:10'),
(23, NULL, NULL, 'DanielTut', 'tebaterpdurol@mail.com', 'Bravo, you were visited with a remarkable idea\r\n  [url=https://zeenite.com/videos/26149/mompov-judi-1/]mompov[/url]', 2, NULL, 1, 'App\\Models\\Article', '2021-08-04 07:45:22', '2021-08-04 07:45:22'),
(24, NULL, NULL, 'Smithvox', 'smithAridAjn@nettirautakauppa.com', 'Sputnik V vaccination has begun in Slovakia. The supplying of the Russian vaccine to the innate settle on was accompanied away a civic spot and led to the forgoing of Prime Woman of the cloth Igor Matovich and a rearrangement of the government. As a culminate, the state received the Russian vaccine, in hostility of the low-down that neither the European regulator nor the WHO has until instanter approved it. \r\nIn neighboring Hungary, which approved the use of Sputnik in February as the anything else in Europe, more than 50% of the grown up citizens has already been vaccinated; in Russia - a hardly any more than 10%. In Slovakia, five thousand people signed up for the Sputnik vaccination. \r\nI\'m sorry, but I think you\'re making a mistake. I can prove it. Write to me in PM, we will talk.. You can present another article on this area of study at this tie-up  [url=\"https://xujan.ergfujujt.site\"]https://stockluckydraw.qasdf.site[/url]', 2, NULL, 1, 'App\\Models\\Article', '2021-09-01 10:56:59', '2021-09-01 10:56:59'),
(25, NULL, NULL, 'Dannyamake', 'temptest565914929@gmail.com', 'BTC may be the latest or last chance to get rich in this era. It will reach $200000 next year or the next year. \r\n \r\nThink about only $2 a few years ago. Come to the world\'s largest and safest virtual currency exchange to reduce the handling fee. Don\'t miss the most important opportunity in life \r\n \r\nhttps://hi.switchy.io/5dyf', 2, NULL, 1, 'App\\Models\\Article', '2021-09-09 12:42:01', '2021-09-09 12:42:01'),
(26, NULL, NULL, 'skyreveryJoymn', 'malinoleg91@mail.ru', '[url=https://skyrevery.ru/helicopters/bell_429/]Аренда частного вертолета Bell 429 – SkyRevery[/url]        -  подробнее на нашем сайте [url=https://skyrevery.ru]skyrevery.ru[/url] \r\n[url=https://skyrevery.ru/]Аренда частного самолета[/url] с экипажем в компании SkyRevery – это выбор тех, кто ценит свое время и живет по своему расписанию! \r\nАренда частного самолета помогает экономить самый важный ресурс – время. Арендовав частный самолет, именно Вы решаете, когда и куда полетите. Для выполнения чартерных рейсов мы предлагаем частные самолеты иностранного производства, гарантирующие высокий уровень комфорта и безопасности полета. Внимательные бортпроводники и высокопрофессиональные пилоты сделают Ваш полет максимально приятным и удобным. \r\nКогда Вам нужна [url=https://skyrevery.ru/]аренда самолета[/url] срочно, мы можем организовать для Вас вылет по готовности от 3 часов с момента подтверждения.', 2, NULL, 1, 'App\\Models\\Article', '2021-09-28 11:42:46', '2021-09-28 11:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `content_images`
--

CREATE TABLE `content_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fileName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `used` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_images`
--

INSERT INTO `content_images` (`id`, `fileName`, `url`, `used`, `created_at`, `updated_at`) VALUES
(1, 'u1-61681bfac002c-0htE.jpg', 'https://dl.negarbin.com/storage/photos/content/products/u1-61681bfac002c-0htE.jpg', '0', '2021-10-14 15:31:01', '2021-10-14 15:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_banners`
--

CREATE TABLE `dashboard_banners` (
  `banner1_image` varchar(255) DEFAULT NULL,
  `banner1_link` varchar(255) DEFAULT NULL,
  `banner2_image` varchar(255) DEFAULT NULL,
  `banner2_link` varchar(255) DEFAULT NULL,
  `banner3_image` varchar(255) DEFAULT NULL,
  `banner3_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dashboard_banners`
--

INSERT INTO `dashboard_banners` (`banner1_image`, `banner1_link`, `banner2_image`, `banner2_link`, `banner3_image`, `banner3_link`) VALUES
('60e9e060b58e0.jpg', '1', '60e9e0631b168.jpg', '2', '60e9e0647c762.jpg', '3');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `user_id`, `amount`, `email`, `created_at`, `updated_at`) VALUES
(1, 1, '0', 'info@negarbin.com', '2021-07-11 22:49:18', '2021-07-11 22:49:18'),
(2, 3, '10000', 'sporal7394@yahoo.com', '2021-07-14 13:44:40', '2021-07-14 13:44:40'),
(3, 4, '10000', 'dsdsd@gmail.com', '2021-07-14 14:56:03', '2021-07-14 14:56:03'),
(4, 4, '10000', 'dsdsd@gmail.com', '2021-07-14 14:56:13', '2021-07-14 14:56:13'),
(5, 4, '10000', 'dsdsd@gmail.com', '2021-07-14 14:56:18', '2021-07-14 14:56:18'),
(6, 4, '9000', 'fsddff@yahoo.com', '2021-07-14 19:58:35', '2021-07-14 19:58:35'),
(7, 2, '15000', 'sporal7394@yahoo.com', '2021-07-15 19:51:44', '2021-07-15 19:51:44'),
(8, 3, '10000', 'sdfsdf@yahoo.com', '2021-07-26 01:06:41', '2021-07-26 01:06:41'),
(9, 3, '9000', 'asdsdfvxcv@yahoo.com', '2021-08-22 14:57:28', '2021-08-22 14:57:28'),
(10, 5, '10000', 'test@gmail.com', '2021-10-02 23:54:02', '2021-10-02 23:54:02'),
(11, 5, '10000', 'test@gmail.com', '2021-10-02 23:55:32', '2021-10-02 23:55:32'),
(12, 5, '10000', 'test@gmail.com', '2021-10-04 11:27:33', '2021-10-04 11:27:33'),
(13, 5, '10000', 'test@gmail.com', '2021-10-04 12:27:38', '2021-10-04 12:27:38');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE `invoice_product` (
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`product_id`, `invoice_id`, `product_price`, `product_name`) VALUES
(7, 1, '0', 'بادکنک صورتی با جای متن سفید'),
(9, 2, '10000', 'موک آپ قهوه'),
(9, 3, '10000', 'موک آپ قهوه'),
(9, 4, '10000', 'موک آپ قهوه'),
(9, 5, '10000', 'موک آپ قهوه'),
(11, 6, '9000', 'موک آپ کارت ویزیت'),
(13, 7, '5000', 'قالب اینستا 2'),
(12, 7, '10000', 'قالب اینستا'),
(15, 8, '10000', 'مردی ایستاد با ریش های رنگ سیاه'),
(11, 9, '9000', 'موک آپ کارت ویزیت'),
(9, 10, '10000', 'موک آپ قهوه'),
(9, 11, '10000', 'موک آپ قهوه'),
(9, 12, '10000', 'موک آپ قهوه'),
(9, 13, '10000', 'موک آپ قهوه');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(6, 'default', '{\"uuid\":\"7ef0301e-a2b6-425c-a9ff-7bf5b4450d72\",\"displayName\":\"App\\\\Jobs\\\\SendMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMail\",\"command\":\"O:17:\\\"App\\\\Jobs\\\\SendMail\\\":12:{s:8:\\\"\\u0000*\\u0000email\\\";s:16:\\\"fsddff@yahoo.com\\\";s:10:\\\"\\u0000*\\u0000payment\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:18:\\\"App\\\\Models\\\\Payment\\\";s:2:\\\"id\\\";i:6;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"invoice\\\";i:1;s:16:\\\"invoice.products\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1626276521, 1626276521),
(7, 'default', '{\"uuid\":\"df64c699-7960-478e-a28a-a8c58f78f24d\",\"displayName\":\"App\\\\Jobs\\\\SendMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMail\",\"command\":\"O:17:\\\"App\\\\Jobs\\\\SendMail\\\":12:{s:8:\\\"\\u0000*\\u0000email\\\";s:20:\\\"sporal7394@yahoo.com\\\";s:10:\\\"\\u0000*\\u0000payment\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:18:\\\"App\\\\Models\\\\Payment\\\";s:2:\\\"id\\\";i:7;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"invoice\\\";i:1;s:16:\\\"invoice.products\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1626362509, 1626362509),
(8, 'default', '{\"uuid\":\"aea62725-54a5-4441-94c9-c9c2ca876536\",\"displayName\":\"App\\\\Jobs\\\\SendMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMail\",\"command\":\"O:17:\\\"App\\\\Jobs\\\\SendMail\\\":12:{s:8:\\\"\\u0000*\\u0000email\\\";s:16:\\\"sdfsdf@yahoo.com\\\";s:10:\\\"\\u0000*\\u0000payment\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:18:\\\"App\\\\Models\\\\Payment\\\";s:2:\\\"id\\\";i:8;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"invoice\\\";i:1;s:16:\\\"invoice.products\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1627245414, 1627245414),
(9, 'default', '{\"uuid\":\"9d08567c-fda1-4cca-8b6e-e5ae228ccea1\",\"displayName\":\"App\\\\Jobs\\\\SendMail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"failOnTimeout\":false,\"backoff\":null,\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendMail\",\"command\":\"O:17:\\\"App\\\\Jobs\\\\SendMail\\\":12:{s:8:\\\"\\u0000*\\u0000email\\\";s:20:\\\"asdsdfvxcv@yahoo.com\\\";s:10:\\\"\\u0000*\\u0000payment\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:18:\\\"App\\\\Models\\\\Payment\\\";s:2:\\\"id\\\";i:9;s:9:\\\"relations\\\";a:2:{i:0;s:7:\\\"invoice\\\";i:1;s:16:\\\"invoice.products\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:11:\\\"afterCommit\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1629628053, 1629628053);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_05_18_153503_create_tokens_table', 1),
(5, '2021_05_23_211038_create_articles_table', 1),
(6, '2021_05_23_212749_create_categories_table', 1),
(7, '2021_05_24_123956_create_products_table', 1),
(8, '2021_05_27_210956_create_comments_table', 1),
(9, '2021_06_02_204105_create_tickets_table', 1),
(10, '2021_06_09_172718_create_category_product_table', 1),
(11, '2021_06_13_003922_create_banners_table', 1),
(12, '2021_06_14_010708_create_searches_table', 1),
(13, '2021_06_15_235302_create_pages_table', 1),
(14, '2021_06_16_020130_create_awards_table', 1),
(15, '2021_06_17_140803_create_award_user_table', 1),
(16, '2021_06_21_032613_create_invoices_table', 1),
(17, '2021_06_21_032831_create_payments_table', 1),
(18, '2021_06_21_034111_create_carts_table', 1),
(19, '2021_06_22_201244_create_invoice_product_table', 1),
(20, '2021_06_23_133205_create_product_user_table', 1),
(21, '2021_06_26_173331_create_withdraws_table', 1),
(22, '2021_06_30_183928_create_tags_table', 1),
(23, '2021_06_30_184020_create_tag_product_table', 1),
(24, '2021_06_30_193834_create_settings_table', 1),
(25, '2021_07_03_020458_create_jobs_table', 1),
(26, '2021_07_05_205558_users_verify', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `name`, `slug`, `content`, `summary`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'کسب درآمد در میهن طرح', 'کسب درآمد در میهن طرح', 'کسب-درآمد-در-میهن-طرح', '<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت', 'کسب درآمد', '2021-07-11 20:15:17', '2021-07-11 20:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `transaction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gateway` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jdate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `invoice_id`, `transaction`, `receipt`, `status`, `message`, `gateway`, `jdate`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, 0, NULL, NULL, '1400-04-20', '2021-07-11 22:49:18', '2021-07-11 22:49:18'),
(2, 2, '000000000000000000000000000000549674', '12345678', 2, NULL, NULL, '1400-04-23', '2021-07-14 13:44:40', '2021-07-14 13:44:55'),
(3, 3, '000000000000000000000000000000549766', NULL, 0, NULL, NULL, '1400-04-23', '2021-07-14 14:56:04', '2021-07-14 14:56:04'),
(4, 4, '000000000000000000000000000000549767', NULL, 0, NULL, NULL, '1400-04-23', '2021-07-14 14:56:13', '2021-07-14 14:56:13'),
(5, 5, '000000000000000000000000000000549768', '12345678', 2, NULL, NULL, '1400-04-23', '2021-07-14 14:56:18', '2021-07-14 14:56:21'),
(6, 6, '000000000000000000000000000000550123', '12345678', 2, NULL, NULL, '1400-04-23', '2021-07-14 19:58:35', '2021-07-14 19:58:41'),
(7, 7, '000000000000000000000000000000551066', '12345678', 2, NULL, NULL, '1400-04-24', '2021-07-15 19:51:44', '2021-07-15 19:51:49'),
(8, 8, '000000000000000000000000000000562368', '12345678', 2, NULL, NULL, '1400-05-04', '2021-07-26 01:06:42', '2021-07-26 01:06:54'),
(9, 9, '000000000000000000000000000000589797', '12345678', 2, NULL, NULL, '1400-05-31', '2021-08-22 14:57:28', '2021-08-22 14:57:32'),
(10, 10, '000000000000000000000000000000638457', NULL, 0, NULL, NULL, '1400-07-10', '2021-10-02 23:54:02', '2021-10-02 23:54:04'),
(11, 11, NULL, NULL, 0, NULL, NULL, '1400-07-10', '2021-10-02 23:55:32', '2021-10-02 23:55:32'),
(12, 12, NULL, NULL, 0, NULL, NULL, '1400-07-12', '2021-10-04 11:27:34', '2021-10-04 11:27:34'),
(13, 13, 'A00000000000000000000000000286367430', NULL, 0, NULL, NULL, '1400-07-12', '2021-10-04 12:27:38', '2021-10-04 12:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '1',
  `downloads` int(11) NOT NULL DEFAULT '0',
  `score` tinyint(4) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `purchases` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `slug`, `thumbnail`, `content`, `summary`, `file`, `price`, `format`, `size`, `status`, `views`, `downloads`, `score`, `votes`, `purchases`, `created_at`, `updated_at`) VALUES
(2, 2, 'تصویر استوک پاستا درون ظرف روی میز', 'تصویر-استوک-پاستا-درون-ظرف-روی-میز', 'u2-t60e88e16662be.jpg', '<p>تصویر استوک پاستا درون بشقات روی میز به همراه تزئین گوجه و سبزی</p>', 'تصویر استوک پاستا درون بشقات روی میز به همراه تزئین گوجه و سبزی', 'u2-p60e88e17c80bf.jpg', 0, 'jpg', '1914954', 2, 125, 45, 5, 1, 0, '2021-07-09 22:27:43', '2021-10-23 01:23:16'),
(5, 1, 'موک آپ ui موبایل و اپلیکیشن', 'موک-آپ-ui-موبایل-و-اپلیکیشن', 'u1-t60e8abeb247aa.jpg', '<p>موک آپ )mock up( ui موبایل و اپلیکیشن</p>', 'موک آپ )mock up( ui موبایل و اپلیکیشن', 'u1-p60e8abeeeb0f3.zip', 0, 'psd', '71104958', 2, 78, 42, 0, 0, 0, '2021-07-10 00:35:12', '2021-10-23 01:23:22'),
(7, 2, 'بادکنک صورتی با جای متن سفید', 'بادکنک-صورتی-با-جای-متن-سفید', 'u2-t60eafc9d7ca33.jpg', '<p>بادکنک صورتی با جای متن سفید</p>', 'بادکنک صورتی با جای متن سفید', 'u2-p60eafc9e43cff.jpg', 0, 'jpg', '550334', 2, 103, 54, 2, 7, 0, '2021-07-11 18:43:50', '2021-10-23 01:23:29'),
(9, 2, 'موک آپ قهوه', 'موک-آپ-قهوه', 'u2-t60eea933a6331.jpg', '<p>موک آپ قهوه</p>', NULL, 'u2-p60eea93452071.zip', 10000, 'psd', '19289283', 2, 89, 15, 5, 1, 2, '2021-07-14 13:37:01', '2021-10-23 01:23:37'),
(10, 5, 'نامه', 'نامه', 'u5-t60eecfdf17186.jpg', '<p>asdfa &nbsp;h rth rth erf errf rh tyju rutyert et&nbsp;</p>', 'sdasdfasdf', 'u5-p60eecfdf96b26.zip', 8500, 'zip', '18386139', 3, 1, 1, 0, 0, 0, '2021-07-14 16:22:00', '2021-07-14 16:29:51'),
(11, 2, 'موک آپ کارت ویزیت', 'موک-آپ-کارت-ویزیت', 'u2-t60eef1d27304b.jpg', NULL, NULL, 'u2-p60eef1d2ec61e.zip', 9000, 'psd', '18386139', 2, 67, 2, 0, 0, 2, '2021-07-14 18:46:51', '2021-10-23 01:22:26'),
(12, 4, 'قالب اینستا', 'قالب-اینستا', 'u4-t60f044b59c2bb.jpg', '<p>چرت و پرت</p>', NULL, 'u4-p60f044b666c8a.jpg', 10000, 'jpg', '48255', 2, 63, 2, 0, 0, 1, '2021-07-15 18:52:46', '2021-10-23 01:23:01'),
(13, 4, 'قالب اینستا 2', 'قالب-اینستا-2', 'u4-t60f044eb37325.jpg', '<p>سشیشی</p>', 'سیشیشی', 'u4-p60f044ebb6574.jpg', 5000, 'JPG', '50278', 2, 71, 3, 0, 0, 1, '2021-07-15 18:53:39', '2021-10-23 01:23:05'),
(15, 2, 'مردی ایستاد با ریش های رنگ سیاه', 'مرد-ایستاده', 'u2-t60fd70adcdf0b.jpg', '<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.', 'u2-p60fd70aeb01c5.jpg', 10000, 'jpg', '121548', 2, 49, 2, 0, 0, 1, '2021-07-25 18:39:50', '2021-10-23 01:23:08'),
(17, 1, 'موک آپ جعبه', 'موک-آپ-جعبه', 'u1-t615c6b146c80c.jpg', '<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربرده<br>&nbsp;</p><figure class=\"image\"><img src=\"https://dl.negarbin.com/storage/photos/content/products/u1-615c6aab73cfa-nN7D.jpg\"></figure><p>متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارس</p>', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربرد', 'u1-p615c6b1506ffc.jpg', 3000, 'psd', '22416', 2, 14, 0, 0, 0, 0, '2021-10-05 18:41:17', '2021-10-23 01:23:12'),
(18, 1, 'هالووین', 'هالووین', 'u1-t615c81fe0851b.jpg', '<p>شسی سیبخسیهبت تسیب حسیهخبتحخت سیب سیحبختسیب</p><figure class=\"image\"><img src=\"https://dl.negarbin.com/storage/photos/content/products/u1-615c81e41ae46-g4Xf.jpg\"></figure>', 'سیسشیشسی', 'u1-p615c81fedf67e.jpg', 2000, 'psd', '72295', 1, 1, 0, 0, 0, 0, '2021-10-05 20:19:02', '2021-10-05 20:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `product_user`
--

CREATE TABLE `product_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_user`
--

INSERT INTO `product_user` (`user_id`, `product_id`, `product_price`, `product_name`) VALUES
(3, 9, '10000', 'موک آپ قهوه'),
(4, 9, '10000', 'موک آپ قهوه'),
(4, 11, '9000', 'موک آپ کارت ویزیت'),
(2, 13, '5000', 'قالب اینستا 2'),
(2, 12, '10000', 'قالب اینستا'),
(3, 15, '10000', 'مردی ایستاد با ریش های رنگ سیاه'),
(3, 11, '9000', 'موک آپ کارت ویزیت');

-- --------------------------------------------------------

--
-- Table structure for table `searches`
--

CREATE TABLE `searches` (
  `keyword1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `searches`
--

INSERT INTO `searches` (`keyword1`, `keyword2`, `keyword3`, `keyword4`, `keyword5`, `keyword6`, `keyword7`) VALUES
('کرونا', 'ویروس', 'عید نوروز', 'منتخب', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `commission` int(11) NOT NULL,
  `min_settlement_amount` int(11) NOT NULL,
  `tag_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`commission`, `min_settlement_amount`, `tag_id`) VALUES
(20, 10000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'نیک', '2021-07-09 23:47:09', '2021-07-09 23:47:09'),
(2, 'موک آپ', '2021-07-10 00:35:13', '2021-07-10 00:35:13'),
(3, 'mock up', '2021-07-10 00:35:13', '2021-07-10 00:35:13'),
(4, 'موبایل', '2021-07-10 00:35:13', '2021-07-10 00:35:13'),
(5, 'اپلیکیشن', '2021-07-10 00:35:13', '2021-07-10 00:35:13'),
(6, 'جوکر', '2021-07-10 01:20:49', '2021-07-10 01:20:49'),
(7, 'بادکنک', '2021-07-11 18:43:50', '2021-07-11 18:43:50'),
(8, 'صورتی', '2021-07-11 18:43:50', '2021-07-11 18:43:50'),
(9, 'بک گراند', '2021-07-11 18:55:42', '2021-07-11 18:55:42'),
(10, 'background', '2021-07-11 18:55:42', '2021-07-11 18:55:42'),
(11, 'قهوه', '2021-07-14 13:37:01', '2021-07-14 13:37:01'),
(12, 'نامه', '2021-07-14 16:22:00', '2021-07-14 16:22:00'),
(13, 'AXA', '2021-07-15 19:53:44', '2021-07-15 19:53:44'),
(14, 'مرد', '2021-07-25 18:39:51', '2021-07-25 18:39:51'),
(15, 'خانواده', '2021-07-25 18:39:51', '2021-07-25 18:39:51'),
(16, 'man', '2021-07-25 18:39:51', '2021-07-25 18:39:51'),
(17, 'مردم', '2021-07-25 18:39:51', '2021-07-25 18:39:51'),
(18, 'جعبه', '2021-08-21 00:14:51', '2021-08-21 00:14:51'),
(19, 'سلام', '2021-08-21 00:14:51', '2021-08-21 00:14:51'),
(20, 'موک', '2021-08-21 00:14:51', '2021-08-21 00:14:51'),
(21, 'آپ', '2021-08-21 00:14:51', '2021-08-21 00:14:51'),
(22, 'هالووین', '2021-10-05 20:19:02', '2021-10-05 20:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `tag_product`
--

CREATE TABLE `tag_product` (
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tag_product`
--

INSERT INTO `tag_product` (`tag_id`, `product_id`) VALUES
(2, 5),
(3, 5),
(4, 5),
(5, 5),
(7, 7),
(8, 7),
(11, 9),
(2, 9),
(12, 10),
(2, 11),
(3, 11),
(14, 15),
(15, 15),
(16, 15),
(17, 15),
(18, 17),
(2, 17),
(22, 18);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_response` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `code`, `user_id`, `used`, `created_at`, `updated_at`) VALUES
(1, '98262', 2, 1, '2021-07-15 18:23:12', '2021-07-15 18:23:37'),
(2, '66914', 3, 1, '2021-08-21 17:38:24', '2021-08-21 17:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IBAN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `phone_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_revenue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `outstanding` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `phone`, `avatar`, `IBAN`, `email_verified_at`, `phone_verified_at`, `password`, `total_revenue`, `outstanding`, `is_admin`, `remember_token`, `created_at`, `updated_at`, `is_email_verified`) VALUES
(1, 'ادمین', NULL, 'admin', 'info@negarbin.com', '09010856426', 'u1-a615c773c8853b.jpg', 'IR', '2021-07-15 14:53:10', '2021-07-09 16:58:19', '$2y$10$AVB0atxB1XO/5jneuzQH6.AK06eMNHpd9owBtPX.0FAUuU15jfhw2', '0', '0', 1, 'ctFd1xqP4STOUiIEdyCrVo4VMKG9IS9LHZC8AY1xnt1zZ0XhPayYj4RE3Zjp', '2021-07-09 16:58:19', '2021-10-05 19:33:08', 1),
(2, 'علیرضا', 'محمدی', 'user-60e86ea840dc3', 'sporal7394@yahoo.com', '09195760039', NULL, 'IR', '2021-07-15 18:37:21', '2021-07-09 20:13:36', '$2y$10$1rk7A0cr1GCWUcI2D1Vl0uS0tH7a3wldYpni93LvyDP38UVYZk226', '38400', '0', 0, 'iMx2Rz4FM3gpg1C6Ve8ztXUnEybuAtKByBIUcZllg3BdHYgHvnu2qnW2Aoik', '2021-07-09 20:13:36', '2021-09-19 21:33:22', 1),
(3, 'کاربر 60eeaa1df178c', NULL, 'user-60eeaa1df178e', NULL, '09217191338', NULL, NULL, NULL, '2021-07-14 13:40:54', '$2y$10$117RvJiYZER/Gph0cqwaJuM5uJygEqIIXrLgTfpZRDQH677og2rvC', '0', '0', 0, '1ghdQH30Zaf5CJT2KFGs9rVf3WBKehLD8mYXWDN68k9ezEOQSC3ocDgGPHHW', '2021-07-14 13:40:54', '2021-08-21 17:38:56', 0),
(4, 'کاربر 60eebb608fc16', NULL, 'user-60eebb608fc18', NULL, '09218764484', NULL, NULL, NULL, '2021-07-14 14:54:32', '$2y$10$o0irtFrjJHA8ke8YXWXopO3sD3ZbPeJbz4W01SsID.cBaZC6yr9cy', '12000', '12000', 0, '5yq2xESEdq0qjidGKQYnMnMEQnG5Vl77gXbVfsXOnm1DJMuq85jzPnsjp0bR', '2021-07-14 14:54:32', '2021-07-15 19:51:49', 0),
(5, 'simin', NULL, 'simin', NULL, '09010856420', NULL, NULL, NULL, NULL, '$2y$10$AVB0atxB1XO/5jneuzQH6.AK06eMNHpd9owBtPX.0FAUuU15jfhw2', '0', '0', 0, '7TNmJXMyF79a7Pu7BOMOOl1BxSmA7KLF0Cr1Q31AlbgSPXBYgnAgG4uSo25m', '2021-07-14 00:00:00', '2021-07-14 00:00:00', 0),
(6, 'کاربر 610ecadc09266', NULL, 'user-610ecadc09269', NULL, '09330447752', NULL, NULL, NULL, '2021-08-07 22:33:08', '$2y$10$63PX2kL2k/Cd0ZlPOkvORe/MlcchztMBOrbC2linLCGYh9sn8WMzq', '0', '0', 0, NULL, '2021-08-07 22:33:08', '2021-08-07 22:33:08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_verify`
--

CREATE TABLE `users_verify` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE `withdraws` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `withdraws`
--

INSERT INTO `withdraws` (`id`, `user_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '16000', 3, '2021-07-14 15:25:35', '2021-07-14 15:30:24'),
(2, 2, '22400', 3, '2021-08-22 14:58:22', '2021-08-22 14:59:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_user_id_foreign` (`user_id`);

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `award_user`
--
ALTER TABLE `award_user`
  ADD KEY `award_user_user_id_foreign` (`user_id`),
  ADD KEY `award_user_award_id_foreign` (`award_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD KEY `category_product_product_id_foreign` (`product_id`),
  ADD KEY `category_product_category_id_foreign` (`category_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_parent_id_foreign` (`parent_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `content_images`
--
ALTER TABLE `content_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_user_id_foreign` (`user_id`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD KEY `invoice_product_product_id_foreign` (`product_id`),
  ADD KEY `invoice_product_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Indexes for table `product_user`
--
ALTER TABLE `product_user`
  ADD KEY `product_user_user_id_foreign` (`user_id`),
  ADD KEY `product_user_product_id_foreign` (`product_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD KEY `settings_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_product`
--
ALTER TABLE `tag_product`
  ADD KEY `tag_product_tag_id_foreign` (`tag_id`),
  ADD KEY `tag_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tickets_user_id_foreign` (`user_id`),
  ADD KEY `tickets_parent_id_foreign` (`parent_id`),
  ADD KEY `tickets_to_user_id_foreign` (`to_user_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_verify`
--
ALTER TABLE `users_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD PRIMARY KEY (`id`),
  ADD KEY `withdraws_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `content_images`
--
ALTER TABLE `content_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users_verify`
--
ALTER TABLE `users_verify`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `withdraws`
--
ALTER TABLE `withdraws`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `award_user`
--
ALTER TABLE `award_user`
  ADD CONSTRAINT `award_user_award_id_foreign` FOREIGN KEY (`award_id`) REFERENCES `awards` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `award_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD CONSTRAINT `invoice_product_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoice_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_user`
--
ALTER TABLE `product_user`
  ADD CONSTRAINT `product_user_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `product_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `settings`
--
ALTER TABLE `settings`
  ADD CONSTRAINT `settings_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tag_product`
--
ALTER TABLE `tag_product`
  ADD CONSTRAINT `tag_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tag_product_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_to_user_id_foreign` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tickets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `withdraws`
--
ALTER TABLE `withdraws`
  ADD CONSTRAINT `withdraws_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
