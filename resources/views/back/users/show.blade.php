@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">مشخصات کاربر</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>مشخصات کاربر "{{$user->username}}"</h4>
                </div>
                <div class="widget-body">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th class="text-primary">نام و نام خانوادگی</th>
                            <td>{{$user->full_name}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">نام کاربری</th>
                            <td>{{$user->username}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">شماره موبایل</th>
                            <td>{{$user->phone}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">آدرس ایمیل</th>
                            <td>{{$user->email}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">آواتار</th>
                            <td><a href="{{$user->adjusted_avatar}}">{{$user->adjusted_avatar}}</a></td>
                        </tr>
                        <tr>
                            <th class="text-primary">شماره شبا</th>
                            <td>{{$user->IBAN}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">استان</th>
                            <td>{{$user->state}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">شهر</th>
                            <td>{{$user->city}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">تعداد محصولات</th>
                            <td>{{number_format($user->products()->where('status', 2)->count())}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">درآمد کل</th>
                            <td>{{number_format($user->total_revenue)}} تومان</td>
                        </tr>
                        <tr>
                            <th class="text-primary">تسویه نشده</th>
                            <td>{{number_format($user->outstanding)}} تومان</td>
                        </tr>
                        <tr>
                            <th class="text-primary">تاریخ عضویت</th>
                            <td>{{jdate($user->created_at)->format('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <th class="text-primary">جایزه ها</th>
                            <th>
                                @foreach($user->awards as $award)
                                    <img src="{{env('STORAGE_ADDRESS').env('AWARDS').$award->icon}}" alt="$award->icon"
                                         width="50" height="50" class="d-inline-block">
                                @endforeach
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
