@extends('back.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/datatables/datatables.min.css')}}">
    <style>
        .ticket .avatar {
            width: 50px;
            height: 50px;
            object-fit: cover;
            border-radius: 50%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">کاربران</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>لیست همه کاربران</h4>
                </div>
                <div class="widget-body">
                    <div class="ticket row">
                        <div class="table-responsive">
                            <table id="sorting-table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>آی دی</th>
                                    <th>مشخصات کاربر</th>
                                    <th>نام کاربری</th>
                                    <th>تعداد محصولات</th>
                                    <th>تعداد فروش</th>
                                    <th>مبلغ فروش</th>
                                    <th>تاریخ عضویت</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/back/vendors/js/calendar/locale/fa.js')}}"></script>
    <script src="{{asset('assets/back/js/persianDatepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sorting-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.users.datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id', orderable: true, searchable: true},
                    {data: 'details', name: 'details'},
                    {data: 'username', name: 'username'},
                    {data: 'products', name: 'products'},
                    {data: 'sales', name: 'sales'},
                    {data: 'total_revenue', name: 'total_revenue'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: true, searchable: true},
                ],
                language: {
                    "emptyTable": "هیچ داده ای یافت نشد",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ ورودی",
                    "infoEmpty": "نمایش 0 تا 0 از 0 ورودی",
                    "infoFiltered": "(فیلتر شده از _MAX_ مجموعه ورودی)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "نمایش _MENU_ ورودی",
                    "loadingRecords": "در حال بارگذاری...",
                    "processing": "در حال پردازش...",
                    "search": "جستجو: ",
                    "zeroRecords": "داده ای یافت نشد",
                    "paginate": {
                        "first": "اولین",
                        "last": "آخرین",
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                }
            });
            $('#sorting-table_filter input').attr('placeholder', 'جستجو در همه ستون ها');
        });
    </script>
@endsection
