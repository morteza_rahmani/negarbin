@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">جایزه های کاربر</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>جایزه های کاربر "{{$user->username}}"</h4>
                </div>
                <div class="widget-body">
                    <form action="{{route('admin.users.updateAwards', $user->id)}}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group row d-flex">
                            <label class="col-lg-2 form-control-label">
                                جایزه ها
                            </label>
                            <div class="col-lg-8">
                                <ul>
                                    @foreach($awards as $award)
                                        <li>
                                            <div class="mb-3">
                                                <div class="styled-checkbox">
                                                    <input type="checkbox" name="awards[]" id="award{{$award->id}}"
                                                           class="form-control" value="{{$award->id}}"
                                                        {{in_array($award->id, $user->awards()->pluck('id')->toArray()) ? 'checked' : ''}}>
                                                    <label for="award{{$award->id}}">{{$award->name}}</label>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-gradient-01">ذخیره تغییرات</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
