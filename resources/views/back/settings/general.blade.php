@extends('back.layouts.app')
@section('styles')
    {{--    <link rel="stylesheet" href="{{asset('assets/back/vendors/css/select2/select2.min.css')}}">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendors/css/select2/select2-bootstrap.min.css')}}">
    <style>
        .select2.select2-container .selection {
            width: 100%;
        }
        .select2-container--bootstrap .select2-selection__clear {
            float: left !important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تنظیمات</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>تنظیمات اصلی</h4>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form class="" action="{{route('admin.settings.general.store')}}" method="post">
                                @csrf
                                <div class="form-group row d-flex align-items-center mb-5">
                                    <label for="commission" class="col-lg-2 form-control-label">درصد کمیسیون</label>
                                    <div class="col-lg-8">
                                        <input type="number" name="commission" id="commission"
                                               value="{{$settings->commission}}" placeholder="درصد"
                                               class="form-control @error('commission'){{'is-invalid'}}@enderror">
                                        @error('commission')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center mb-5">
                                    <label for="min_settlement_amount" class="col-lg-2 form-control-label">
                                        حداقل مبلغ قابل تسویه توسط کاربران
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="number" name="min_settlement_amount" id="min_settlement_amount"
                                               value="{{$settings->min_settlement_amount}}" placeholder="تومان"
                                               class="form-control @error('min_settlement_amount'){{'is-invalid'}}@enderror">
                                        @error('min_settlement_amount')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center mb-5">
                                    <label for="tag" class="col-lg-2 form-control-label">
                                        برچسب محصولات منتخب (صفحه اصلی)
                                    </label>
                                    <div class="col-lg-8">
                                        <select class="w-100 form-control @error('tag_id'){{'is-invalid'}}@enderror"
                                                type="number" name="tag_id" id="tag">
                                            @if($settings->tag_id)
                                                <option value="{{$settings->tag_id}}" selected>{{$settings->tag->title}}</option>
                                            @endif
                                        </select>
                                        @error('tag_id')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="ذخیره تغییرات" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            const commission = $('#commission');
            commission.on('keyup', function () {
                const n = commission.val();
                if (n < 0)
                    commission.val(0);
                if (n > 100)
                    commission.val(100);
            });
        });
        // Select Tag
        $("#tag").select2({
            theme: 'bootstrap',
            allowClear: true,
            ajax: {
                url: "{{route('admin.suggestionTags')}}",
                delay: 100,
                dataType: 'json',
                data: function (params) {
                    return {
                        term: params.term,
                    };
                },
                processResults: function (data) {
                    console.log(data)
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: item.id
                            }
                        })
                    };
                },
            },
            placeholder: 'برچسب مورد نظر را جستجو کنید',
            minimumInputLength: 2,
        });
    </script>
@endsection
