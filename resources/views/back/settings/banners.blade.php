@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تنظیمات</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>تنظیمات سایت</h4>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{route('admin.settings.banners.store')}}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h2>بنرها</h2>
                                        <hr>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center mb-5">
                                    <div class="col-lg-2 form-control-label">
                                        <img src="{{asset('assets/front/images/banners.jpg')}}" alt="pattern">
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green">
                                            <strong>1</strong>
                                        </h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner1_file" class="col-lg-2 form-control-label">
                                        تصویر بک گراند
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner1_file" id="banner1_file"
                                               class="form-control @error('banner1_file'){{'is-invalid'}}@enderror">
                                        @error('banner1_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner1_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner1" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner1_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner1_username" class="col-lg-2 form-control-label">
                                        یوزر نیم نویسنده
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner1_username" id="banner1_username"
                                               class="form-control @error('banner1_username'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner1_username}}">
                                        @error('banner1_username')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner1_user_avatar_file" class="col-lg-2 form-control-label">
                                        آواتار نویسنده
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner1_user_avatar_file" id="banner1_user_avatar_file"
                                               class="form-control @error('banner1_user_avatar_file'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner1_user_avatar}}">
                                        @error('banner1_user_avatar_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner1_user_avatar))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            آواتار فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="60" width="60" alt="banner1_avatar" class="rounded-circle"
                                                 style="object-fit: cover"
                                                 src="{{env('STORAGE_ADDRESS').env('AVATARS').$banners->banner1_user_avatar}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner1_user_url" class="col-lg-2 form-control-label">لینک
                                        نویسنده</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner1_user_url" id="banner1_user_url"
                                               class="form-control @error('banner1_user_url'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner1_user_url}}">
                                        @error('banner1_user_url')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green"><strong>2</strong></h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner2_file" class="col-lg-2 form-control-label">تصویر بک
                                        گراند</label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner2_file" id="banner2_file"
                                               class="form-control @error('banner2_file'){{'is-invalid'}}@enderror">
                                        @error('banner2_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner2_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner2" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner2_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner2_link" class="col-lg-2 form-control-label">لینک تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner2_link" id="banner2_link"
                                               class="form-control @error('banner2_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner2_link}}">
                                        @error('banner2_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green"><strong>3</strong></h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner3_file" class="col-lg-2 form-control-label">
                                        تصویر بک گراند
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner3_file" id="banner3_file"
                                               class="form-control @error('banner3_file'){{'is-invalid'}}@enderror">
                                        @error('banner3_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner3_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner3" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner3_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner3_link" class="col-lg-2 form-control-label">لینک تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner3_link" id="banner3_link"
                                               class="form-control @error('banner3_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner3_link}}">
                                        @error('banner3_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner3_icon" class="col-lg-2 form-control-label">آیکون تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner3_icon" id="banner3_icon"
                                               class="form-control @error('banner3_icon'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner3_icon}}">
                                        @error('banner3_icon')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green"><strong>4</strong></h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner4_file" class="col-lg-2 form-control-label">
                                        تصویر بک گراند
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner4_file" id="banner4_file"
                                               class="form-control @error('banner4_file'){{'is-invalid'}}@enderror">
                                        @error('banner4_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner4_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner4" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner4_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner4_link" class="col-lg-2 form-control-label">لینک تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner4_link" id="banner4_link"
                                               class="form-control @error('banner4_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner4_link}}">
                                        @error('banner4_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner4_icon" class="col-lg-2 form-control-label">آیکون تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner4_icon" id="banner4_icon"
                                               class="form-control @error('banner4_icon'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner4_icon}}">
                                        @error('banner4_icon')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green"><strong>5</strong></h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner5_file" class="col-lg-2 form-control-label">تصویر بک
                                        گراند</label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner5_file" id="banner5_file"
                                               class="form-control @error('banner5_file'){{'is-invalid'}}@enderror">
                                        @error('banner5_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner5_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner5" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner5_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner5_link" class="col-lg-2 form-control-label">
                                        لینک تصویر
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner5_link" id="banner5_link"
                                               class="form-control @error('banner5_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner5_link}}">
                                        @error('banner5_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner5_icon" class="col-lg-2 form-control-label">آیکون تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner5_icon" id="banner5_icon"
                                               class="form-control @error('banner5_icon'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner5_icon}}">
                                        @error('banner5_icon')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <hr class="my-4">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-lg-2 form-control-label">حذف بنر</label>
                                    <div class="col-lg-8">
                                        <div class="mb-3">
                                            <div class="styled-checkbox">
                                                <input type="checkbox" name="remove_banner2" id="remove_banner2">
                                                <label for="remove_banner2">حذف بنر 2</label>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="styled-checkbox">
                                                <input type="checkbox" name="remove_banner3" id="remove_banner3">
                                                <label for="remove_banner3">حذف بنر 3</label>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="styled-checkbox">
                                                <input type="checkbox" name="remove_banner4" id="remove_banner4">
                                                <label for="remove_banner4">حذف بنر 4</label>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div class="styled-checkbox">
                                                <input type="checkbox" name="remove_banner5" id="remove_banner5">
                                                <label for="remove_banner5">حذف بنر 5</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="ذخیره تغییرات" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
