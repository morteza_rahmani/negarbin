@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تنظیمات</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>تنظیمات سایت</h4>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{route('admin.settings.searches.store')}}" method="post">
                                @csrf
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h2>جستجو ها</h2>
                                        <hr>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>1</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword1" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword1" id="keyword1" value="{{$searches->keyword1}}"
                                               class="form-control @error('keyword1'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword1')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>2</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword2" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword2" id="keyword2" value="{{$searches->keyword2}}"
                                               class="form-control @error('keyword2'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword2')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>3</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword3" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword3" id="keyword3" value="{{$searches->keyword3}}"
                                               class="form-control @error('keyword3'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword3')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>4</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword4" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword4" id="keyword4" value="{{$searches->keyword4}}"
                                               class="form-control @error('keyword4'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword4')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>5</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword5" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword5" id="keyword5" value="{{$searches->keyword5}}"
                                               class="form-control @error('keyword5'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword5')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>6</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword6" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword6" id="keyword6" value="{{$searches->keyword6}}"
                                               class="form-control @error('keyword6'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword6')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:red"><strong>7</strong></h3>
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center">
                                    <label for="keyword7" class="col-lg-2 form-control-label">عنوان جستجو</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="keyword7" id="keyword7" value="{{$searches->keyword7}}"
                                               class="form-control @error('keyword7'){{'is-invalid'}}@enderror"
                                               placeholder="عنوان جستجو">
                                        @error('keyword7')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <br>
                                <div class="text-right">
                                    <input type="submit" value="ذخیره تغییرات" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>


                    <br>


                </div>
            </div>
            <!-- End Sorting -->

        </div>
    </div>
@endsection
