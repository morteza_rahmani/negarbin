@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تنظیمات</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>تنظیمات سایت - اسلایدر داشبورد کاربران</h4>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{route('admin.settings.dashboard_banners.store')}}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h2>بنرها</h2>
                                        <hr>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green">
                                            <strong>1</strong>
                                        </h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner1_file" class="col-lg-2 form-control-label">
                                        آپلود تصویر
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner1_file" id="banner1_file"
                                               class="form-control @error('banner1_file'){{'is-invalid'}}@enderror">
                                        @error('banner1_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner1_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner1" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner1_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner1_link" class="col-lg-2 form-control-label">لینک تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner1_link" id="banner1_link"
                                               class="form-control @error('banner1_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner1_link}}">
                                        @error('banner1_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green"><strong>2</strong></h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner2_file" class="col-lg-2 form-control-label">
                                        آپلود تصویر
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner2_file" id="banner2_file"
                                               class="form-control @error('banner2_file'){{'is-invalid'}}@enderror">
                                        @error('banner2_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner2_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner2" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner2_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner2_link" class="col-lg-2 form-control-label">لینک تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner2_link" id="banner2_link"
                                               class="form-control @error('banner2_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner2_link}}">
                                        @error('banner2_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-lg-2 form-control-label">
                                        <h3 style="color:green"><strong>3</strong></h3>
                                    </div>
                                </div>
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner3_file" class="col-lg-2 form-control-label">
                                        آپلود تصویر
                                    </label>
                                    <div class="col-lg-8">
                                        <input type="file" name="banner3_file" id="banner3_file"
                                               class="form-control @error('banner3_file'){{'is-invalid'}}@enderror">
                                        @error('banner3_file')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($banners->banner3_image))
                                    <div class="form-group row d-flex align-items-center">
                                        <label class="col-lg-2 form-control-label">
                                            تصویر فعلی
                                        </label>
                                        <div class="col-lg-8">
                                            <img height="100" alt="banner3" class="rounded"
                                                 src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner3_image}}">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row d-flex align-items-center">
                                    <label for="banner3_link" class="col-lg-2 form-control-label">لینک تصویر</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="banner3_link" id="banner3_link"
                                               class="form-control @error('banner3_link'){{'is-invalid'}}@enderror"
                                               value="{{$banners->banner3_link}}">
                                        @error('banner3_link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="text-right">
                                    <input type="submit" value="ذخیره تغییرات" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
