@extends('back.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/datatables/datatables.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تسویه ها</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <!-- Sorting -->
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>لیست تسویه حساب ها</h4>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table id="sorting-table" class="table mb-0">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>username</th>
                                    <th>نام مشتری</th>
                                    <th>تاریخ ثبت</th>
                                    <th>وضعیت</th>
                                    <th>مبلغ</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-sm-12 col-md-5">

                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="dataTables_paginate paging_simple_numbers"
                                 id="sorting-table_paginate">
                                <ul class="pagination" style="justify-content:flex-end">
                                    <li id="sorting-table_previous"
                                        class="paginate_button page-item previous disabled">
                                        <a href="#" class="page-link">قبلی</a>
                                    </li>
                                    <li class="paginate_button page-item active">
                                        <a href="#" class="page-link">1</a>
                                    </li>
                                    <li class="paginate_button page-item">
                                        <a href="#" class="page-link">2</a>
                                    </li>
                                    <li id="sorting-table_next" class="paginate_button page-item next">
                                        <a href="#" class="page-link">بعدی</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Sorting -->

        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sorting-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.withdraws.datatable') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'username', name: 'username'},
                    {data: 'full_name', name: 'full_name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'status', name: 'status'},
                    {data: 'amount', name: 'amount'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                language: {
                    "emptyTable": "هیچ داده ای یافت نشد",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ ورودی",
                    "infoEmpty": "نمایش 0 تا 0 از 0 ورودی",
                    "infoFiltered": "(فیلتر شده از _MAX_ مجموعه ورودی)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "نمایش _MENU_ ورودی",
                    "loadingRecords": "در حال بارگذاری...",
                    "processing": "در حال پردازش...",
                    "search": "جستجو: ",
                    "zeroRecords": "داده ای یافت نشد",
                    "paginate": {
                        "first": "اولین",
                        "last": "آخرین",
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                }
            });
            $('#sorting-table_filter input').attr('placeholder', 'جستجو در همه ستون ها');
        });
    </script>
@endsection
