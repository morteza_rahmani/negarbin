@extends('back.layouts.app')
@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تیکت ها</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>افزودن تیکت</h2>
                </div>
                <div class="widget-body no-padding">
                    <ul class="ticket list-group w-100">

                        <li>
                            <div class="widget-body">
                                <form action="{{route('admin.tickets.store')}}" method="post">
                                    @csrf
                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="title" class="col-lg-2 form-control-label">عنوان</label>
                                        <div class="col-lg-8">
                                            <input type="text" id="title" name="title" placeholder="عنوان تیکت"
                                                   class="form-control @error('title'){{'is-invalid'}}@enderror"
                                                   value="{{old('title')}}">
                                            @error('title')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="to"
                                               class="col-lg-2 form-control-label @error('title'){{'is-invalid'}}@enderror">
                                            به
                                        </label>
                                        <div class="col-lg-8">
                                            <input type="text" placeholder="آیدی کاربر مورد نظر" name="to" id="to"
                                                   class="form-control @error('to'){{'is-invalid'}}@enderror"
                                                   value="{{old('to')}}">
                                            @error('to')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="message" class="col-lg-2 form-control-label">متن</label>
                                        <div class="col-lg-8">
                                            <textarea name="message" rows="8" cols="80" id="message"
                                                      placeholder="پیام خود را بنویسید ..."
                                                      class="form-control @error('message'){{'is-invalid'}}@enderror"
                                            >{{old('message')}}</textarea>
                                            @error('message')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <input type="submit" name="edit_submit" class="btn btn-success" value="ثبت">
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
