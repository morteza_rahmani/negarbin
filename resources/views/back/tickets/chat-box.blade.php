<div class="messenger-message {{$ticket->is_response ? 'messenger-message-recipient' : 'messenger-message-sender'}}">
    <img class="messenger-image messenger-image-default"
         src="{{$ticket->user->adjusted_avatar}}" alt="...">
    <div class="messenger-message-wrapper">
        <div class="messenger-message-content">
            <p>
                <span class="messenger-message-author mb-2">{{$ticket->user->full_name}}</span>
                {!! nl2br($ticket->message) !!}
            </p>
        </div>
        <div class="messenger-details">
            <span class="messenger-message-localization font-size-small">
                {{jdate($ticket->created_at)->format('Y/m/d (H:i)')}}
            </span>
        </div>
    </div>
</div>
