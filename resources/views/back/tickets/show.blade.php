@extends('back.layouts.app')
@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">تیکت ها</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center justify-content-between">
                    <h2><span dir="ltr" class="text-muted">#{{$ticket->id}}</span> {{$ticket->title}}</h2>
                    <div class="badge badge-text bg-{{$ticket->statusColor()}}">{{$ticket->status}}</div>
                </div>
                <div class="widget-body">
                    <div class="tab-content">
                        <div id="messenger">
                            @include('back.tickets.chat-box')
                            @foreach($ticket->responses as $res)
                                @include('back.tickets.chat-box', ['ticket' => $res])
                            @endforeach
                        </div>
                        <div id="respond">
                            <h3 class="my-5">ارسال پاسخ</h3>
                            @if($ticket->status == 'بسته')
                                <div class="alert alert-primary-bordered" style="width: fit-content">
                                    این تیکت توسط کاربر بسته شده است.
                                </div>
                            @else
                                <form action="{{route('admin.tickets.update', $ticket->id)}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="ticket_message">متن پیام:</label>
                                        <textarea name="message" id="ticket_message" cols="30" rows="8"
                                                  class="form-control @error('message'){{'is-invalid'}}@enderror"
                                        >{{old('message')}}</textarea>
                                        @error('message')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-gradient-01">ارسال پاسخ تیکت</button>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
