@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">ایجاد جایزه جدید</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>ایجاد جایزه جدید</h2>
                </div>
                <div class="widget-body no-padding">
                    <div class="widget-body">
                        <form action="{{route('admin.awards.update', $award->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="name" class="col-lg-2 form-control-label">عنوان جایزه</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('name'){{'is-invalid'}}@enderror"
                                           name="name" id="name" placeholder="عنوان جایزه"
                                           value="{{$award->name}}">
                                    @error('name')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="icon" class="col-lg-2 form-control-label">آیکون جایزه</label>
                                <div class="col-lg-8">
                                    <input type="file" class="form-control @error('icon'){{'is-invalid'}}@enderror"
                                           name="icon" id="icon" placeholder="آیکون جایزه">
                                    @error('icon')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="icon" class="col-lg-2 form-control-label">آیکون فعلی</label>
                                <div class="col-lg-8">
                                    <img src="{{env('STORAGE_ADDRESS').env('AWARDS').$award->icon}}" width="50"
                                         height="50" alt="{{$award->icon}}">
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="description" class="col-lg-2 form-control-label">توضیحات جایزه</label>
                                <div class="col-lg-8">
                                    <textarea class="form-control @error('description'){{'is-invalid'}}@enderror"
                                              name="description" id="description" placeholder="توضیح کوتاه درمورد جایزه"
                                    >{{$award->description}}</textarea>
                                    @error('description')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-gradient-01">ذخیره</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
