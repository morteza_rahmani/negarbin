@extends('back.layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/datatables/datatables.min.css')}}">
    <style>
        .ticket .avatar {
            width: 50px;
            height: 50px;
            object-fit: cover;
            border-radius: 50%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">نظرات</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>همه نظرات</h2>
                </div>
                <div class="widget-body">
                    <ul class="ticket list-group w-100">
                        <li>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="sorting-table" class="table table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>نویسنده</th>
                                            <th>دیدگاه</th>
                                            <th>پست</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                            <th>تاریخ ارسال</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/back/vendors/js/datatables/datatables.min.js')}}"></script>
    {{--    <script src="{{asset('assets/back/vendors/js/calendar/locale/fa.js')}}"></script>--}}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sorting-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.comments.datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'author', name: 'author'},
                    {data: 'content', name: 'content'},
                    {data: 'post', name: 'post'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: true, searchable: true},
                    {data: 'created_at', name: 'created_at'},
                ],
                language: {
                    "emptyTable": "هیچ داده ای یافت نشد",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ ورودی",
                    "infoEmpty": "نمایش 0 تا 0 از 0 ورودی",
                    "infoFiltered": "(فیلتر شده از _MAX_ مجموعه ورودی)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "نمایش _MENU_ ورودی",
                    "loadingRecords": "در حال بارگذاری...",
                    "processing": "در حال پردازش...",
                    "search": "جستجو: ",
                    "zeroRecords": "داده ای یافت نشد",
                    "paginate": {
                        "first": "اولین",
                        "last": "آخرین",
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                }
            });
            $('#sorting-table_filter input').attr('placeholder', 'جستجو در همه ستون ها');
        });
    </script>
@endsection
