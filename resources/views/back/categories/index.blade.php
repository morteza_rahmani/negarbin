@extends('back.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/icomoons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/back/css/bootstrap-select/bootstrap-select.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">دسته بندی ها</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>دسته بندی ها</h2>
                </div>
                <div class="widget-body no-padding">
                    <ul class="ticket list-group w-100">
                        <li>
                            <div class="widget-body">
                                <div class="row">
                                    <div class="col-md-5">
                                        <h5>اضافه کردن دسته جدید</h5>
                                        <br>
                                        <form action="{{route('admin.categories.store')}}" method="post">
                                            @csrf
                                            <div class="form-group mb-3">
                                                <label for="category_name" class="form-control-label">نام</label>
                                                <input type="text" name="name" id="category_name"
                                                       class="form-control">
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="category_slug" class="form-control-label">نامک
                                                    انگلیسی</label>
                                                <input type="text" name="slug" id="category_slug"
                                                       class="form-control">
                                            </div>
                                            <div class="form-group mb-3">
                                                <div class="">
                                                    <label for="category_parent" class="form-control-label">
                                                        دسته والد
                                                    </label>
                                                </div>
                                                <div class="dropdown bootstrap-select show-menu-arrow">
                                                    <select class="selectpicker show-menu-arrow" id="category_parent"
                                                            name="parent_id" data-live-search="true">
                                                        <option selected value> -- انتخاب کنید -- </option>
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}">
                                                                {{$category->name}}
                                                            </option>
                                                            @if(!empty($category->children[0]))
                                                                @foreach($category->children as $category)
                                                                    <option value="{{$category->id}}">
                                                                        _{{$category->name}}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="priority" class="form-control-label">اولویت</label>
                                                <input type="number" name="priority" class="form-control" id="priority"
                                                       placeholder="مقدار اولویت را وارد نمایید">
                                                <span class="font-weight-400 text-small">
                                                    دسته بندی ها بر اساس اولویت و به صورت صعودی مرتب میشوند
                                                </span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="category_icon" class="form-control-label">آیکون</label>
                                                <input type="text" name="icon" class="form-control" id="category_icon"
                                                       placeholder="نام آیکون را وارد کنید">
                                            </div>
                                            <div class="text-right">
                                                <input type="submit" name="category_submit" value="ثبت"
                                                       class="btn btn-success btn-lg mr-1 mb-2">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-7">
                                        <h5>دسته های فعلی</h5>
                                        <br>
                                        <div class="form-group mb-3">
                                            @if(count($categories) > 0)
                                                @include('back.categories.category-box')
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/bootstrap-select/bootstrap-select.min.js')}}"></script>
@endsection
