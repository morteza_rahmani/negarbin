@foreach($categories as $category)
    <ul>
        <li>
            <div class="alert alert-shadow alert-lg ng_categories" title="اولویت: {{$category->priority}}">
                <div class="align-items.center d-flex ng_category-title">
                    @if(isset($category->icon))
                        <i class="{{$category->icon}} mr-1 font-weight-bold" style="font-size: 1.2rem"></i>
                    @endif
                    {{$category->name}}
                </div>
                <div class="ng_category_option">
                    <a href="{{route('admin.categories.edit', $category->id)}}" class="text-right">ویرایش</a>
                    <form action="{{route('admin.categories.destroy', $category->id)}}" method="post"
                          class="d-inline-block"
                          onsubmit="return confirm(`آیا برای حذف دسته بندی {{"'{$category->name}'"}} اطمینان دارید؟`)">
                        @csrf
                        @method('delete')
                        <button type="submit" class="bg-transparent border-0 p-0 text-right text-primary"
                                style="font-weight: 500 !important">حذف
                        </button>
                    </form>
                </div>
            </div>
            @if(count($category->children) > 0)
                @include('back.categories.category-box', ['categories' => $category->children()->orderBy('priority')->get()])
            @endif
        </li>
    </ul>
@endforeach
