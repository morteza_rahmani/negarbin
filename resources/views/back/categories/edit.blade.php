@extends('back.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/icomoons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/back/css/bootstrap-select/bootstrap-select.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">دسته بندی ها</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>ویرایش دسته بندی "{{$category->name}}"</h2>
                </div>
                <div class="widget-body no-padding">
                    <ul class="ticket list-group w-100">
                        <li>
                            <div class="widget-body">
                                <h5>ویرایش دسته بندی</h5>
                                <br>
                                <form action="{{route('admin.categories.update', $category->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="form-group mb-3">
                                        <label for="category_name" class="form-control-label">نام</label>
                                        <input type="text" name="name" id="category_name" class="form-control"
                                               value="{{$category->name}}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="category_slug" class="form-control-label">نامک
                                            انگلیسی</label>
                                        <input type="text" name="slug" id="category_slug" class="form-control"
                                               value="{{$category->slug}}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <div class="">
                                            <label for="category_parent" class="form-control-label">
                                                دسته والد
                                            </label>
                                        </div>
                                        <div class="dropdown bootstrap-select show-menu-arrow">
                                            <select class="selectpicker show-menu-arrow" id="category_parent"
                                                    name="parent_id" data-live-search="true">
                                                <option value> -- انتخاب کنید -- </option>
                                                @foreach($categories as $cat)
                                                    @if($cat->id != $category->id)
                                                        <option value="{{$cat->id}}"
                                                                @if($cat->id == $category->parent_id) selected @endif>
                                                            {{$cat->name}}
                                                        </option>
                                                        @if(count($cat->children) > 0)
                                                            @foreach($cat->children as $cat)
                                                                @if($cat->id != $category->id)
                                                                    <option value="{{$cat->id}}"
                                                                    @if($cat->id == $category->parent_id){{'selected'}}@endif>
                                                                        _{{$cat->name}}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="priority" class="form-control-label">اولویت</label>
                                        <input type="number" name="priority" class="form-control" id="priority"
                                               placeholder="مقدار اولویت را وارد نمایید" value="{{$category->priority}}">
                                        <span class="font-weight-400 text-small">
                                            دسته بندی ها بر اساس اولویت و به صورت صعودی مرتب میشوند
                                        </span>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="category_icon" class="form-control-label">آیکون</label>
                                        <input type="text" name="icon" class="form-control" id="category_icon"
                                               placeholder="نام آیکون را وارد کنید" value="{{$category->icon}}">
                                    </div>
                                    <div class="text-right">
                                        <a class="btn btn-danger" href="{{route('admin.categories.index')}}">بازگشت</a>
                                        <input type="submit" name="category_submit" value="ویرایش"
                                               class="btn btn-success font-weight-500">
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/bootstrap-select/bootstrap-select.min.js')}}"></script>
@endsection
