@extends('back.layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/datatables/datatables.min.css')}}">
    <style>
        .ticket .avatar {
            height: 50px;
            object-fit: cover;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">محصولات</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>همه محصولات</h2>
                </div>
                <div class="widget-body widget-10 no-padding">
                    <ul class="ticket list-group w-100">
                        <li>
                            <div class="widget-body">
                                <div class="table-responsive">
                                    <table id="sorting-table" class="table mb-0">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>تصویر</th>
                                            <th>محصول</th>
                                            <th>فروشنده</th>
                                            <th>قیمت</th>
                                            <th>فروش</th>
                                            <th>درآمد</th>
                                            <th>تاریخ</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/back/vendors/js/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sorting-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.products.datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'thumbnail', name: 'thumbnail'},
                    {data: 'product_name', name: 'product_name'},
                    {data: 'author', name: 'author'},
                    {data: 'price', name: 'price'},
                    {data: 'purchases', name: 'purchases'},
                    {data: 'purchases', name: 'purchases'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                language: {
                    "emptyTable": "هیچ داده ای یافت نشد",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ ورودی",
                    "infoEmpty": "نمایش 0 تا 0 از 0 ورودی",
                    "infoFiltered": "(فیلتر شده از _MAX_ مجموعه ورودی)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "نمایش _MENU_ ورودی",
                    "loadingRecords": "در حال بارگذاری...",
                    "processing": "در حال پردازش...",
                    "search": "جستجو: ",
                    "zeroRecords": "داده ای یافت نشد",
                    "paginate": {
                        "first": "اولین",
                        "last": "آخرین",
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                }
            });
            $('#sorting-table_filter input').attr('placeholder', 'جستجو در همه ستون ها');
        });
    </script>
@endsection
