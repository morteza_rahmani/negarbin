@extends('back.layouts.app')
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/vendors/css/tagify/tagify.min.css')}}">
    <link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">ویرایش محصول</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>ویرایش محصول ({{$product->name}})</h2>
                </div>
                <div class="widget-body no-padding">
                    <ul class="ticket list-group w-100">
                        <li>
                            <div class="widget-body">
                                <form action="{{route('admin.products.update', $product->id)}}" method="post"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_name" class="col-lg-2 form-control-label">عنوان</label>
                                        <div class="col-lg-8">
                                            <input type="text" name="product_name" id="product_name"
                                                   value="{{$product->name}}"
                                                   class="form-control @error('product_name'){{'is-invalid'}}@enderror">
                                            @error('product_name')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_slug" class="col-lg-2 form-control-label">نامک</label>
                                        <div class="col-lg-8">
                                            <input type="text" name="product_slug" id="product_slug"
                                                   value="{{$product->slug}}"
                                                   class="form-control @error('product_slug'){{'is-invalid'}}@enderror">
                                            @error('product_slug')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="thumbnail" class="col-lg-2 form-control-label">تصویر شاخص</label>
                                        <div class="col-lg-8">
                                            <input type="file" name="thumbnail" id="thumbnail"
                                                   class="form-control @error('thumbnail'){{'is-invalid'}}@enderror">
                                            @error('thumbnail')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                        <img alt="preview" width="60" height="60" style="object-fit: cover"
                                             src="{{env('STORAGE_ADDRESS').'/'.env('PRODUCTS').$product->thumbnail}}">
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_content" class="col-lg-2 form-control-label">محتوا</label>
                                        <div class="col-lg-8">
                                            <textarea name="product_content" rows="12" cols="80" id="product_content"
                                                      class="tinymce form-control @error('product_content'){{'is-invalid'}}@enderror"
                                            >{{ $product->content }}</textarea>
                                            @error('product_content')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_file" class="col-lg-2 form-control-label">
                                            آپلود فایل جدید
                                        </label>
                                        <div class="col-lg-8">
                                            <div class="mb-3">
                                                <input class="form-control" type="file" id="product_file" name="file">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label class="col-lg-2 form-control-label">فایل فعلی</label>
                                        <div class="col-lg-8">
                                            <div class="mb-3">
                                                <a href="{{route('products.download', $product->id)}}"
                                                   target="_blank" class="btn btn-gradient-03">
                                                    لینک دانلود فایل
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_price" class="col-lg-2 form-control-label">قیمت</label>
                                        <div class="col-lg-8">
                                            <div class="mb-3">
                                                <input type="text" name="product_price" id="product_price"
                                                       class="form-control @error('product_price'){{'is-invalid'}}@enderror"
                                                       placeholder="تومان" value="{{$product->price}}">
                                                @error('product_price')
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label class="col-lg-2 form-control-label">دسته بندی</label>
                                        <div class="bg-grey col-lg-8 px-0 py-4"
                                             style="max-height: 380px; overflow: auto">
                                            <ul>
                                                @foreach($categories as $category)
                                                    <li>
                                                        <div class="mb-3">
                                                            <div class="styled-checkbox">
                                                                <input id="check-{{$category->id}}" type="checkbox"
                                                                       name="categories[]" value="{{$category->id}}"
                                                                @if(in_array($category->id, $product->categories()->pluck('id')->toArray()))
                                                                    {{'checked'}}
                                                                    @endif>
                                                                <label for="check-{{$category->id}}">
                                                                    {{$category->name}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        @if(count($category->children) > 0)
                                                            <ul class="children_list">
                                                                @foreach($category->children as $child)
                                                                    <li>
                                                                        <div class="mb-3">
                                                                            <div class="styled-checkbox">
                                                                                <input id="check-{{$child->id}}"
                                                                                       type="checkbox"
                                                                                       name="categories[]"
                                                                                       value="{{$child->id}}"
                                                                                @if(in_array($child->id, $product->categories()->pluck('id')->toArray()))
                                                                                    {{'checked'}}
                                                                                    @endif>
                                                                                <label for="check-{{$child->id}}">
                                                                                    {{$child->name}}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        @if(count($child->children) > 0)
                                                                            <ul class="children_list">
                                                                                @foreach($child->children as $child)
                                                                                    <li>
                                                                                        <div class="mb-3">
                                                                                            <div
                                                                                                class="styled-checkbox">
                                                                                                <input
                                                                                                    id="check-{{$child->id}}"
                                                                                                    type="checkbox"
                                                                                                    name="categories[]"
                                                                                                    value="{{$child->id}}"
                                                                                                @if(in_array($child->id, $product->categories()->pluck('id')->toArray()))
                                                                                                    {{'checked'}}
                                                                                                    @endif>
                                                                                                <label
                                                                                                    for="check-{{$child->id}}">
                                                                                                    {{$child->name}}
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_summary" class="col-lg-2 form-control-label">
                                            چکیده مطلب
                                        </label>
                                        <div class="col-lg-8">
                                            <div class="mb-3">
                                                <textarea name="product_summary" rows="8" cols="80" id="product_summary"
                                                          class="form-control @error('product_summary'){{'is-invalid'}}@enderror"
                                                >{{$product->summary}}</textarea>
                                                @error('product_summary')
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="tags" class="col-lg-2 form-control-label">برچسب ها</label>
                                        <div class="col-lg-8">
                                            <div class="mb-3">
                                                <input type="text" name="tags" id="tags" value="{{$tags}}"
                                                       class="form-control tagify_input px-2 @error('tags'){{'is-invalid'}}@enderror">
                                                @error('tags')
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row d-flex align-items-center mb-5">
                                        <label for="product_format" class="col-lg-2 form-control-label">فرمت</label>
                                        <div class="col-lg-8">
                                            <div class="mb-3">
                                                <input type="text" name="format" id="product_format"
                                                       value="{{$product->format}}"
                                                       class="form-control @error('format'){{'is-invalid'}}@enderror">
                                                @error('format')
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <input type="submit" name="edit_submit" class="btn btn-gradient-01"
                                               value="ویرایش">
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/axios@0.21.1/dist/axios.min.js"></script>
    <script src="{{asset('assets/back/vendors/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('assets/back/vendors/js/tagify/jQuery.tagify.min.js')}}"></script>
    <script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
    <script type="text/javascript">
        let useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
        let editor_config = {
            path_absolute: "/",
            selector: '.tinymce',
            relative_urls: false,
            directionality: "rtl",
            plugins: 'print preview paste importcss searchreplace autolink autosave directionality code visualblocks ' +
                'visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking ' +
                'anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap ' +
                'emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | ' +
                'alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor ' +
                'backcolor removeformat | pagebreak | charmap emoticons | fullscreen preview print | ' +
                'insertfile image media template link anchor codesample | ltr rtl',
            file_picker_callback(callback, value, meta) {
                let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth
                let y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight

                tinymce.activeEditor.windowManager.openUrl({
                    url: '/file-manager/tinymce5/?leftPath=photos/content/products',
                    title: 'File manager',
                    width: x * 0.9,
                    height: y * 0.9,
                    onMessage: (api, message) => {
                        callback(message.content, {text: message.text})
                    }
                })
            },
            toolbar_sticky: false,
            image_advtab: true,
            importcss_append: true,
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',
            contextmenu: 'link image imagetools table',
            skin: useDarkMode ? 'oxide-dark' : 'oxide',
            content_css: useDarkMode ? 'dark' : 'default',
            content_style: 'body { font-family:iranyekan; font-size:14px }'
        };
        tinymce.init(editor_config)

        // Tagify
        let inputElm = document.querySelector("#tags"),
            tagify = new Tagify(inputElm, {
                maxTags: parseInt(inputElm.dataset.maxTags) || 6,
                whitelist: [],
                placeholder: 'تگ مورد نظر را وارد کنید و اینتر بزنید',
                originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(',')
            })
        tagify.on('input', function (t) {
            var value = t.detail.value
            tagify.whitelist = null;
            if (value.length > 1) {
                axios
                    .get('{{route('suggestionTags')}}/?term=' + value)
                    .then(function (t) {
                        tagify.settings.whitelist = t.data // update inwhitelist Array in-place
                        tagify.dropdown.show(value)
                    })
                    .catch(function (inputElm) {
                    });
            } else {
                tagify.whitelist = null;
            }
        })
    </script>

@endsection
