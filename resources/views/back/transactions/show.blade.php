@extends('back.layouts.app')
@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">جزئیات سفارش</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>سفارش: #{{$payment->invoice->id}}</h4>
                </div>
                <div class="widget-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                شناسه پرداخت:
                            </td>
                            <td>{{$payment->transaction}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت پرداخت:</td>
                            <td>{{$payment->translateStatus()}}</td>
                        </tr>
                        <tr>
                            <td>تاریخ:</td>
                            <td>{{jdate($payment->created_at)->format('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>نام:</td>
                            <td>{{$payment->invoice->user->full_name}}</td>
                        </tr>
                        <tr>
                            <td>ایمیل:</td>
                            <td>{{$payment->invoice->email}}</td>
                        </tr>
                        <tr>
                            <td>ریز کل:</td>
                            <td>{{number_format($payment->invoice->amount)}} تومان</td>
                        </tr>
                        <tr>
                            <td>جمع کل:</td>
                            <td>{{number_format($payment->invoice->amount)}} تومان</td>
                        </tr>
                        </tbody>
                    </table>
                    @if($payment->status == \App\Models\Payment::STATUS_SUCCEED)
                        <h3 class="mb-4 mt-5">محصولات</h3>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>قیمت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payment->invoice->products as $product)
                                <tr>
                                    <td>
                                        <a href="{{route('products.show', [$product->id, $product->slug])}}"
                                           class="edd_purchase_receipt_product_name text-dark mb-2">
                                            {{$product->pivot->product_name}}
                                        </a>
                                        <ul class="edd_purchase_receipt_files">
                                            <li class="edd_download_file">
                                                <a href="{{route('products.download', $product->id)}}"
                                                   class="edd_download_file_link">
                                                    دانلود فایل
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        {{number_format($product->pivot->product_price)}} تومان
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
