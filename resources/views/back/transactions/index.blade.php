@extends('back.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/datatables/datatables.min.css')}}">
    <style>
        .ticket .avatar {
            width: 50px;
            height: 50px;
            object-fit: cover;
            border-radius: 50%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">کاربران</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="widget widget-10 has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>لیست همه کاربران</h4>
                </div>
                <div class="widget-body">
                    <div class="ticket row">
                        <div class="w-100">
                            <div class="form-group col-md-6">
                                <label for="status_select">فیلتر بر اساس وضعیت</label>
                                <select class="form-control" name="status_select" id="status_select" style="height: auto">
                                    <option selected value> -- انتخاب کنید -- </option>
                                    <option value="1">موفق</option>
                                    <option value="2">ناموفق</option>
                                    <option value="0">پرداخت نشده</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="sorting-table" class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>شماره سفارش</th>
                                    <th>مشتری</th>
                                    <th>نام مشتری</th>
                                    <th>مقدار</th>
                                    <th>تاریخ خرید</th>
                                    <th>وضعیت پرداختی</th>
                                    <th>جزئیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/back/vendors/js/calendar/locale/fa.js')}}"></script>
    <script src="{{asset('assets/back/js/persianDatepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#status_select').on('change', function () {
                let vs_val = $(this).find(":selected").val().toString();
                if (vs_val === '') {
                    datatable.search('').columns(8).search('').draw();
                } else {
                    datatable.columns(8).search(vs_val ? '^' + vs_val + '$' : '', true, false).draw();
                }
            });
            let datatable = $('#sorting-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.transactions.datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'user', name: 'user'},
                    {data: 'user_name', name: 'user_name'},
                    {data: 'amount', name: 'amount'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'status_styled', name: 'status_styled'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'status', name: 'status', visible: false},
                ],
                language: {
                    "emptyTable": "هیچ داده ای یافت نشد",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ ورودی",
                    "infoEmpty": "نمایش 0 تا 0 از 0 ورودی",
                    "infoFiltered": "(فیلتر شده از _MAX_ مجموعه ورودی)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "نمایش _MENU_ ورودی",
                    "loadingRecords": "در حال بارگذاری...",
                    "processing": "در حال پردازش...",
                    "search": "جستجو: ",
                    "zeroRecords": "داده ای یافت نشد",
                    "paginate": {
                        "first": "اولین",
                        "last": "آخرین",
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                }
            });
            $('#sorting-table_filter input').attr('placeholder', 'جستجو در همه ستون ها');
        });
    </script>
@endsection
