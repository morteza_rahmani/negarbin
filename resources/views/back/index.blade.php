@extends('back.layouts.app')

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">داشبورد</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <!-- Begin Widget 09 -->
            <div class="widget widget-09 has-shadow">
                <!-- Begin Widget Header -->
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>میزان کل فروش (تومان)</h2>
                </div>
                <!-- End Widget Header -->
                <!-- Begin Widget Body -->
                <div class="widget-body">

                    <div class="widget-body">
                        <div class="chart">
                            <canvas id="line-chart-01"></canvas>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End Widget 09 -->
        </div>
    </div>
    @if(count($payments) > 0)
        <div class="row flex-row">
            <div class="col-xl-12">
                <div class="widget widget-07 has-shadow">
                    <div class="widget-header bordered d-flex align-items-center">
                        <h2>آخرین خریدها</h2>
                    </div>
                    <div class="widget-body">
                        <div class="table-responsive table-scroll padding-right-10" style="max-height:520px;">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>شماره سفارش</th>
                                    <th>نام مشتری</th>
                                    <th>تاریخ خرید</th>
                                    <th>مبلغ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>
                                            <span class="text-primary">
                                                {{$payment->id}}
                                            </span>
                                        </td>
                                        <td>{{$payment->invoice->user->full_name}}</td>
                                        <td>{{jdate($payment->created_at)->format('%d %B %Y')}}</td>
                                        <td>{{number_format($payment->invoice->amount)}} تومان</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="widget-footer d-flex align-items-center">
                        <div class="p-2">
                            <a href="{{route('admin.transactions.index')}}" class="btn btn-gradient-01">
                                مشاهده همه خریدها
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(count($tickets) > 0)
        <div class="row flex-row">
            <div class="col-xl-12">
                <div class="widget widget-10 has-shadow">
                    <div class="widget-header bordered d-flex align-items-center">
                        <h2>تیکت های پشتیبانی</h2>
                    </div>
                    <div class="widget-body no-padding">
                        <ul class="ticket list-group w-100">
                            @foreach($tickets as $ticket)
                                <li class="list-group-item">
                                    <a href="{{route('admin.tickets.show', $ticket->id)}}">
                                        <div class="media">
                                            <div class="media-left align-self-center pr-4">
                                                <img src="{{$ticket->user->adjusted_avatar}}"
                                                     class="user-img rounded-circle" alt="avatar">
                                            </div>
                                            <div class="media-body align-self-center">
                                                <div class="username">
                                                    <h4>{{$ticket->user->full_name}}</h4>
                                                </div>
                                                <div class="msg">
                                                    <p>
                                                        {{$ticket->message}}
                                                    </p>
                                                </div>
                                                <div class="status">
                                                    <span class="open mr-2">{{$ticket->status}}</span>
                                                    ({{jdate($ticket->updated_at)->ago()}})
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/chart/chart.min.js')}}"></script>
    <script src="{{asset('assets/back/vendors/js/calendar/locale/fa.js')}}"></script>
    <script src="{{asset('assets/back/js/dashboard/db-default.js')}}"></script>
    <script type="text/javascript">


        var ctx = document.getElementById('line-chart-01').getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["{{$pDates[0]}}", "{{$pDates[1]}}", "{{$pDates[2]}}", "{{$pDates[3]}}", "{{$pDates[4]}}",
                    "{{$pDates[5]}}", "{{$pDates[6]}}", "{{$pDates[7]}}", "{{$pDates[8]}}", "{{$pDates[9]}}",
                    "{{$pDates[10]}}", "{{$pDates[11]}}"],
                datasets: [
                    {
                        label: "فروش",
                        borderColor: "#08a6c3",
                        pointBackgroundColor: "#08a6c3",
                        pointHoverBorderColor: "#08a6c3",
                        pointHoverBackgroundColor: "#08a6c3",
                        pointBorderColor: "#fff",
                        pointBorderWidth: 3,
                        pointRadius: 6,
                        fill: true,
                        backgroundColor: "transparent",
                        borderWidth: 3,
                        data: {{$pAmount}}
                    },
                ]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    backgroundColor: 'rgba(47, 49, 66, 0.8)',
                    titleFontSize: 13,
                    titleFontColor: '#fff',
                    caretSize: 0,
                    cornerRadius: 4,
                    xPadding: 10,
                    displayColors: false,
                    yPadding: 10
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            display: true,
                            beginAtZero: true
                        },
                        gridLines: {
                            drawBorder: true,
                            display: true
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            drawBorder: true,
                            display: true
                        },
                        ticks: {
                            display: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
