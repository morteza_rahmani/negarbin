@extends('back.layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/back/css/datatables/datatables.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">مطالب</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <!-- Begin Widget 10 -->
            <div class="widget widget-10 has-shadow">
                <!-- Begin Widget Header -->
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>همه مطالب</h2>

                </div>
                <!-- End Widget Header -->
                <!-- Begin Widget Body -->
                <div class="widget-body no-padding">
                    <ul class="ticket list-group w-100">
                        <!-- 01 -->
                        <li>
                            <div class="widget-body">
                                <div class="table-responsive">
                                    <table id="sorting-table" class="table table-hover mb-0">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>تصویر</th>
                                            <th>موضوع(title)</th>
                                            <th>عنوان</th>
                                            <th>نامک</th>
                                            <th>نویسنده</th>
                                            <th>برچسبها</th>
                                            <th>کلمات کلیدی</th>
                                            <th>تاریخ انتشار</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <!-- End 01 -->
                    </ul>
                </div>
                <!-- End Widget Body -->
            </div>
            <!-- End Widget 10 -->
        </div>

    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/back/vendors/js/datatables/datatables.min.js')}}"></script>
{{--    <script src="{{asset('assets/back/vendors/js/calendar/locale/fa.js')}}"></script>--}}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sorting-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.articles.datatable') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'thumbnail', name: 'thumbnail', orderable: true, searchable: true},
                    {data: 'title', name: 'title'},
                    {data: 'name', name: 'name'},
                    {data: 'slug', name: 'slug'},
                    {data: 'author', name: 'author'},
                    {data: 'tags', name: 'tags'},
                    {data: 'keywords', name: 'keywords'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: true, searchable: true},
                ],
                language: {
                    "emptyTable": "هیچ داده ای یافت نشد",
                    "info": "نمایش _START_ تا _END_ از _TOTAL_ ورودی",
                    "infoEmpty": "نمایش 0 تا 0 از 0 ورودی",
                    "infoFiltered": "(فیلتر شده از _MAX_ مجموعه ورودی)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "نمایش _MENU_ ورودی",
                    "loadingRecords": "در حال بارگذاری...",
                    "processing": "در حال پردازش...",
                    "search": "جستجو: ",
                    "zeroRecords": "داده ای یافت نشد",
                    "paginate": {
                        "first": "اولین",
                        "last": "آخرین",
                        "next": "بعدی",
                        "previous": "قبلی"
                    },
                }
            });
            $('#sorting-table_filter input').attr('placeholder', 'جستجو در همه ستون ها');
        });
    </script>
@endsection
