@extends('back.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('vendor/laravel-filemanager/css/lfm.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/back/vendors/css/tagify/tagify.min.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">افزودن مطلب</h2>
            </div>
        </div>
    </div>
    <div class="row flex-row">
        <div class="col-xl-12">
            <div class="widget has-shadow">
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>افزودن مطلب</h2>
                </div>
                <div class="widget-body no-padding">
                    <div class="widget-body">
                        <form action="{{route('admin.pages.update', $page->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="title" class="col-lg-2 form-control-label">موضوع (title)</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('title'){{'is-invalid'}}@enderror"
                                           name="title" id="title" placeholder="موضوع پست درون تگ title قرار میگیرد"
                                           value="{{$page->title}}">
                                    @error('title')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="name" class="col-lg-2 form-control-label">عنوان مطلب</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('name'){{'is-invalid'}}@enderror"
                                           name="name" id="name" placeholder="عنوان مطلب را وارد کنید"
                                           value="{{$page->name}}">
                                    @error('name')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="slug" class="col-lg-2 form-control-label">نامک (slug)</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('slug'){{'is-invalid'}}@enderror"
                                           name="slug" id="slug" placeholder="نامک مطلب را وارد کنید"
                                           value="{{$page->slug}}">
                                    @error('slug')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="content" class="col-lg-2 form-control-label">متن</label>
                                <div class="col-lg-8">
                                    <textarea
                                        class="form-control tinymce @error('content'){{'is-invalid'}}@enderror"
                                        name="content" id="content" placeholder="محتوای مطلب را بنویسید"
                                    >{{$page->content}}</textarea>
                                    @error('content')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="summary" class="col-lg-2 form-control-label">چکیده مطلب</label>
                                <div class="col-lg-8">
                                    <div class="mb-3">
                                        <textarea name="summary" rows="6" id="summary"
                                                  placeholder="خلاصه مطلب را بنویسید (155-160 کاراکتر پیشنهاد میشود)"
                                                  class="form-control @error('summary'){{'is-invalid'}}@enderror"
                                        >{!! $page->summary !!}</textarea>
                                        @error('summary')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row d-flex align-items-center mb-5">
                                <label for="keywords" class="col-lg-2 form-control-label">کلمات کلیدی</label>
                                <div class="col-lg-8">
                                    <input type="text" name="keywords" id="keywords" value="{{$page->keywords}}"
                                           class="form-control tagify_input @error('keywords'){{'is-invalid'}}@enderror"
                                           placeholder="کلمه مورد نظر را وارد کنید و اینتر بزنید">
                                    @error('keywords')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-gradient-01">ویرایش</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('assets/back/vendors/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('assets/back/vendors/js/tagify/tagify.min.js')}}"></script>
    <script type="text/javascript">
        let useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
        let editor_config = {
            path_absolute: "/",
            selector: '.tinymce',
            relative_urls: false,
            directionality: "rtl",
            plugins: 'print preview paste importcss searchreplace autolink autosave directionality code visualblocks ' +
                'visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking ' +
                'anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap ' +
                'emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | ' +
                'alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor ' +
                'backcolor removeformat | pagebreak | charmap emoticons | fullscreen preview print | ' +
                'insertfile image media template link anchor codesample | ltr rtl',
            file_picker_callback(callback, value, meta) {
                let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth
                let y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight

                tinymce.activeEditor.windowManager.openUrl({
                    url: '/file-manager/tinymce5/?leftPath=photos/content/pages',
                    title: 'File manager',
                    width: x * 0.9,
                    height: y * 0.9,
                    onMessage: (api, message) => {
                        callback(message.content, {text: message.text})
                    }
                })
            },
            toolbar_sticky: false,
            image_advtab: true,
            importcss_append: true,
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: 'mceNonEditable',
            toolbar_mode: 'sliding',
            contextmenu: 'link image imagetools table',
            skin: useDarkMode ? 'oxide-dark' : 'oxide',
            content_css: useDarkMode ? 'dark' : 'default',
            content_style: 'body { font-family:iranyekan; font-size:14px }'
        };
        tinymce.init(editor_config)
        // Tagify
        let inputElm = document.querySelector("#keywords"),
            tagify1 = new Tagify(inputElm, {
                originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(', ')
            })
    </script>
@endsection
