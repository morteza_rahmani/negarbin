<div class="default-sidebar">
    <!-- Begin Side Navbar -->
    <nav class="side-navbar box-scroll sidebar-scroll">
        <!-- Begin Main Navigation -->
        <ul class="list-unstyled">

            <li>
                <a href="{{route('admin.dashboard.index')}}">
                    <i class="la la-columns"></i>
                    <span>داشبورد</span>
                </a>
            </li>

            <li>
                <a href="#dropdown-db_products" data-toggle="collapse" aria-expanded="false" >
                    <i class="la la-gg"></i>
                    <span>محصولات</span>
                </a>
                <ul id="dropdown-db_products" class="list-unstyled collapse pt-0">
                    <li><a href="{{route('admin.products.index')}}">همه محصولات</a></li>
                    <li><a href="{{route('admin.categories.index')}}">دسته ها</a></li>
                </ul>
            </li>

            <li>
                <a href="{{route('admin.transactions.index')}}">
                    <i class="la la-spinner"></i>
                    <span>تراکنش ها</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.withdraws.index')}}">
                    <i class="la la-money"></i>
                    <span>تسویه ها</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.users.index')}}">
                    <i class="la la-users"></i>
                    <span>کاربران</span>
                </a>
            </li>
            <li>
                <a href="#dropdown-db_awards" data-toggle="collapse" aria-expanded="false">
                    <i class="la la-newspaper-o"></i>
                    <span>جوایز</span>
                </a>
                <ul id="dropdown-db_awards" class="list-unstyled collapse pt-0">
                    <li><a href="{{route('admin.awards.index')}}">لیست جوایز</a></li>
                    <li><a href="{{route('admin.awards.create')}}">جایزه جدید</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('admin.comments.index')}}">
                    <i class="la la-comments"></i>
                    <span>نظرات</span>
                </a>
            </li>
            <li>
                <a href="#dropdown-db_articles" data-toggle="collapse" aria-expanded="false">
                    <i class="la la-newspaper-o"></i>
                    <span>وبلاگ</span>
                </a>
                <ul id="dropdown-db_articles" class="list-unstyled collapse pt-0">
                    <li><a href="{{route('admin.articles.index')}}">همه مطالب</a></li>
                    <li><a href="{{route('admin.articles.create')}}">مطلب جدید</a></li>
                </ul>
            </li>
            <li>
                <a href="#dropdown-db_pages" data-toggle="collapse" aria-expanded="false">
                    <i class="la la-file-text"></i>
                    <span>برگه ها</span>
                </a>
                <ul id="dropdown-db_pages" class="list-unstyled collapse pt-0">
                    <li><a href="{{route('admin.pages.index')}}">همه برگه ها</a></li>
                    <li><a href="{{route('admin.pages.create')}}">برگه جدید</a></li>
                </ul>
            </li>
            <li>
                <a href="#dropdown-db_settings" data-toggle="collapse" aria-expanded="false">
                    <i class="la la-cog"></i>
                    <span>تنظیمات</span>
                </a>
                <ul id="dropdown-db_settings" class="list-unstyled collapse pt-0">
                    <li><a href="{{route('admin.settings.general.index')}}">تنظیمات اصلی</a></li>
                    <li><a href="{{route('admin.settings.banners.index')}}">بنرها</a></li>
                    <li><a href="{{route('admin.settings.dashboard_banners.index')}}">اسلایدر داشبورد کاربران</a></li>
                    <li><a href="{{route('admin.settings.searches.index')}}">جستجوها</a></li>
                </ul>
            </li>
            <li>
                <a href="#dropdown-db_tickets" data-toggle="collapse" aria-expanded="false">
                    <i class="la la-ticket"></i>
                    <span>تیکت های پشتیبانی</span>
                </a>
                <ul id="dropdown-db_tickets" class="list-unstyled collapse pt-0">
                    <li><a href="{{route('admin.tickets.index')}}">همه تیکت ها</a></li>
                    <li><a href="{{route('admin.tickets.create')}}">تیکت جدید</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- End Side Navbar -->
</div>
