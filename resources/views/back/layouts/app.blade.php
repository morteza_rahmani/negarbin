<!DOCTYPE html>
<!--
Item Name: SeenBoard - Web App & Admin Dashboard Template
Version: 1.0
Author: Mt.rezaei
-->
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SeenBoard - Dashboard</title>
    <meta name="description" content="SeenBoard is a Web App and Admin Dashboard Template built with Bootstrap 4">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta')

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/back/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/back/img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/back/img/favicon-16x16.png')}}">
    <!-- Stylesheet -->
    <!-- Font Iran licence -->
    <link rel="stylesheet" href="{{asset('assets/back/css/fontiran.css')}}">
    <link rel="stylesheet" href="{{asset('assets/back/vendors/css/base/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/back/vendors/css/base/seenboard-1.0.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/back/css/animate/animate.min.css')}}">

    @yield('styles')

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body id="page-top">
<div id="preloader">
    <div class="canvas">
        <img src="{{asset('assets/back/img/logo.png')}}" alt="logo" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>

<div class="page">
    <header class="header">
        <nav class="navbar fixed-top">
            <!-- Begin Search Box-->
            <div class="search-box">
                <button class="dismiss"><i class="ion-close-round"></i></button>
                <form id="searchForm" action="#" role="search">
                    <input type="search" placeholder="جستجو کنید ..." class="form-control">
                </form>
            </div>
            <!-- End Search Box-->
            <!-- Begin Topbar -->
            <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
                <!-- Begin Logo -->
                <div class="navbar-header">
                    <a href="{{route('home')}}" class="navbar-brand">
                        <div class="brand-image brand-big">
                            <img src="{{asset('assets/back/img/logo-big.png')}}" alt="logo" class="logo-big">
                        </div>
                        <div class="brand-image brand-small">
                            <img src="{{asset('assets/back/img/logo.png')}}" alt="logo" class="logo-small">
                        </div>
                    </a>
                    <!-- Toggle Button -->
                    <a id="toggle-btn" href="#" class="menu-btn active">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                    <!-- End Toggle -->
                </div>
                <!-- End Logo -->

            </div>
            <!-- End Topbar -->
        </nav>
    </header>
    <div class="page-content d-flex align-items-stretch">
        @include('back.layouts.sidebar')
        <div class="content-inner">
            <div class="container-fluid">
                @yield('content')
            </div>
            <footer class="main-footer">
                <div class="row">
                    <div
                        class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-start justify-content-lg-start justify-content-md-start justify-content-center">
                        <p>طراحی شده در | استودیو سپید هنر</p>
                    </div>
                    <div
                        class="col-xl-6 col-lg-6 col-md-6 col-sm-12 d-flex align-items-center justify-content-xl-end justify-content-lg-end justify-content-md-end justify-content-center">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="documentation.html">مستندات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="changelog.html">آپدیت ها</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
<!-- Begin Vendor Js -->
<script src="{{asset('assets/back/vendors/js/base/jquery.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/js/base/core.min.js')}}"></script>
<script src="{{asset('assets/back/vendors/js/app/app.js')}}"></script>
<script src="{{asset('assets/back/vendors/js/noty/noty.min.js')}}"></script>
@include('back.layouts.notification')
<!-- End Vendor Js -->
@yield('scripts')
</body>
</html>
