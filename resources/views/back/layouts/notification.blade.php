<script>
    @if(session()->has('success'))
    new Noty({
        type: 'success', text: "{{ session()->get('success') }}", timeout: 3000, progressBar: true,
        closeWith: ['click'], layout: 'topLeft'
    }).show()
    @endif

    @if(session()->has('info'))
    new Noty({
        type: 'info', text: "{{ session()->get('info') }}", timeout: 3000, progressBar: true,
        closeWith: ['click'], layout: 'topLeft'
    }).show()
    @endif

    @if(session()->has('warning'))
    new Noty({
        type: 'warning', text: "{{ session()->get('warning') }}", timeout: 3000, progressBar: true,
        closeWith: ['click'], layout: 'topLeft'
    }).show()
    @endif

    @if(session()->has('error'))
    new Noty({
        type: 'error', text: "{{ session()->get('error') }}", timeout: 3000, progressBar: true,
        closeWith: ['click'], layout: 'topLeft'
    }).show()
    @endif

    @if(session()->has('notification'))
    new Noty({
        type: 'notification', text: "{{ session()->get('notification') }}", timeout: 3000, progressBar: true,
        closeWith: ['click'], layout: 'topLeft'
    }).show()
    @endif
</script>
