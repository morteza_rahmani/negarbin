@if ($paginator->hasPages())
    <div class="dataTables_paginate paging_simple_numbers"
         id="sorting-table_paginate">
        <ul class="pagination" style="justify-content:flex-end">
            @if ($paginator->onFirstPage())
                <li id="sorting-table_previous" class="paginate_button page-item previous disabled">
                    <a class="page-link">قبلی</a>
                </li>
            @else
                <li id="sorting-table_previous"
                    class="paginate_button page-item previous">
                    <a href="{{ $paginator->previousPageUrl() }}" class="page-link">قبلی</a>
                </li>
            @endif
            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="paginate_button page-item active">
                                <a href="#" class="page-link">{{$page}}</a>
                            </li>
                        @else
                            <li class="paginate_button page-item">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li id="sorting-table_next" class="paginate_button page-item next">
                    <a href="{{ $paginator->nextPageUrl() }}" class="page-link">بعدی</a>
                </li>
            @else
                <li id="sorting-table_next" class="paginate_button page-item next disabled">
                    <a class="page-link">بعدی</a>
                </li>
            @endif
        </ul>
    </div>
@endif
