@extends('front.layouts.app')

@section('content')
    <section class="single_content">
        <div class="container">
            <div class="page_content rounded shadow">
                @include('front.layouts.notification')
            </div>
        </div>
    </section>
@endsection
