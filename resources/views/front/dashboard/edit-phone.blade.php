@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <form action="{{route('dashboard.profile.updatePhone')}}" method="post">
                                @csrf
                                <div class="my-2">
                                    @include('front.layouts.notification')
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li class="m-0">{{ $error }}</li>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <label>
                                    <h6>ویرایش شماره موبایل : </h6>
                                    <div class="negarbin_dashboard_field">
                                        <input type="text" name="phone"
                                               value="{{auth()->user()->phone}}">
                                    </div>
                                </label>
                                <button type="submit" class="btn btn-primary">ارسال کد تایید</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
