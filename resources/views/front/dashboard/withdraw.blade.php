@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">

                @include('front.layouts.dashboard-sidebar')

                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">

                        <div class="description_product negarbin_withdraw_table negarbin_left_dashboard">
                            <h3>تسویه حساب</h3>
                            <br>
                            @if(isset($withdraw))
                                <div class="alert alert-{{$withdraw->colorStatus()}}" role="alert">
                                    وضعیت آخرین درخواست تسویه حساب شما ({{number_format($withdraw->amount)}} تومان)
                                    : {{$withdraw->translateStatus()}}
                                </div>
                            @endif
                            @include('front.layouts.notification')
                            <form action="{{route('dashboard.withdraw.store')}}" method="post">
                                @csrf
                                <table>
                                    <tr>
                                        <td>
                                            درآمد کل :
                                            <strong>{{number_format($user->total_revenue)}}</strong>
                                            تومان
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            موجودی فعلی :
                                            <strong>{{number_format($user->outstanding)}}</strong>
                                            تومان
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            تعداد کل فروش :
                                            <strong>{{number_format($user->products->sum('purchases'))}}</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Outstanding">
                                            <strong>
                                            </strong>
                                            <strong>تسویه نشده : </strong>
                                            <strong>{{number_format($user->outstanding)}}</strong>
                                            <strong>تومان</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Outstanding_button">
                                            <input type="submit" value="تسویه" class="btn btn-info">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
