@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            @include('front.layouts.notification')
                            <h3>محصولات</h3>
                            <table class="table-striped">
                                <thead>
                                <tr>
                                    <th>تصویر</th>
                                    <th>نام</th>
                                    <th>وضعیت</th>
                                    <th>قیمت</th>
                                    <th>خرید ها</th>
                                    <th>اقدامات</th>
                                    <th>تاریخ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="fes-product-list-td">
                                            <img width="100" height="100" alt="{{$product->name}}"
                                                 src="{{env('STORAGE_ADDRESS').env('PRODUCTS_SMALL').$product->thumbnail}}"
                                                 class="attachment-100x100 size-100x100 wp-post-image"/>
                                        </td>
                                        <td class="fes-product-list-td">{{$product->name}}</td>
                                        <td class="fes-product-list-td">
                                            <span class="download-status published">
                                                {{$product->statusText()}}
                                            </span>
                                        </td>
                                        <td class="fes-product-list-td">
                                            <span class="edd_price" id="edd_price_506">
                                                {{number_format($product->price)}} تومان
                                            </span>
                                        </td>
                                        <td class="fes-product-list-td">{{number_format($product->purchases)}}</td>
                                        <td class="fes-product-list-td">
                                            @if($product->status == 2)
                                                <a href="{{route('products.show', [$product->id, $product->slug])}}" title="نمایش"
                                                   class="edd-fes-action view-product-fes">نمایش</a>
                                            @endif
                                            <a href="{{route('dashboard.products.edit', $product->id)}}" title="ویرایش"
                                               class="edd-fes-action edit-product-fes">ویرایش</a>
                                            <a href="{{route('dashboard.products.remove', $product->id)}}" title="حذف"
                                               class="edd-fes-action edit-product-fes">حذف</a>
                                        </td>
                                        <td>
                                            <p>{{jdate($product->created_at)->format('Y/m/d')}}</p>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {{$products->links('front.layouts.pagination')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
