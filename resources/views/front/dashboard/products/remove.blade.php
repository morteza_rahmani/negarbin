@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <form action="{{route('dashboard.products.destroy', $product->id)}}" method="post">
                            @csrf
                            @method('delete')
                            <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                                <div class="">
                                    <p>برای حذف محصول اطمینان دارید؟ این اقدام غیر قابل بازگشت است.</p>
                                </div>
                                <br>
                                <h4>حذف محصول: #{{$product->id}} ({{$product->name}}) </h4>
                                <br>
                                <input type="submit" name="remove-product" value="حذف" class="btn btn-secondary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
