@extends('front.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/css/tagify.min.css')}}">
@endsection

@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <div class="my-2">
                                @include('front.layouts.notification')
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <h3>افزودن محصول</h3>
                            <br>
                            <div class="new_product">
                                <form action="{{route('dashboard.products.store')}}" method="post"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="negarbin_dashboard_field">
                                        <h6>عنوان <span class="text-danger">*</span></h6>
                                        <input type="text" name="product_name" value="{{old('product_name')}}"
                                               class="negarbin_dashboard_input @error('product_name'){{'is-invalid'}}@enderror">
                                        @error('product_name')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <!-- Uploader  -->
                                    <div class="negarbin_dashboard_field">
                                        <h6>آپلود تصویر شاخص <span class="text-danger">*</span></h6>
                                        <div id="file-upload-form" class="uploader">
                                            <input id="file-upload" type="file" name="thumbnail" accept="image/*"/>
                                            <label for="file-upload" id="file-drag">
                                                <img id="file-image" src="#" alt="Preview" class="hidden">
                                                <div id="start">
                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                                    <div>
                                                        <img src="{{asset('assets/front/images/montain.svg')}}"
                                                             width="80" height="80" alt="">
                                                    </div>
                                                    <div id="notimage" class="hidden">Please select an image</div>
                                                    <span id="file-upload-btn"
                                                          class="btn btn-primary">آپلود تصویر شاخص</span>
                                                </div>
                                                <div id="response" class="hidden">
                                                    <div id="messages"></div>
                                                    <progress class="progress" id="file-progress" value="0">
                                                        <span>0</span>%
                                                    </progress>
                                                </div>
                                            </label>
                                        </div>
                                        @error('thumbnail')
                                        <div class="invalid-feedback d-block">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="negarbin_dashboard_field">
                                        <h6>محتوا</h6>
                                        <textarea name="product_content" rows="20" cols="80" id="editor"
                                                  class="@error('product_content'){{'is-invalid'}}@enderror"
                                        >{{old('product_content')}}</textarea>
                                        @error('product_content')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="negarbin_dashboard_field">
                                        <div class="multiple">
                                            <div class="row">
                                                <div class="mb-3 col-md-6">
                                                    <h6>آپلود فایل <span class="text-danger">*</span></h6>
                                                    <input
                                                        class="form-control @error('file'){{'is-invalid'}}@enderror"
                                                        type="file" id="formFile" name="file" value="{{old('file')}}">
                                                    @error('file')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <h6>قیمت</h6>
                                                    <input type="text" name="product_price" placeholder="تومان"
                                                           value="{{old('product_price')}}"
                                                           class="@error('product_price'){{'is-invalid'}}@enderror">
                                                    <span class="text-muted" style="font-size: 13px;">
                                                        درصورتی که قصد دارید محصول رایگان عرضه شود فیلد قیمت را روی صفر قرار دهید.
                                                    </span>
                                                    @error('product_price')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(count($categories) > 0)
                                        <div class="negarbin_dashboard_field">
                                            <h6>دسته بندی</h6>
                                            <div class="negarbin_dashboard_category">
                                                <ul>
                                                    @foreach($categories as $category)
                                                        <li>
                                                            <label>
                                                                <input type="checkbox" name="categories[]"
                                                                       value="{{$category->id}}"
                                                                    {{(is_array(old('categories')) &&
                                                                    in_array($category->id, old('categories'))) ?
                                                                    'checked' : ''}}>
                                                                {{$category->name}}
                                                            </label>
                                                            @if(count($category->children) > 0)
                                                                <ul>
                                                                    @foreach($category->children as $child)
                                                                        <li>
                                                                            <label>
                                                                                <input type="checkbox"
                                                                                       name="categories[]"
                                                                                       value="{{$child->id}}"
                                                                                    {{(is_array(old('categories')) &&
                                                                                    in_array($child->id, old('categories'))) ?
                                                                                    'checked' : ''}}>
                                                                                {{$child->name}}
                                                                            </label>
                                                                            @if(count($child->children) > 0)
                                                                                <ul>
                                                                                    @foreach($child->children as $child)
                                                                                        <li>
                                                                                            <label>
                                                                                                <input type="checkbox"
                                                                                                       name="categories[]"
                                                                                                       value="{{$child->id}}"
                                                                                                    {{(is_array(old('categories')) &&
                                                                                                    in_array($child->id, old('categories'))) ?
                                                                                                    'checked' : ''}}>
                                                                                                {{$child->name}}
                                                                                            </label>
                                                                                        </li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            @endif
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="negarbin_dashboard_field excerptation">
                                        <h6>چکیده مطلب</h6>
                                        <textarea name="product_summary" rows="6"
                                                  placeholder="چکیده متن خود را وارد کنید"
                                                  class="@error('product_summary'){{'is-invalid'}}@enderror"
                                        >{{old('product_summary')}}</textarea>
                                        @error('product_summary')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="negarbin_dashboard_field excerptation">
                                        <div class="row">
                                            <div class="col-12">
                                                <h6>برچسب ها - حداکثر 6 تگ</h6>
                                                <div>
                                                    <input type="text" name="tags" id="tags" value="{{old('tags')}}"
                                                           class="tagify_input @error('tags'){{'is-invalid'}}@enderror"
                                                           placeholder="تگ مورد نظر را وارد کنید و اینتر بزنید">
                                                    @error('tags')
                                                    <div class="invalid-feedback">
                                                        {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="negarbin_dashboard_field">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h6>فرمت <span class="text-danger">*</span></h6>
                                                <input type="text" name="format" value="{{old('format')}}"
                                                       class="@error('format'){{'is-invalid'}}@enderror"
                                                       placeholder="فرمت">
                                                @error('format')
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="negarbin_dashboard_field negarbin_submit_form">
                                        <input type="submit" class="btn btn-success" value="ارسال"
                                               name="product_submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://unpkg.com/axios@0.21.1/dist/axios.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/front/js/ckeditor.js')}}"></script>
    <script src="{{asset('assets/front/js/jQuery.tagify.min.js')}}"></script>
    <script type="text/javascript">
        // Define an adapter to upload the files
        class MyUploadAdapter {
            constructor(loader) {
                // The file loader instance to use during the upload. It sounds scary but do not
                // worry — the loader will be passed into the adapter later on in this guide.
                this.loader = loader;

                // URL where to send files.
                this.url = '{{ route('ckeditor.upload') }}';
            }

            // Starts the upload process.
            upload() {
                return this.loader.file.then(
                    (file) =>
                        new Promise((resolve, reject) => {
                            this._initRequest();
                            this._initListeners(resolve, reject, file);
                            this._sendRequest(file);
                        })
                );
            }

            // Aborts the upload process.
            abort() {
                if (this.xhr) {
                    this.xhr.abort();
                }
            }

            // Initializes the XMLHttpRequest object using the URL passed to the constructor.
            _initRequest() {
                const xhr = (this.xhr = new XMLHttpRequest());
                // Note that your request may look different. It is up to you and your editor
                // integration to choose the right communication channel. This example uses
                // a POST request with JSON as a data structure but your configuration
                // could be different.
                // xhr.open('POST', this.url, true);
                xhr.open("POST", this.url, true);
                xhr.setRequestHeader("x-csrf-token", "{{ csrf_token() }}");
                xhr.responseType = "json";
            }

            // Initializes XMLHttpRequest listeners.
            _initListeners(resolve, reject, file) {
                const xhr = this.xhr;
                const loader = this.loader;
                const genericErrorText = `Couldn't upload file: ${file.name}.`;
                xhr.addEventListener("error", () => reject(genericErrorText));
                xhr.addEventListener("abort", () => reject());
                xhr.addEventListener("load", () => {
                    const response = xhr.response;
                    // This example assumes the XHR server's "response" object will come with
                    // an "error" which has its own "message" that can be passed to reject()
                    // in the upload promise.
                    //
                    // Your integration may handle upload errors in a different way so make sure
                    // it is done properly. The reject() function must be called when the upload fails.
                    if (!response || response.error) {
                        return reject(response && response.error ? response.error.message : genericErrorText);
                    }
                    // If the upload is successful, resolve the upload promise with an object containing
                    // at least the "default" URL, pointing to the image on the server.
                    // This URL will be used to display the image in the content. Learn more in the
                    // UploadAdapter#upload documentation.
                    resolve({
                        default: response.url,
                    });
                });
                // Upload progress when it is supported. The file loader has the #uploadTotal and #uploaded
                // properties which are used e.g. to display the upload progress bar in the editor
                // user interface.
                if (xhr.upload) {
                    xhr.upload.addEventListener("progress", (evt) => {
                        if (evt.lengthComputable) {
                            loader.uploadTotal = evt.total;
                            loader.uploaded = evt.loaded;
                        }
                    });
                }
            }

            // Prepares the data and sends the request.
            _sendRequest(file) {
                // Prepare the form data.
                const data = new FormData();
                data.append("upload", file);
                // Important note: This is the right place to implement security mechanisms
                // like authentication and CSRF protection. For instance, you can use
                // XMLHttpRequest.setRequestHeader() to set the request headers containing
                // the CSRF token generated earlier by your application.
                // Send the request.
                this.xhr.send(data);
            }

            // ...
        }

        function SimpleUploadAdapterPlugin(editor) {
            editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
                // Configure the URL to the upload script in your back-end here!
                return new MyUploadAdapter(loader);
            };
        }

        // CKEditor Rendering
        ClassicEditor
            .create(document.querySelector('#editor'), {
                extraPlugins: [SimpleUploadAdapterPlugin],
            })
            .then(editor => {
                window.editor = editor;
            })
            .catch(err => {
                console.error(err.stack);
            });

        // Tags
        let inputElm = document.querySelector("#tags"),
            tagify = new Tagify(inputElm, {
                maxTags: parseInt(inputElm.dataset.maxTags) || 6,
                whitelist: [],
                originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(',')
            })
        tagify.on('input', function (t) {
            var value = t.detail.value
            tagify.whitelist = null;
            if (value.length > 1) {
                axios
                    .get('{{route('suggestionTags')}}/?term=' + value)
                    .then(function (t) {
                        tagify.settings.whitelist = t.data // update inwhitelist Array in-place
                        tagify.dropdown.show(value)
                    })
                    .catch(function (inputElm) {
                    });
            } else {
                tagify.whitelist = null;
            }
        })
    </script>
@endsection
