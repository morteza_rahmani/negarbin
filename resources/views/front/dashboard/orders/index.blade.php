@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <h3>سفارشات </h3>
                            @if(count($invoices) > 0)
                                <table class="table fes-table table-condensed table-striped small_font_table">
                                    <thead>
                                    <tr>
                                        <th>ترتیب</th>
                                        <th>وضعیت</th>
                                        <th>جمع</th>
                                        <th>نمایش سفارش</th>
                                        <th>تاریخ</th>
                                    </tr>
                                    </thead>
                                    @foreach($invoices as $invoice)
                                        @php($payment = $invoice->payments()->latest()->first())
                                        <tr>
                                            <td>سفارش: #{{$invoice->id}}</td>
                                            <td>{{$payment->translateStatus()}}</td>
                                            <td>{{number_format($invoice->amount)}} تومان</td>
                                            <td>
                                                <a href="{{route('dashboard.orders.show', $payment->id)}}">نمایش</a>
                                            </td>
                                            <td>{{jdate($payment->created_at)->format('Y/m/d')}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{$invoices->links('front.layouts.pagination')}}
                            @else
                                <div class="alert alert-info">
                                    لیست سفارشات شما خالی است.
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
