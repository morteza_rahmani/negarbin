<div class="message_wrapper @if($ticket->is_response){{'message_wrapper_user'}}@endif">
    <div class="message_avatar">
        <div class="message_avatar_right">
            <img src="{{$ticket->user->adjusted_avatar}}"
                 alt="{{$ticket->user->avatar}}" loading="lazy">
        </div>
        <div class="message_avatar_left">
            <div class="user_name">{{$ticket->user->full_name}}</div>
            <div class="date">
                {{jdate($ticket->created_at)->format('Y/m/d (H:i)')}}
            </div>
        </div>
    </div>
    <div class="message message_author">
        <span>{!! nl2br($ticket->message) !!}</span>
    </div>
</div>
