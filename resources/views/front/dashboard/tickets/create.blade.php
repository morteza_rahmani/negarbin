@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <h3>ارسال تیکت</h3>
                            <br>
                            <form action="{{route('dashboard.support.store')}}" method="post">
                                @csrf
                                <div class="my-2">
                                    @include('front.layouts.notification')
                                </div>
                                <label class="negarbin_dashboard_field">
                                    <h6>عنوان :</h6>
                                    <input type="text" name="title" value="{{old('title')}}"
                                           class="@error('title'){{'is-invalid'}}@enderror">
                                    @error('title')
                                    <div class="invalid-feedback m-0">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </label>
                                <label class="negarbin_dashboard_field">
                                    <h6>متن :</h6>
                                    <textarea name="message" rows="8" cols="80"
                                              class="negarbin_dashboard_input @error('message'){{'is-invalid'}}@enderror"
                                    >{{old('message')}}</textarea>
                                    @error('message')
                                    <div class="invalid-feedback m-0">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </label>
                                <input type="submit" class="btn btn-success" value="ارسال">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
