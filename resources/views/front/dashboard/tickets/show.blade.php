@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">1
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <div class="my-2">
                                @include('front.layouts.notification')
                            </div>
                            <h3><span class="text-muted">#{{$ticket->id}}</span> {{$ticket->title}}</h3>
                            <br>
                            <div class="tickets">
                                @include('front.dashboard.tickets.chat-box')
                                @foreach($ticket->responses as $res)
                                    @include('front.dashboard.tickets.chat-box', ['ticket' => $res])
                                @endforeach
                            </div>
                            <section id="respond">
                                @if($ticket->status === 'بسته')
                                    <div class="alert alert-warning">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24"
                                             fill="none" stroke="#856404" stroke-width="2" stroke-linecap="round"
                                             stroke-linejoin="round">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <line x1="12" y1="8" x2="12" y2="12"></line>
                                            <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                        </svg>
                                        این تیکت بسته شده است. شما می توانید با ارسال یک پاسخ تیکت را مجددا باز نمایید.
                                    </div>
                                @endif
                                <h3>افزودن پاسخ</h3>
                                <br>
                                <form action="{{route('dashboard.support.update', $ticket->id)}}" method="post">
                                    @csrf
                                    <label class="negarbin_dashboard_field">
                                        <h6>متن :</h6>
                                        <textarea name="message" rows="8" cols="80"
                                                  class="negarbin_dashboard_input @error('message'){{'is-invalid'}}@enderror"
                                        >{{old('message')}}</textarea>
                                        @error('message')
                                        <div class="invalid-feedback m-0">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </label>
                                    <input type="submit" class="btn btn-success float-right mb-5" value="ارسال">
                                </form>
                                @if($ticket->status !== 'بسته')
                                    <form action="{{route('dashboard.support.close', $ticket->id)}}" method="post"
                                          class="float-left">
                                        @csrf
                                        <button class="btn btn-danger">بستن این تیکت</button>
                                    </form>
                                @endif
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
