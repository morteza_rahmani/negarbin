@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <h3>پشتیبانی</h3>
                            <div class="text-left">
                                <a href="{{route('dashboard.support.create')}}" class="btn btn-success pull-left">
                                    ارسال تیکت
                                </a>
                            </div>
                            <br>
                            @if(!empty($tickets[0]))
                                <table class="table fes-table table-condensed table-striped small_font_table">
                                    <thead>
                                    <tr>
                                        <th>شماره تیکت</th>
                                        <th>عنوان</th>
                                        <th>وضعیت</th>
                                        <th>آخرین بروزرسانی</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td>#{{$ticket->id}}</td>
                                            <td>{{$ticket->title}}</td>
                                            <td>{{$ticket->status}}</td>
                                            <td>{{jdate($ticket->updated_at)->format('Y/m/d - H:i')}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="{{route('dashboard.support.show', $ticket->id)}}"
                                                       class="btn btn-sm btn-info ml-1">مشاهده</a>
                                                    @if($ticket->status !== 'بسته')
                                                        <form action="{{route('dashboard.support.close', $ticket->id)}}"
                                                              method="post">
                                                            @csrf
                                                            <button type="submit" class="btn btn-sm btn-danger">بستن
                                                            </button>
                                                        </form>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$tickets->links('front.layouts.pagination')}}
                            @else
                                <div class="alert alert-info">
                                    شما هنوز تیکتی ثبت نکرده اید.
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
