@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            @if(isset($banners))
                                @php
                                    $i = 0;
                                    $i += isset($banners->banner1_image) ? 1 : 0;
                                    $i += isset($banners->banner2_image) ? 1 : 0;
                                    $i += isset($banners->banner3_image) ? 1 : 0;
                                @endphp
                                <div class="user_banner">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0"
                                                class="active"></li>
                                            @if($i > 1)
                                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                            @endif
                                            @if($i > 2)
                                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                            @endif
                                        </ol>
                                        <div class="carousel-inner">
                                            @if(isset($banners->banner1_image))
                                                <div class="carousel-item active">
                                                    <a href="{{$banners->banner1_link}}">
                                                        <img class="d-block w-100"
                                                             src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner1_image}}"
                                                             alt="First slide">
                                                    </a>
                                                </div>
                                            @endif
                                            @if(isset($banners->banner2_image))
                                                <div class="carousel-item">
                                                    <a href="{{$banners->banner2_link}}">
                                                        <img class="d-block w-100"
                                                             src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner2_image}}"
                                                             alt="Second slide">
                                                    </a>
                                                </div>
                                            @endif
                                            @if(isset($banners->banner3_image))
                                                <div class="carousel-item">
                                                    <a href="{{$banners->banner3_link}}">
                                                        <img class="d-block w-100"
                                                             src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner3_image}}"
                                                             alt="Third slide">
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                           data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                           data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            @endif
                            <p class="shop_address">
                                <img src="{{asset('assets/front/images/shops.svg')}}" width="20" height="20">
                                <strong>آدرس فروشگاه شما :</strong>
                                <a href="{{route('author.show', auth()->user()->username)}}">
                                    {{route('author.show', auth()->user()->username)}}
                                </a>
                            </p>
                            <br>
                            <p>
                                درآمد کل :
                                <strong>{{number_format(auth()->user()->total_revenue)}}</strong>
                                تومان
                            </p>
                            <p>
                                موجودی فعلی :
                                <strong>{{number_format(auth()->user()->outstanding)}}</strong>
                                تومان
                            </p>
                            <p>
                                تعداد کل فروش :
                                <strong>{{auth()->user()->products->sum('purchases')}}</strong>
                            </p>
                            <div class="chart">
                                <canvas id="myChart" width="400" height="200"></canvas>
                            </div>
                            @if(count($tickets) > 0)
                                <div class="last_tickets">
                                    <h5 style="margin-bottom:15px;">
                                        <strong>آخرین تیکت ها</strong>
                                    </h5>
                                    <table class="table fes-table table-condensed table-striped small_font_table">
                                        <thead>
                                        <tr>
                                            <th>شماره تیکت</th>
                                            <th>عنوان</th>
                                            <th>وضعیت</th>
                                            <th>آخرین بروزرسانی</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tickets as $ticket)
                                            <tr>
                                                <td>#{{$ticket->id}}</td>
                                                <td>{{$ticket->title}}</td>
                                                <td>{{$ticket->status}}</td>
                                                <td>{{jdate($ticket->updated_at)->format('Y/m/d - H:i')}}</td>
                                                <td>
                                                    <a href="{{route('dashboard.support.show', $ticket->id)}}"
                                                       class="btn btn-info">مشاهده</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/css/Chart.min.css')}}">
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('assets/front/js/Chart.min.js')}}"></script>
    <script type="text/javascript">
        var ctx = document.getElementById('myChart');
        Chart.defaults.global.defaultFontFamily = 'iranyekan';
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["{{$pDates[0]}}", "{{$pDates[1]}}", "{{$pDates[2]}}", "{{$pDates[3]}}", "{{$pDates[4]}}",
                    "{{$pDates[5]}}", "{{$pDates[6]}}", "{{$pDates[7]}}", "{{$pDates[8]}}", "{{$pDates[9]}}",
                    "{{$pDates[10]}}", "{{$pDates[11]}}"],
                datasets: [{
                    label: 'چارت درآمد ماهیانه',
                    borderWidth: 1,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    data: {{$pAmount}},
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
            }
        });
    </script>
@endsection
