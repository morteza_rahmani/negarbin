@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="fes-vendor-dashboard-wrap row">
                @include('front.layouts.dashboard-sidebar')
                <div class="fes-vendor-dashboard col-md-9">
                    <div class="single_description marg_top_20 rounded shadow">
                        <div class="description_product negarbin_all_vendor_dashboard negarbin_left_dashboard">
                            <form action="{{route('dashboard.profile.store')}}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="my-2">
                                    @include('front.layouts.notification')
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <h3>پروفایل</h3>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>
                                            <h6>نام : <span class="text-danger">*</span></h6>
                                            <div class="negarbin_dashboard_field">
                                                <input type="text" name="first_name"
                                                       value="{{auth()->user()->first_name}}">
                                            </div>
                                        </label>
                                        <label>
                                            <h6>ایمیل : <span class="text-danger">*</span></h6>
                                            <div class="negarbin_dashboard_field">
                                                <input type="email" name="email" value="{{auth()->user()->email}}"
                                                       placeholder="example@gmail.com">
                                            </div>
                                        </label>
                                        <div class="upload_avatar">
                                            <!-- Uploader  -->
                                            <div class="negarbin_dashboard_field">
                                                <h6>آپلود آواتار :</h6>
                                                <small class="text-muted">فایل شما نباید بزرگ تر از 120px باشد</small>
                                                <div id="file-upload-form" class="uploader">
                                                    <input id="file-upload" type="file" name="profile_avatar"
                                                           accept="image/*">
                                                    <label for="file-upload" id="file-drag">
                                                        @if(isset(auth()->user()->avatar))
                                                            <img id="file-image" alt="Preview"
                                                                 src="{{auth()->user()->adjusted_avatar}}">
                                                        @else
                                                            <img id="file-image" src="" alt="Preview" class="hidden">
                                                        @endif
                                                        <div id="start"
                                                             @if(isset(auth()->user()->avatar)) class="hidden" @endif>
                                                            <i class="fa fa-download" aria-hidden="true"></i>
                                                            <div>
                                                                <img width="80" height="80"
                                                                     alt="mountain"
                                                                     src="{{asset('assets/front/images/montain.svg')}}">
                                                            </div>
                                                            <div id="notimage" class="hidden">Please select an image
                                                            </div>
                                                            <span id="file-upload-btn" class="btn btn-primary">آپلود تصویر شاخص</span>
                                                        </div>
                                                        <div id="response" class="hidden">
                                                            <div id="messages"></div>
                                                            <progress class="progress" id="file-progress" value="0">
                                                                <span>0</span>%
                                                            </progress>
                                                        </div>
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>
                                            <h6>نام خانوادگی : </h6>
                                            <div class="negarbin_dashboard_field">
                                                <input type="text" name="last_name"
                                                       value="{{auth()->user()->last_name}}">
                                            </div>
                                        </label>
                                        <label>
                                            <h6>شماره همراه : <span class="text-danger">*</span></h6>
                                            <div class="negarbin_dashboard_field">
                                                <input type="tel" name="phone" placeholder="09123548962"
                                                       value="{{auth()->user()->phone}}" disabled>
                                                <a href="{{route('dashboard.profile.editPhone')}}"
                                                   target="_blank" style="font-size: 12px">
                                                    تغییر شماره همراه
                                                </a>
                                            </div>
                                        </label>
                                        <label>
                                            <h6>نام کاربری : <span class="text-danger">*</span></h6>
                                            <div class="negarbin_dashboard_field">
                                                <input type="tel" name="username" placeholder="username-123"
                                                       value="{{auth()->user()->username}}">
                                                <small class="text-muted">
                                                    فقط حروف انگلیسی، اعداد و خط تیره مجاز است.
                                                </small>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-md-12">
                                        <label>
                                            <h6>شماره شبا :</h6>
                                            <div class="negarbin_dashboard_field input-group flex-nowrap m-0">
                                                <input type="text" name="IBAN" class="dashboard-iban-field"
                                                       value="{{substr(auth()->user()->IBAN, 2)}}"
                                                       aria-describedby="ir-addon">
                                                <span class="input-group-text-dashboard" id="ir-addon">IR</span>
                                            </div>
                                            <small class="text-muted d-block">
                                                بدون فاصله و خط تیره
                                            </small>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="negarbin_dashboard_field">
                                        <input type="submit" name="profile_save" value="ذخیره"
                                               class="btn btn-success">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
