@extends('front.layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/css/justifiedGallery.css')}}">
@endsection
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 single_asl">
                    <div class="properties_item uploader_div user_intro shadow rounded">
                        <a href="{{route('author.show', $user->username)}}">
                            <div class="uploader_avatar shadow"
                                 style="background-image: url('{{$user->adjusted_avatar}}')">
                            </div>
                        </a>
                        <a href="{{route('author.show', $user->username)}}">
                            <div class="uploader_name">
                                <h5>{{$user->full_name}}</h5>
                            </div>
                        </a>
                        @if(count($user->awards) > 0)
                            <div class="awards">
                                @foreach($user->awards as $award)
                                    <span>
                                        <img src="{{env('STORAGE_ADDRESS').env('AWARDS').$award->icon}}"
                                             class="img-fluid" alt="{{$award->name}}" title="{{$award->name}}">
                                    </span>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="properties_item comment_buyes shadow rounded">
                        <div class="row text-center">
                            <div class="col-md-6">
                                <div class="negarbin_directionLtr">
                                    <strong class="purchase__count">
                                        <div class="single_desc">محصول</div>
                                        <div class="single_number">{{number_format($productsCount)}}</div>
                                        <i class="icon-box" style="transform:translateY(5px)"></i>
                                    </strong>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="negarbin_directionLtr">
                                    <strong class="purchase__comment">
                                        <div class="single_desc">فروش</div>
                                        <div
                                            class="single_number">{{abbreviateNumber($user->products->sum('purchases'))}}</div>
                                        <i class="icon-basket"></i>
                                    </strong>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="properties_item date_registered rouded shadow">

                        <h6>
                            <img src="{{asset('assets/front/images/schedule.svg')}}" width="25" alt="Date">
                            تاریخ عضویت :
                            <small>{{jdate($user->created_at)->format('%d %B %Y')}}</small>
                        </h6>

                    </div>


                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="single_description rounded shadow">

                        <div class="description_product">

                            <h3 class="text-right user_product_text">محصولات کاربر</h3>

                            @if(count($products) > 0)
                                <div class="original_filters original_author_filters">
                                    <form class="archive_sort">
                                        <a class="sort-by-btn cursor-pointer" data-parameter="pop">
                                            <label class="sub_title_item_button {{!isset($_GET['sort']) ||
                                               (isset($_GET['sort']) && ($_GET['sort'] != 'sale' && $_GET['sort'] != 'new')) ?
                                               'sub_title_item_button_active' : ''}}">
                                                پربازدیدترین ها
                                            </label>
                                        </a>
                                        <a class="sort-by-btn cursor-pointer" data-parameter="sale">
                                            <label class="sub_title_item_button {{isset($_GET['sort']) &&
                                               $_GET['sort'] == 'sale' ? 'sub_title_item_button_active' : ''}}">
                                                پرفروش ترین ها
                                            </label>
                                        </a>
                                        <a class="sort-by-btn cursor-pointer" data-parameter="new">
                                            <label class="sub_title_item_button {{isset($_GET['sort']) &&
                                               $_GET['sort'] == 'new' ? 'sub_title_item_button_active' : ''}}">
                                                جدیدترین ها
                                            </label>
                                        </a>
                                    </form>
                                </div>
                                <div id="basic" class="row">
                                    @foreach($products as $product)
                                        <div class="post_entry">
                                            @if($product->price > 0)
                                                <div id="top_post_info" class="top_post_info">
                                                    <div class="role">
                                                        <img src="{{asset('assets/front/images/crown.svg')}}" alt="">
                                                    </div>
                                                </div>
                                            @endif
                                            <a href="{{route('products.show', [$product->id, $product->slug])}}">
                                                <img
                                                    src="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}"
                                                    class="img-fluid rounded shadow">
                                                <div class="show_case_wrapper">
                                                    <div class="showcase_entry">
                                                        <figcaption>
                                                            <div class="samte_rast">
                                                                <h6>{{$product->name}}</h6>
                                                                <span class="avatar_small">
                                                                <img src="{{$user->adjusted_avatar}}" class="img-fluid"
                                                                     alt="">
                                                            </span>
                                                                <span class="author">{{$user->username}}</span>
                                                            </div>
                                                            <div class="samte_chap">
                                                            <span>
                                                                <i class="icon-download-index"></i>
                                                                {{abbreviateNumber($product->downloads)}}
                                                            </span>
                                                            </div>
                                                        </figcaption>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                                {{$products->links('front.layouts.pagination')}}
                            @else
                                <div class="my-4 text-center">
                                    <i class="icon-box text-muted" style="font-size: 100px"></i>
                                    <p class="mt-3 text-muted text-center">
                                        موردی یافت نشد!
                                    </p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.justifiedGallery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#basic").justifiedGallery({
                margins: 10,
                rowHeight: 150,
                rtl: false,
            });
            $('.sort-by-btn').on('click', function (e) {
                e.preventDefault()
                setUrl('sort', $(this).data('parameter'))
            })

            function setUrl(key, param) {
                let url = new URL($(location).attr('href'));
                url.searchParams.set(key, param);
                url.searchParams.set('page', 1);
                $(location).attr('href', url.href);
            }
        });
    </script>
@endsection
