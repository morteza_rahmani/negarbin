@extends('front.layouts.app')

@section('content')
    <section class="single_content">
        <div class="container">
            <div class="page_content rounded shadow">
                <div class="text-center">
                    @if($payment->status == 2)
                        <img src="{{asset('assets/front/images/rocket.svg')}}" width="70" alt="rocket">
                        <p class="rocket_title">پرداخت شما با موفقیت تکمیل شد!</p>
                    @elseif($payment->status == 1)
                        <img src="{{asset('assets/front/images/cancel-button.svg')}}" width="70" alt="cancel">
                        <p class="text-danger mt-2 mb-4">{{$payment->message ?? 'متاسفانه پرداخت شما تکمیل نشد!'}}</p>
                    @else
                        <img src="{{asset('assets/front/images/cancel-button.svg')}}" width="70" alt="cancel">
                        <p class="rocket_title">پرداخت انجام نشده است!</p>
                    @endif
                </div>

                <div class="edd_table">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>شناسه پرداخت :</td>
                            <td>{{$payment->transaction}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت پرداخت :</td>
                            <td>{{$payment->translateStatus()}}</td>
                        </tr>
                        <tr>
                            <td>روش پرداخت :</td>
                            <td>زرین پال</td>
                        </tr>
                        <tr>
                            <td>تاریخ :</td>
                            <td>{{jdate($payment->created_at)->format('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>کل :</td>
                            <td>{{number_format($payment->invoice->amount)}} تومان</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                @if($payment->status == \App\Models\Payment::STATUS_SUCCEED)
                    <h3 class="receipt_title">محصولات</h3>
                    <div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">نام</th>
                                <th scope="col">قیمت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payment->invoice->products as $product)
                                <tr>
                                    <td>
                                        <a href="{{route('products.show', [$product->id, $product->slug])}}"
                                            class="edd_purchase_receipt_product_name text-dark mb-2">
                                            {{$product->pivot->product_name}}
                                        </a>
                                        <ul class="edd_purchase_receipt_files">
                                            <li class="edd_download_file">
                                                <a href="{{route('products.download', $product->id)}}"
                                                   class="edd_download_file_link">
                                                    دانلود فایل
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        {{number_format($product->pivot->product_price)}} تومان
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
