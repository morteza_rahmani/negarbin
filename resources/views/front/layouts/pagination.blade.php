@if ($paginator->hasPages())
    <section id="pagination" class="pagination">
        @if ($paginator->onFirstPage())
            <span class="next page-numbers">
                <i class="icon-arrow-right"></i>
            </span>
        @else
            <a href="{{$paginator->previousPageUrl()}}" class="next page-numbers">
                <i class="icon-arrow-right"></i>
            </a>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <span class="page-numbers">{{ $element }}</span>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="#" onclick="return false" class="page-numbers current">{{$page}}</a>
                    @else
                        <a href="{{$url}}" class="page-numbers">{{$page}}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <a href="{{$paginator->nextPageUrl()}}" class="next page-numbers">
                <i class="icon-arrow-left"></i>
            </a>
        @else
            <span class="next page-numbers">
                <i class="icon-arrow-left"></i>
            </span>
        @endif
    </section>
@endif
