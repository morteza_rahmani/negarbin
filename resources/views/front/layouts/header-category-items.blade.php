<ul>
@foreach($category->children()->orderBy('priority')->get() as $child)
    <li><a href="{{route('products.archive', $child->slug)}}">{{$child->name}}<i class="icon-left"></i></a></li>
    @if(count($child->children) > 0)
        @foreach($child->children()->orderBy('priority')->get() as $child)
            <li><a href="{{route('products.archive', $child->slug)}}" class="category_small">{{$child->name}}</a></li>
        @endforeach
    @endif
@endforeach
</ul>
