<div class="header-menu" @if(Route::currentRouteName() != 'home')
style="position: relative;background-color: #252525;" @endif>
    <div class="primary-menu">
        <a href="{{route('home')}}" class="logo vector">
            <img src="{{asset('assets/front/images/logotype2.png')}}" class="img-fluid" alt="logo">
        </a>
        <ul class="primary-menu__top js-primary-menu" id="primary-menu">
            @foreach($categories as $category)
                @if(count($category->children) > 0)
                    <li data="{{'taxonom' . $category->id}}"
                        class="primary-menu-item" id="primary-menu-item-{{$category->id}}"
                        data-route="{{route('render-header-categories', $category->id)}}">
                        <a href="{{route('products.archive', $category->slug)}}">
                            <i class="{{$category->icon}}"></i>
                            {{$category->name}}
                        </a>
                        <div class="megamenu" id="{{'taxonom' . $category->id}}">
                            <div class="row megamenu-columns-wrapper">
                                <div class="col-md-3 col-sm-3">
                                    <ul class="brd_left h-100"></ul>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <ul class="brd_left h-100"></ul>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <ul class="brd_left h-100"></ul>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <ul class="h-100"></ul>
                                </div>
                            </div>
                        </div>
                    </li>
                @else
                    <li>
                        <a href="{{route('products.archive', $category->slug)}}">
                            <i class="{{$category->icon}}"></i>
                            {{$category->name}}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        <a id="nav-icon3">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div><!-- primary menu-->
    <div class="secondary-menu">
        <span class="log">
            @auth
                <a href="#" onclick="return false" id="menu_profile">
                    <text>
                        <i class="icon-angle-down" style="font-size: 9px"></i>
                        حساب کاربری
                    </text>
                    <span class="profile_header_avatar"
                          style="background-image: url({{auth()->user()->adjusted_avatar}})"></span>
                </a>
                <div class="dropdownlogin" id="dropdownlogin">
                <ul>
                    <li>
                        {{auth()->user()->full_name}}
                    </li>
                    @if(auth()->user()->is_admin)
                        <li><a href="{{route('admin.dashboard.index')}}">پنل ادمین</a></li>
                    @endif
                    <li><a href="{{route('dashboard.index')}}">داشبورد</a></li>
                    <li><a href="{{route('dashboard.products.create')}}">آپلود فایل</a></li>
                    <li><a href="{{route('dashboard.profile.index')}}">تنظیمات کاربری</a></li>
                    <li>
                        <form action="{{route('logout')}}" method="post">
                            @csrf
                            <button type="submit" class="btn shadow-none w-100 text-right" style="padding: 10px">
                                خروج
                            </button>
                        </form>
                    </li>
                </ul>
              </div>
            @endauth
            @guest
                <a class="cursor-pointer showLoginPageButton" data-route="{{route('ajax.login.view')}}">
                    <i class="icon-dash-user"></i>
                    <text>ورود / عضویت</text>
                </a>
            @endguest
        </span>
        @if(auth()->check())
            <span class="access_mobile" id="access_mobile">
                <a href="{{route('dashboard.index')}}">
                    <span class="profile_header_avatar"
                          style="background-image: url({{auth()->user()->adjusted_avatar}})"></span>
                </a>
            </span>
            <a href="{{route('cart')}}" class="menu-button button packages_button">
                <span class="n_packages_prices">
                    <i class="icon-shopping-basket">
                        <div class="n_quantity">{{auth()->user()->cartProducts->count()}}</div>
                    </i>
                </span>
            </a>
        @else
            <span class="access_mobile" id="access_mobile">
                <a href="#" class="showLoginPageButton" onclick="return false"
                   data-route="{{route('ajax.login.view')}}">
                    <i class="icon-user1"></i>
                </a>
            </span>
            <a href="#" class="menu-button button packages_button showLoginPageButton" onclick="return false"
               data-route="{{route('ajax.login.view')}}">
                <span class="n_packages_prices">
                    <i class="icon-shopping-basket"></i>
                </span>
            </a>
        @endif
    </div>
    <!-- navabar for mobile -->
    <div id="black-box" class="black-box shadow rounded">
        <h6>دسته بندی کالاها</h6>
        <ul>
            @foreach($categories as $category)
                @if(count($category->children) > 0)
                    <li class="burger_item burger_item-with-sub">
                        <a href="{{route('products.archive', $category->slug)}}" onclick="return false">
                            {{$category->name}}
                        </a>
                        <ul class="init_category">
                            @foreach($category->children()->orderBy('priority')->get() as $child)
                                @if(count($child->children) > 0)
                                    <li class="burger_item_level_2 has_sub_group">
                                        <a href="{{route('products.archive', $child->slug)}}"
                                           onclick="return false">{{$child->name}}</a>
                                        <ul class="group_level_3">
                                            @foreach($child->children()->orderBy('priority')->get() as $child)
                                                <li>
                                                    <a href="{{route('products.archive', $child->slug)}}">
                                                        {{$child->name}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li class="burger_item_level_2">
                                        <a href="{{route('products.archive', $child->slug)}}">{{$child->name}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li class="burger_item">
                        <a href="{{route('products.archive', $category->slug)}}">
                            {{$category->name}}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
