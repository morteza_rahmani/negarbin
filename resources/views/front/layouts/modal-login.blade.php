@if($page == 'login')
    <div class="">
        <h5>ورود</h5>
    </div>
    <div class="">
        <small>
            شماره موبایل یا پست الکترونیک خود را وارد کنید
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="doLoginForm" data-route="{{route('ajax.login')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="text" name="login" placeholder="شماره همراه خود را وارد کنید ..."
                   class="form-control mb-2" required>
            <input type="password" name="password" placeholder="کلمه عبور"
                   class="form-control" required>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="ورود">
        </div>
    </form>
    <div class="go_to_register_page d-flex justify-content-between">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.register.view')}}">ایجاد حساب کاربری جدید</a>
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.change_password_view')}}">فراموشی رمز عبور</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@elseif($page == 'register')
    <div class="">
        <h5>ثبت نام</h5>
    </div>
    <div class="">
        <small>
            شماره موبایل خود را وارد کنید
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="verifyPhoneForm" data-route="{{route('ajax.send_verify_code')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="text" name="phone" placeholder="شماره همراه خود را وارد کنید ..."
                   class="form-control mb-2" required>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="ارسال کد تایید">
        </div>
    </form>
    <div class="go_to_register_page">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.login.view')}}">ورود به حساب کاربری</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@elseif($page == 'verify')
    <div class="">
        <h5>ثبت نام</h5>
    </div>
    <div class="">
        <small>
            کد تایید ارسال شده به شماره موبایل خود را وارد کنید.
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="doVerifyForm" data-route="{{route('ajax.verify')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="text" name="verify_code" placeholder="کد تایید" class="form-control mb-2" required>
            <div class="verify_phone-countdown"></div>
            <div class="send-verify_code-again disable-content" data-phone=""
                 data-route="{{route('ajax.send_verify_code')}}">
                <a href="#" onclick="return false">ارسال مجدد کد</a>
            </div>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="تایید شماره تلفن">
        </div>
    </form>
    <div class="go_to_register_page">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.login.view')}}">ورود به حساب کاربری</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@elseif($page == 'password')
    <div class="">
        <h5>ثبت نام</h5>
    </div>
    <div class="">
        <small>
            رمز عبور دلخواه خود را وارد کنید.
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="registerPasswordForm" data-route="{{route('ajax.register')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="password" name="password" placeholder="رمز عبور"
                   class="form-control mb-2" required>
            <input type="password" name="password_confirmation" placeholder="تکرار رمز عبور"
                   class="form-control mb-2" required>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="ثبت نام">
        </div>
    </form>
    <div class="go_to_register_page">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.login.view')}}">ورود به حساب کاربری</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@elseif($page == 'change_password_1')
    <div class="">
        <h5>فراموشی رمز عبور</h5>
    </div>
    <div class="">
        <small>
            شماره موبایل خود را وارد کنید.
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="verifyPhoneForm" data-route="{{route('ajax.change_password_1')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="text" name="phone" placeholder="شماره موبایل خود را وارد کنید"
                   class="form-control mb-2" required>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="ارسال کد تایید">
        </div>
    </form>
    <div class="go_to_register_page">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.login.view')}}">ورود به حساب کاربری</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@elseif($page == 'change_password_2')
    <div class="">
        <h5>فراموشی رمز عبور</h5>
    </div>
    <div class="">
        <small>
            کد تایید ارسال شده به شماره موبایل خود را وارد کنید.
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="changePasswordStep2" data-route="{{route('ajax.change_password_2')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="text" name="verify_code" placeholder="کد تایید" class="form-control mb-2" required>
            <div class="verify_phone-countdown"></div>
            <div class="send-verify_code-again disable-content" data-phone=""
                 data-route="{{route('ajax.change_password_1')}}">
                <a href="#" onclick="return false">ارسال مجدد کد</a>
            </div>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="تایید">
        </div>
    </form>
    <div class="go_to_register_page">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.login.view')}}">ورود به حساب کاربری</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@elseif($page == 'change_password_3')
    <div class="">
        <h5>فراموشی رمز عبور</h5>
    </div>
    <div class="">
        <small>
            رمز عبور جدید خود را وارد کنید
        </small>
    </div>
    <div class="response-data" style="display: none"></div>
    <form id="changePasswordStep3" data-route="{{route('ajax.change_password_3')}}">
        <div class="inputGroup-sizing-default login_inputs">
            <input type="password" name="password" placeholder="رمز عبور جدید"
                   class="form-control mb-2" required>
            <input type="password" name="password_confirmation" placeholder="تکرار رمز عبور جدید"
                   class="form-control mb-2" required>
        </div>
        <div class="submit_mobile_number_login">
            <input type="submit" class="btn btn-danger" value="تغییر رمز عبور">
        </div>
    </form>
    <div class="go_to_register_page">
        <a class="cursor-pointer text-primary" id="changeLoginPageButton"
           data-route="{{route('ajax.login.view')}}">ورود به حساب کاربری</a>
    </div>
    <div class="login_privacy">
        با ورود و یا ثبت نام در نگاربین شما شرایط و قوانین استفاده از سرویس های سایت نگاربین و
        قوانین حریم خصوصی آن را میپذیرید
    </div>
@endif
