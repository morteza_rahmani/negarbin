@foreach($replies as $reply)
    @if(empty($reply->user_id))
        @php
            $author = $reply->name;
            $avatar = asset('assets/front/images/no-photo.jpg');
        @endphp
    @else
        @php
            $author = $reply->user->full_name;
            $avatar = $reply->user->adjusted_avatar;
        @endphp
    @endif
    <li class="comment" id="comment-{{$reply->id}}">
        <div id="div-comment-{{$reply->id}}" class="comment-body">
            <div class="comment-author vcard">
                <img height="32" width="32" class="avatar" alt=""
                     src="{{$avatar}}">
                <cite class="fn">{{$author}}</cite>
            </div>
            <div class="comment-meta commentmetadata">
                <a href="#" onclick="return false">
                    {{jdate($reply->created_at)->format('%d %B %Y ساعت H:i')}}
                </a>
            </div>
            <p>
                {!! $reply->content !!}
            </p>
            <div class="reply">
                <a class="comment-reply-link" data-comment-ID="{{$reply->id}}" data-comment-author="{{$author}}"
                   href="#respond">پاسخ</a>
            </div>
        </div>
    </li>
    @include('front.layouts.replies', ['replies' => $reply->replies()->where('status', 1)->latest()->get()])
@endforeach
