@php($cr = Route::currentRouteName())
<!-- start right sidebar -->
<div class="col-md-3">
    <div class="original_properties rounded-lg shadow">
        <div class="author_info">
            <div class="author_img">
                <img src="{{auth()->user()->adjusted_avatar}}"
                     class="img-fluid rounded-circle" alt="no-photo.jpg">
                <span class="hello_msg">سلام</span>
            </div>
            <div class="author_name">
                {{auth()->user()->full_name}}
            </div>
        </div>
        <div class="fes-vendor-menu">
            <ul>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab {{$cr == 'dashboard.index' ? 'active' : ''}}">
                    <a href="{{route('dashboard.index')}}">
                        <span class="menu_icon_wrapper">
                            <i class="icon-dash-home icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">داشبورد</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab
                    {{$cr == 'dashboard.products.index' ? 'active' : ''}}">
                    <a href="{{route('dashboard.products.index')}}">
                        <span class="menu_icon_wrapper">
                            <i class="icon-dash-list icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">محصولات</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab
                           {{$cr == 'dashboard.products.create' ? 'active' : ''}}">
                    <a href="{{route('dashboard.products.create')}}">
                        <span class="menu_icon_wrapper">
                            <i class="icon-dash-pencil icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">افزودن محصول</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab
                           {{$cr == 'dashboard.withdraw.index' ? 'active' : ''}}">
                    <a href="{{route('dashboard.withdraw.index')}}">
                        <span class="menu_icon_wrapper">
                            <i class="icon-dash-earnings icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">تسویه حساب</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab
                           {{$cr == 'dashboard.orders.index' || $cr == 'dashboard.orders.show' ? 'active' : ''}}">
                    <a href="{{route('dashboard.orders.index')}}">
                        <span class="menu_icon_wrapper">
                              <i class="icon-dash-gift icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">سفارشات</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab
                           {{$cr == 'dashboard.profile.index' ? 'active' : ''}}">
                    <a href="{{route('dashboard.profile.index')}}">
                        <span class="menu_icon_wrapper">
                            <i class="icon-dash-user icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">پروفایل</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab
                           {{$cr == 'dashboard.support.index' || $cr == 'dashboard.support.show' ? 'active' : ''}}">
                    <a href="{{route('dashboard.support.index')}}">
                        <span class="menu_icon_wrapper">
                              <i class="icon-envelope icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">پشتیبانی</span>
                    </a>
                </li>
                <li class="fes-vendor-menu-tab fes-vendor-dashboard-tab">
                    <form action="{{route('logout')}}" method="post" onClick="submit()">
                        @csrf
                        <span class="menu_icon_wrapper">
                            <i class="icon-dash-off icon-black"></i>
                        </span>
                        <span class="hidden-phone hidden-tablet menu_name">خروج</span>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end right sidebar -->
