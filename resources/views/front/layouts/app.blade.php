<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <link rel="shortcut icon" href="{{asset('assets/front/images/faveicon.ico')}}" title="Favicon">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/style.min.css')}}">
    @yield('styles')
    <title>{{$page_title}} | {{config('app.name')}}</title>
</head>
<body>
<header>
@include('front.layouts.header')
<!-- Modal -->
    <div class="modal fade" id="loginModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="loginModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal_content">
                        <div class="login_close">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="text-center login_logo">
                            <img src="{{asset('assets/front/images/kolelogo.png')}}" alt="logo">
                        </div>
                        <div class="text-right modal_desc">
                            <div id="modal_desc_content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Route::currentRouteName() == 'home')
        <div class="header-search hero-image withpopular negarbin_directionLtr"
             style="background-image: url('{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner1_image}}');">
            <div class="container">
                <div class="row">
                    <div class="offset-md-0 col-md-12 offset-lg-2 col-lg-8 col-xs-12 col-sm-12">
                        <div class="search-form">
                            <form class="general_search" action="{{route('products.index')}}" method="get" role="form">
                                <div class="general_search_field">
                                    <input id="s" name="s" type="search"
                                           placeholder="تصویر مورد نظر خود را جستجو کنید ...">
                                </div>
                                <div class="general_search_selection dropdown">
                                    <a id="allcontent" class="cursor-pointer">
                                        <text>جستجو در</text>
                                        <i class="icon-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menus">
                                        <ul>
                                            <li>
                                                <input id="input0" type="checkbox" name="category[]"
                                                       value="-1" checked>
                                                <label for="input0">
                                                    همه موارد
                                                </label>
                                            </li>
                                            @foreach($categories as $category)
                                                <li>
                                                    <input id="input{{$category->id}}" type="checkbox" name="category"
                                                           value="{{$category->id}}">
                                                    <label for="input{{$category->id}}">
                                                        {{$category->name}}
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="general_search_submit">
                                    <button type="submit" class="anim">
                                        <i class="icon-search"></i>
                                    </button>
                                </div>
                                <input type="hidden" name="sort" value="pop">
                                <input type="hidden" name="page" value="1">
                            </form>
                            @if(isset($searches->keyword1) || isset($searches->keyword2) || isset($searches->keyword3)
                                || isset($searches->keyword4) || isset($searches->keyword5)
                                || isset($searches->keyword6) || isset($searches->keyword7))
                                <div class="header-popular-searches">
                                    @for($i = 1; $i <= 7; $i++)
                                        @if($searches->value("keyword{$i}") !== null)
                                            <a href="{{route('products.index')}}?s={{$searches->value("keyword{$i}")}}">
                                                {{$searches->value("keyword{$i}")}}
                                            </a>
                                        @endif
                                    @endfor
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="headline">
                <div class="author">
                    <a href="{{$banners->banner1_user_url}}">
                        <span class="authorname">
                            Picture by
                            <strong>{{$banners->banner1_username}}</strong>
                        </span>
                        <span class="authorthumb"
                              style="background-image:url({{env('STORAGE_ADDRESS').env('AVATARS').$banners->banner1_user_avatar}})">
                        </span>
                    </a>
                </div>
                <div class="h2">
                    بزرگترین بازار اینترنتی فایل های گرافیکی در ایران با بیش از 3 هزار فایل و 1000 کاربر فعال
                </div>
            </div>
        </div>
    @else
        <section class="find-search">
            <div class="container">
                <div class="row negarbin_directionLtr">
                    <div class="cool_search offset-md-1 col-md-10 offset-lg-2 col-lg-8 col-xs-12">
                        <div class="find-form clearfix">
                            <form id="findfrm" action="{{route('products.index')}}" role="form" method="get">
                                <input type="search" name="s" id="fnd_field"
                                       class="ui-autocomplete-input text-input text-right" placeholder="جستجو کنید ..."
                                       value="{{$_GET['s'] ?? ''}}">
                                <div id="formbuttons" class="find-form-buttons">
                                    <button class="find-anim done" type="submit">
                                        <i class="icon-search"></i>
                                    </button>

                                    <div class="search-dropdown dropdown">
                                        <a id="allcontentsrh" class="selection cursor-pointer">
                                            <i class="icon-down"></i>
                                            <em>جستجو در</em>
                                        </a>
                                        <div class="dropdownn-menus text-right">
                                            <ul>
                                                <li>
                                                    <label for="innerinput0">
                                                        همه موارد
                                                        <input type="checkbox" name="category[]" id="innerinput0"
                                                               value="-1"
                                                            {{isset($_GET['category']) &&
                                                            in_array(-1, $_GET['category']) ?
                                                            'checked' : ''}}>
                                                    </label>
                                                </li>
                                                @foreach($categories as $category)
                                                    <li>
                                                        <label for="input{{$category->id}}">
                                                            {{$category->name}}
                                                            <input id="input{{$category->id}}" type="checkbox"
                                                                   name="category[]" value="{{$category->id}}"
                                                                {{isset($_GET['category']) &&
                                                                in_array($category->id, $_GET['category']) ?
                                                                'checked' : ''}}>
                                                        </label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="sort" value="pop">
                                <input type="hidden" name="page" value="1">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
</header>
<main>
    @yield('content')
</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer__cta">
                    <div class="footer__cta-content">
                        <div class="footer__cta-content-container">
                            <div class="footer__cta-logo-container">
                                <div class="footer-icon-logo">
                                    <img src="{{asset('assets/front/images/negarbin_logo.png')}}" class="img-fluid"
                                         alt="نگاربین">
                                </div>
                            </div>
                            <article class="footer__cta-content-text">
                                <h3>فروشنده نگاربین شو</h3>
                                <p>محصولات گرافیکی خودتو توی نگاربین بذار و بفروش !</p>
                            </article>
                        </div>
                        <div class="footer__cta-btn-container">
                            <a href="#" class="footer-cta-btn">فروشنده شو</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <section class="footer__widget-section">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="footer__widget single-widget">
                        <h3 class="footer__widget-title">اطلاع رسانی</h3>
                        <div class="footer__widget-content">
                            <nav class="footer__widget-nav">
                                <ul class="footer_widget-menu">
                                    <li>
                                        <a href="{{route('blog.index')}}">وبلاگ</a>
                                    </li>
                                    <li>
                                        <a href="#">ویژگی های سایت</a>
                                    </li>
                                    <li>
                                        <a href="#">شبکه های اجتماعی</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="footer__widget single-widget">
                        <h3 class="footer__widget-title">خدمات مشتریان</h3>
                        <div class="footer__widget-content">
                            <nav class="footer__widget-nav">
                                <ul class="footer_widget-menu">
                                    <li>
                                        <a href="#">راهنمای خرید</a>
                                    </li>
                                    <li>
                                        <a href="#">سوالات متداول</a>
                                    </li>
                                    <li>
                                        <a href="#">قوانین و مقررات</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="footer__widget single-widget">
                        <h3 class="footer__widget-title">خدمات فروشندگان</h3>
                        <div class="footer__widget-content">
                            <nav class="footer__widget-nav">
                                <ul class="footer_widget-menu">
                                    <li>
                                        <a href="#">عضویت فروشندگان</a>
                                    </li>
                                    <li>
                                        <a href="#">قوانین فروش محصول</a>
                                    </li>
                                    <li>
                                        <a href="#">کمیسیون ها</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-6">
                    <div class="footer__widget single-widget">
                        <h3 class="footer__widget-title">نگاربین</h3>
                        <div class="footer__widget-content">
                            <nav class="footer__widget-nav">
                                <ul class="footer_widget-menu">
                                    <li>
                                        <a href="#">درباره ما</a>
                                    </li>
                                    <li>
                                        <a href="#">تماس با ما</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="row footer__copyright-section">
            <div class="col-md-12">
                <a href="#" class="btn btn-backtotop">
                    <i class="icon-up"></i>
                </a>
                <p class="footer__copyright">© استفاده تجاری از تمام محصولات نگاربین ممنوع بوده و پیگرد قانونی خواهد
                    داشت</p>
            </div>
        </section>
    </div>
</footer>

<script type="text/javascript" src="{{asset('assets/front/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/index.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/ajax.js')}}"></script>
@yield('scripts')
</body>
</html>
