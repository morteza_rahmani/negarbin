@extends('front.layouts.app')

@section('meta')
    <meta name="description" content="{{$product->summary}}">
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/css/justifiedGallery.css')}}">
@endsection

@section('content')
    <section class="single_content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 single_asl p-sidebar">
                    @include('front.layouts.notification')
                    <div class="original_properties rounded-lg shadow">
                        <h3>{{$product->name}}</h3>
                        <div class="n_breadcrumb">
                            <span>
                                <a href="{{route('home')}}">خانه</a>
                            </span>
                            @if(isset($category))
                                <span>
                                    <a href="{{route('products.archive', $category->slug)}}">
                                        {{$category->name}}
                                    </a>
                                </span>
                                @if($category->children()->whereIn('id', $category_ids)->count() > 0)
                                    @foreach($category->children()->whereIn('id', $category_ids)->limit(1)->get() as $child)
                                        <span>
                                            <a href="{{route('products.archive', $child->slug)}}">
                                                {{$child->name}}
                                            </a>
                                        </span>
                                    @endforeach
                                    @if($child->children()->whereIn('id', $category_ids)->count() > 0)
                                        @foreach($child->children()->whereIn('id', $category_ids)->limit(1)->get() as $child)
                                            <span>
                                                <a href="{{route('products.archive', $child->slug)}}">
                                                    {{$child->name}}
                                                </a>
                                            </span>
                                        @endforeach
                                    @endif
                                @endif
                            @endif
                            <span>{{$product->name}}</span>
                        </div>
                        <h6>{!! $product->summary !!}</h6>
                        <div class="fields_product">
                            <div class="field_item">
                                <i class="icon-badge"></i>
                                <span class="question">کد محصول : </span>
                                <span class="answer">{{$product->id}}</span>
                            </div>
                            <div class="field_item">
                                <i class="icon-doc"></i>
                                <span class="question">فرمت : </span>
                                <span class="answer">{{$product->format}}</span>
                            </div>
                            <div class="field_item">
                                <i class="icon-folder"></i>
                                <span class="question">حجم فایل : </span>
                                <span class="answer">{{round(($product->size / 1024 / 1024), 1) . 'MB'}}</span>
                            </div>
                            <div class="field_item">
                                <i class="icon-calendar"></i>
                                <span class="question">تاریخ انتشار : </span>
                                <span class="answer">{{jdate($product->created_at)->format('Y/m/d')}}</span>
                            </div>
                            <div class="field_item">
                                <img src="{{asset('assets/front/images/coin.svg')}}" alt="">
                                <span class="question" style="font-weight:bold;color:#60569F;">قیمت : </span>
                                <span class="answer" style="font-weight:bold;color:#60569F;">
                                    @if($product->price > 0)
                                        {{number_format($product->price)}} تومان
                                    @else
                                        رایگان
                                    @endif
                                </span>
                            </div>
                        </div>
                        <a href="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}">
                            <div class="single_button single_overview rounded">
                                <div class="vip">پیش نمایش تصویر</div>
                            </div>
                        </a>
                        @if($product->price == 0 || (Auth::check() && (Auth::user()->purchased($product->id) || Auth::id() == $product->user_id || Auth::user()->is_admin)))
                            <a href="{{route('products.download', $product->id)}}" target="_blank"
                               class="edd_free_link button blue single_button rounded shadow">
                                <span class="edd-add-to-cart-label">دانلود</span>
                            </a>
                        @else
                            @if(auth()->check() && $product->inCart())
                                <a href="{{route('cart')}}"
                                   class="edd_vip button blue single_button buy_padding rounded shadow border-0 w-100">
                                    <i class="icon-shopping-basket"></i>
                                    <span class="edd-add-to-cart-label">موجود در سبد خرید</span>
                                </a>
                            @else
                                <form action="{{route('addToCart', $product->id)}}" method="post">
                                    @csrf
                                    <button type="submit"
                                            class="edd_vip button blue single_button buy_padding rounded shadow border-0 w-100">
                                        <i class="icon-shopping-basket"></i>
                                        <span class="edd-add-to-cart-label">افزودن به سبد خرید</span>
                                    </button>
                                </form>
                            @endif
                        @endif
                    </div>
                    <div class="properties_item uploader_div shadow rounded">
                        <a href="{{route('author.show', $product->user->username)}}">
                            <div class="uploader_avatar shadow"
                                 style="background-image:url('{{$product->user->adjusted_avatar}}')"></div>
                        </a>
                        <a href="{{route('author.show', $product->user->username)}}">
                            <div class="uploader_name">
                                <h5>{{$product->user->full_name}}</h5>
                            </div>
                        </a>
                        @if(count($product->user->awards) > 0)
                            <div class="awards">
                                @foreach($product->user->awards as $award)
                                    <span>
                                        <img src="{{env('STORAGE_ADDRESS').env('AWARDS').$award->icon}}"
                                             class="img-fluid" alt="{{$award->name}}" title="{{$award->name}}">
                                    </span>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div class="properties_item comment_buyes shadow rounded">
                        <div class="row text-center">
                            <div class="col-md-6">
                                <div class="negarbin_directionLtr">
                                    <strong class="purchase__count">
                                        @if($product->price > 0)
                                            <div class="single_desc">خرید</div>
                                            <div class="single_number">{{abbreviateNumber($product->purchases)}}</div>
                                            <i class="icon-basket"></i>
                                        @else
                                            <div class="single_desc">دانلود</div>
                                            <div class="single_number">{{abbreviateNumber($product->downloads)}}</div>
                                            <i class="icon-download-index"></i>
                                        @endif
                                    </strong>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="negarbin_directionLtr">
                                    <strong class="purchase__comment">
                                        <div class="single_desc">بازدید</div>
                                        <div class="single_number">{{abbreviateNumber($product->views)}}</div>
                                        <i class="icon-view"></i>
                                    </strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="properties_item rouded shadow">
                        <div class=" product_rating ">
                            <div>
                                <h6>امتیاز خریداران : </h6>
                            </div>
                            <div class="rate">
                                <?php
                                for ($x = 1; $x <= $product->score; $x++) {
                                    echo '<span class="active_rate"></span>';
                                }
                                while ($x <= 5) {
                                    echo '<span></span>';
                                    $x++;
                                }
                                ?>
                            </div>
                        </div>
                        <div class="miangin_text">
                            <small>میانگین {{$product->score}} امتیاز، براساس {{$product->votes}} رای</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 p-main">
                    <div class="single_description rounded shadow">
                        <div class="single_thumbnail">
                            <img src="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}" class="img-fluid"
                                 alt="{{$product->name}}">
                        </div>
                        <div class="description_product">
                            <div class="stars">
                                <?php
                                for ($x = 1; $x <= $product->score; $x++) {
                                    echo '<span class="active_rate"></span>';
                                }
                                while ($x <= 5) {
                                    echo '<span></span>';
                                    $x++;
                                }
                                ?>
                            </div>
                            {!! $product->content !!}
                            <div class="suggested">
                                @foreach($product->tags as $tag)
                                    <a class="cursor-pointer">
                                        {{$tag->title}}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="comments-section" class="single_comment shadow rounded">
                        <h2 class="comment_title">
                            <i class="icon-bubbles3"></i>
                            نظرات
                        </h2>
                        <ol class="commentlist">
                            @foreach($comments as $comment)
                                @if(empty($comment->user_id))
                                    @php
                                        $author = $comment->name;
                                        $avatar = asset('assets/front/images/no-photo.jpg');
                                    @endphp
                                @else
                                    @php
                                        $author = $comment->user->full_name;
                                        $avatar = $comment->user->adjusted_avatar;
                                    @endphp
                                @endif
                                <li class="comment" id="comment-{{$comment->id}}">
                                    <div class="comment-body">
                                        <div class="comment-author vcard">
                                            <img src="{{$avatar}}" height="32" width="32" class="avatar"
                                                 alt="avatar">
                                            <cite class="fn">{{$author}}</cite>
                                        </div>
                                        <div class="comment-meta commentmetadata">
                                            <a href="#" onclick="return false">
                                                {{jdate($comment->created_at)->format('%d %B %Y ساعت H:i')}}
                                            </a>
                                        </div>
                                        <p>{{$comment->content}}</p>
                                        <div class="reply">
                                            <a class="comment-reply-link" data-comment-ID="{{$comment->id}}"
                                               data-comment-author="{{$author}}" href="#respond">
                                                پاسخ
                                            </a>
                                        </div>
                                    </div>
                                    @if($comment->replies()->where('status', 1)->count() > 0)
                                        <ul class="children">
                                            @include('front.layouts.replies',
                                                ['replies' => $comment->replies()->where('status', 1)->latest()->get()])
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                        <div id="respond" class="comment-respond">
                            <h3 id="reply-title" class="comment-reply-title">
                                <span>دیدگاه خود را با ما در میان بگذارید</span>
                                <small>
                                    <a id="cancel-comment-reply-link" href="#" onclick="return false"
                                       style="display:none;">
                                        لغو پاسخ
                                    </a>
                                </small>
                            </h3>
                            @if(session()->has('success'))
                                <div class="alert alert-success mb-3">
                                    {{ session()->get('success') }}
                                </div>
                            @elseif(session()->has('error'))
                                <div class="alert alert-danger mb-3">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <form action="{{route('comment.add')}}" method="post" id="commentform"
                                  class="comment-form">
                                @csrf
                                <fieldset class="comments-rating">
                                    <span class="rating-container">
                                        <input type="radio" id="rating-5" name="rating" value="5"/>
                                        <label for="rating-5">5</label>
                                        <input type="radio" id="rating-4" name="rating" value="4"/>
                                        <label for="rating-4">4</label>
                                        <input type="radio" id="rating-3" name="rating" value="3"/>
                                        <label for="rating-3">3</label>
                                        <input type="radio" id="rating-2" name="rating" value="2"/>
                                        <label for="rating-2">2</label>
                                        <input type="radio" id="rating-1" name="rating" value="1"/>
                                        <label for="rating-1">1</label>
                                        <input type="radio" id="rating-0" class="star-cb-clear" name="rating"
                                               value="0"/>
                                        <label for="rating-0">0</label>
                                    </span>
                                </fieldset>
                                <p class="comment-form-comment">
                                    <label for="comment">دیدگاه</label>
                                    <textarea id="comment" name="comment" rows="8" placeholder="دیدگاه خود را بنویسید"
                                              required>{{old('comment')}}</textarea>
                                </p>
                                @if(!auth()->check())
                                    <p class="comment-form-author">
                                        <label for="author">نام</label>
                                        <input id="author" name="author" type="text" placeholder="نام" size="20"
                                               value="{{old('author')}}" required>
                                    </p>
                                    <p class="comment-form-email">
                                        <label for="email">ایمیل</label>
                                        <input id="email" name="email" type="email" placeholder="ایمیل" size="30"
                                               value="{{old('email')}}" required>
                                    </p>
                                @endif
                                <p class="form-submit">
                                    <input name="submit" type="submit" id="submit" class="submit"
                                           value="فرستادن دیدگاه">
                                    <input type="hidden" name="comment_post_ID" value="{{$product->id}}"
                                           id="comment_post_ID">
                                    <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                    <input type="hidden" name="comment_post_type" id="comment_post_type"
                                           value="product">
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(count($similar_products) > 0)
        <section class="my_related_post">
            <div class="container">
                <div class="related shadow rounded">
                    <h2>محصولات مشابه</h2>
                    <div id="basic" class="row">
                        @foreach($similar_products as $product)
                            <div class="post_entry">
                                @if($product->price > 0)
                                    <div id="top_post_info" class="top_post_info">
                                        <div class="role">
                                            <img src="{{asset('assets/front/images/crown.svg')}}" alt="on_sale">
                                        </div>
                                    </div>
                                @endif
                                <a href="{{route('products.show', [$product->id, $product->slug])}}">
                                    <img src="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}"
                                         class="img-fluid rounded shadow" alt="" loading="lazy">
                                    <div class="show_case_wrapper">
                                        <div class="showcase_entry">
                                            <figcaption>
                                                <div class="samte_rast">
                                                    <h6>{{$product->name}}</h6>
                                                    <span class="avatar_small">
                                                <img src="{{$product->user->adjusted_avatar}}" class="img-fluid" alt="">
                                            </span>
                                                    <span class="author">
                                                {{$product->user->username}}
                                            </span>
                                                </div>
                                                <div class="samte_chap">
                                            <span>
                                                <i class="icon-download-index"></i>
                                                {{abbreviateNumber($product->downloads)}}
                                            </span>
                                                </div>
                                            </figcaption>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.justifiedGallery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#basic").justifiedGallery({
                margins: 10,
                rowHeight: 230,
                rtl: false,
            });

            const commentsSection = $('#comments-section')
            if ($(window).width() <= 992) {
                commentsSection.appendTo('.p-sidebar');
            }
            $(window).on('resize', function () {
                let win = $(this);
                if (win.width() > 992) {
                    commentsSection.appendTo('.p-main');
                } else if (win.width() <= 992) {
                    commentsSection.appendTo('.p-sidebar');
                }
            });

            $('.comment-reply-link').on('click', function () {
                let author = $(this).attr('data-comment-author')
                let comment_id = $(this).attr('data-comment-id')
                let h3Text = `برای <a href="#comment-${comment_id}">${author}</a> پاسخی بگذارید`
                $('#reply-title > span').html(h3Text)
                $('#cancel-comment-reply-link').show()
                $('#comment_parent').val(comment_id)
                $('.comments-rating').hide()
            })
            $('#cancel-comment-reply-link').on('click', function () {
                $('#reply-title > span').text("دیدگاه خود را با ما در میان بگذارید")
                $('#cancel-comment-reply-link').hide()
                $('#comment_parent').val('0')
                $('.comments-rating').show()
            })
        });
    </script>
@endsection
