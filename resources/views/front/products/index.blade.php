@extends('front.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/css/justifiedGallery.css')}}">
@endsection

@section('content')
    <section class="archive_title">
        <div class="container">
            <h3 class="text-right">{{isset($category) ? $category->name : 'جستجوی تصاویر'}}</h3>
            <div id="original_filters" class="sub_title">
                <form id="archive_sort" class="archive_sort" method="post" action="">
                    <a class="sort-by-btn cursor-pointer" data-parameter="pop">
                        <label class="sub_title_item_button {{!isset($_GET['sort']) ||
                               (isset($_GET['sort']) && ($_GET['sort'] != 'sale' && $_GET['sort'] != 'new')) ?
                               'sub_title_item_button_active' : ''}}" for="pop">
                            پربازدیدترین ها
                            <input type="radio" id="pop" name="sort" value="pop" checked/>
                        </label>
                    </a>
                    <a class="sort-by-btn cursor-pointer" data-parameter="sale">
                        <label class="sub_title_item_button {{isset($_GET['sort']) && $_GET['sort'] == 'sale' ?
                               'sub_title_item_button_active' : ''}}" for="sale">
                            پرفروش ترین
                            <input type="radio" id="sale" name="sort" value="sale"/>
                        </label>
                    </a>
                    <a class="sort-by-btn cursor-pointer" data-parameter="new">
                        <label class="sub_title_item_button {{isset($_GET['sort']) && $_GET['sort'] == 'new' ?
                               'sub_title_item_button_active' : ''}}" for="new">
                            جدیدترین ها
                            <input type="radio" id="new" name="sort" value="new"/>
                        </label>
                    </a>
                </form>
                <div class="archive_prop">
                    <span class="vertical_seprator"></span>
                    @if(isset($category))
                        @php
                            $categories = $category->children;
                        @endphp
                    @endif
                    @if(count($categories) > 0)
                        <span class="sub_title_item_text">
                        <a onclick="return false" class="sub_title_hook">
                            <text>دسته بندی ها</text>
                            <i class="icon-down"></i>
                        </a>
                            <span class="sub_title_drop_menus">
                            <ul id="taxonomies">
                                <li>
                                    <label for="input-filter0" class="filterCategory">
                                        <input type="checkbox" id="input-filter0"
                                               name="input-filter0" value="-1"
                                            {{isset($_GET['category']) &&
                                            in_array(-1, $_GET['category']) ? 'checked' : ''}}>
                                        همه
                                    </label>
                                </li>
                                @foreach($categories as $category)
                                    <li>
                                        <label for="input-filter{{$category->id}}" class="filterCategory">
                                            <input type="checkbox" id="input-filter{{$category->id}}"
                                                   name="input-filter{{$category->id}}" value="{{$category->id}}"
                                                {{isset($_GET['category']) &&
                                                in_array($category->id, $_GET['category']) ? 'checked' : ''}}>
                                            {{$category->name}}
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                        </span>
                    </span>
                    @endif
                    <span class="sub_title_item_text">
                    <a onclick="return false" class="sub_title_hook">
                        <text>لایسنس</text>
                        <i class="icon-down"></i>
                    </a>
                    <span class="sub_title_drop_menus">
                        <ul>
                            <li class="filterBy" data-filter="all">
                                <label for="inputall" class="d-flex align-items-center">
                                    <input type="radio" id="inputall" name="input_filterBy" class="ml-2"
                                    {{isset($_GET['filterby']) && ($_GET['filterby'] == 'free' || $_GET['filterby'] == 'pricing') ? '' : 'checked'}}>
                                    همه
                                </label>
                            </li>
                            <li class="filterBy" data-filter="free">
                                <label for="inputfree" class="d-flex align-items-center">
                                    <input type="radio" id="inputfree" name="input_filterBy" class="ml-2"
                                    {{isset($_GET['filterby']) && $_GET['filterby'] == 'free' ? 'checked' : ''}}>
                                    رایگان
                                </label>
                            </li>
                            <li class="filterBy" data-filter="pricing">
                                <label for="inputpricing" class="d-flex align-items-center">
                                    <input type="radio" id="inputpricing" name="input_filterBy" class="ml-2"
                                    {{isset($_GET['filterby']) && $_GET['filterby'] == 'pricing' ? 'checked' : ''}}>
                                    پولی
                                </label>
                            </li>
                        </ul>
                    </span>
                </span>
                </div>
            </div>
        </div>
    </section>
    <section class="result">
        <div class="container">
            <div id="basic_wrapper">
                <div class="combine" id="combine">
                    <div class="spinner">
                        <div class="ball1"></div>
                        <div class="ball2"></div>
                        <div class="ball3"></div>
                    </div>
                </div>
                @if(count($products) > 0)
                    <div id="basic" class="row">
                        @foreach($products as $product)
                            <div class="post_entry">
                                @if($product->price > 0)
                                    <div id="top_post_info" class="top_post_info">
                                        <div class="role">
                                            <img src="{{asset('assets/front/images/crown.svg')}}" alt="on_sale">
                                        </div>
                                    </div>
                                @endif
                                <a href="{{route('products.show', [$product->id, $product->slug])}}">
                                    <img src="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}"
                                         class="img-fluid rounded shadow" alt="" loading="lazy">
                                    <div class="show_case_wrapper">
                                        <div class="showcase_entry">
                                            <figcaption>
                                                <div class="samte_rast">
                                                    <h6>{{$product->name}}</h6>
                                                    <span class="avatar_small">
                                                <img src="{{$product->user->adjusted_avatar}}" class="img-fluid" alt="">
                                            </span>
                                                    <span class="author">
                                                {{$product->user->username}}
                                            </span>
                                                </div>
                                                <div class="samte_chap">
                                            <span>
                                                <i class="icon-download-index"></i>
                                                {{abbreviateNumber($product->downloads)}}
                                            </span>
                                                </div>
                                            </figcaption>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="text-center mt-5">
                        <i class="icon-search la text-muted" style="font-size: 100px;"></i>
                        <p class="text-muted mt-3">هیچ موردی با این فیلتر یافت نشد!</p>
                    </div>
                @endif
            </div>
        </div>
    </section>
    {{$products->links('front.layouts.pagination')}}
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.justifiedGallery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#basic").justifiedGallery({
                margins: 10,
                rowHeight: 230,
                maxRowHeight: 500,
                rtl: false,
            });
            $('.sort-by-btn').on('click', function (e) {
                e.preventDefault()
                setUrl('sort', $(this).data('parameter'))
            })
            $('.filterBy').on('click', function () {
                setUrl('filterby', $(this).data('filter'))
            })
            $('.filterCategory').on('click', function () {
                let url = new URL($(location).attr('href'));
                let categories = url.searchParams.getAll('category[]')
                let cat_id = $(this).find('input[type="checkbox"]').val()
                let cat_index = categories.indexOf(cat_id);
                if (cat_index === -1) {
                    categories.push(cat_id);
                } else {
                    categories.splice(cat_index, 1)
                }
                url.searchParams.delete('category[]')
                categories.map(function (category) {
                    url.searchParams.append('category[]', category)
                })
                $(location).attr('href', url.href);
            })
        });

        function setUrl(key, param) {
            let url = new URL($(location).attr('href'));
            url.searchParams.set(key, param);
            url.searchParams.set('page', 1);
            $(location).attr('href', url.href);
        }
    </script>
@endsection
