@extends('front.layouts.app')
@section('content')
    <div style="background-image:url('{{asset('assets/front/images/bg_matlab.svg')}}');background-repeat:no-repeat">
        <section class="">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 mx-auto">
                        <div class="blog_page">
                            <h1 class="text-center">وبلاگ</h1>
                            <div class="blog_search text-center shadow">
                                <form action="{{route('blog.index')}}" method="get" style="margin-bottom:0">
                                    <input type="search" name="s" value="{{$_GET['s'] ?? ''}}" placeholder="جستجو در وبلاگ ...">
                                    <button type="submit">
                                        <i class="icon-search"></i>
                                    </button>
                                </form>
                            </div>
                            @foreach($articles as $article)
                                <div class="single_description rounded shadow single_matlab">
                                    <a href="{{route('blog.show', $article->slug)}}" class="single_thumbnail">
                                        <img src="{{$article->thumbnail ?? asset('assets/front/images/posts/10.jpg')}}" class="img-fluid"
                                             alt="{{$article->thumbnail_alt}}" title="{{$article->thumbnail_alt}}"
                                             loading="lazy">
                                    </a>
                                    <div class="description_product">
                                        <a href="{{route('blog.show', $article->slug)}}">
                                            <h1 class="text-center" style="color:black">{{$article->name}}</h1>
                                        </a>
                                        <div class="meta_single_mtlab">
                                            <ul>
                                                @if($article->user_id)
                                                    <li class="meta_matlab_item">
                                                        <div class="author_matlab_avatar d-inline">
                                                            <a href="{{route('author.show', $article->user->username)}}">
                                                                <img
                                                                    src="{{$article->user->adjusted_avatar}}"
                                                                    width="34" height="34" alt="">
                                                                <small>{{$article->user->full_name}}</small>
                                                            </a>
                                                        </div>
                                                    </li>
                                                @endif
                                                <li class="meta_matlab_item">
                                                    <small>{{jdate($article->updated_at)->format('%d %B %Y')}}</small>
                                                    <i class="icon-calendar"></i>
                                                </li>
                                                <li class="meta_matlab_item">
                                                    <i class="icon-bubbles3"></i>
                                                    <small>{{$article->comments->count()}}</small>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="post-content mb-3">
                                            {{$article->summary}} ...
                                        </div>
                                        @if($article->tags)
                                            @php
                                                $tags = explode(',', $article->tags)
                                            @endphp
                                            <div class="suggested">
                                                @foreach($tags as $tag)
                                                    <a href="#" onclick="return false">
                                                        {{$tag}}
                                                    </a>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            {{$articles->links('front.layouts.pagination')}}

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
