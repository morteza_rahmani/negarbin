@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <main>
            <div class="blog_sub_navigation">
                <div class="blog_container">
                    <div class="row">
                        <div class="col-12">
                            <div class="blog_sub_navigation_content">
                                <div class="blog_sub_navigation_content_nav">
                                    <ul class="blog_sub_navigation_menu">
                                        <li class="blog_sub_menu_item blog_sub_menu_item_active">
                                            <a href="{{route('blog.index')}}">همه مطالب</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog_sub_navigation__search blog__search">
                                    <div class="search_container">
                                        <form class="search" action="{{route('blog.index')}}" method="get">
                                            <input type="search" name="s" value="{{$_GET['s'] ?? ''}}"
                                                   style="outline: 0;" placeholder="جستجو...">
                                            <button class="go_btn border-0 p-0">
                                                <i class="icon-arrow-right"></i>
                                            </button>
                                        </form>
                                        <div class="search_btn search_open"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($articles[0]))
                <section class="featured_section">
                    <div class="blog_container">
                        <div class="row">
                            <div class="col-12">
                                <div class="featured_section__post">
                                    <figure class="featured__thumbnail">
                                        <a href="{{route('blog.show', $articles[0]->slug)}}">
                                            <img src="{{$articles[0]->thumbnail}}">
                                        </a>
                                        @if($articles[0]->tags)
                                            @php
                                                $tags = explode(',', $articles[0]->tags)
                                            @endphp
                                            @foreach($tags as $tag)
                                                <span class="thumbnail__badge">{{$tag}}</span>
                                            @endforeach
                                        @endif
                                    </figure>
                                    <article class="featured__article">
                                        <a href="{{route('author.show', $articles[0]->user->username)}}"
                                           class="featured__article_badge">
                                            <img src="{{$articles[0]->user->adjusted_avatar}}"
                                                 width="34" height="34" alt="author"
                                                 class="rounded-circle">
                                            {{$articles[0]->user->full_name}}
                                        </a>
                                        <header class="featured__article_header">
                                            <h2 class="featured__article_title">
                                                <a href="{{route('blog.show', $articles[0]->slug)}}"
                                                   class="featured__article_permalink">
                                                    {{$articles[0]->name}}
                                                </a>
                                            </h2>
                                        </header>
                                        <div class="featured__article_content">
                                            <div class="font__size_16" style="color: #4f5b6d; margin-bottom: 30px">
                                                <p>
                                                    {!! mb_strlen($articles[0]->summary) > 220 ?
                                                    mb_substr($articles[0]->summary, 0, 220) . ' ...' :
                                                    $articles[0]->summary !!}
                                                </p>
                                            </div>
                                            <a href="{{route('blog.show', $articles[0]->slug)}}">
                                                ادامه مطلب
                                                <i class="icon-arrow-left"></i>
                                            </a>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            <section class="latest_post__section">
                <div class="blog_container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="post__card_header">
                                        <h2 class="post__card_header_title">
                                            @if(isset($_GET['s']))
                                                نتایج جستجوی <span>{{$_GET['s']}}</span>
                                            @else
                                                آخرین های <span>وبلاگ</span>
                                            @endif
                                        </h2>
                                        {{--<a href="#" class="post__card_header_view_all">--}}
                                        {{--مشاهده همه--}}
                                        {{--<i class="icon-arrow-left"></i>--}}
                                        {{--</a>--}}
                                    </div>
                                </div>
                                @if(count($articles) > 0)
                                    @php($i = 0)
                                    @foreach($articles as $article)
                                        <div class="single__post__grid col-lg-6 col-md-6 col-sm-12">
                                            <article class="post__single">
                                                <figure class="post__single_thumbnail">
                                                    <a href="{{route('blog.show', $article->slug)}}">
                                                        <img src="{{$article->thumbnail}}"
                                                             alt="{{$article->thumbnail_alt}}">
                                                    </a>
                                                </figure>
                                                <div class="post__single_content post">
                                                    <h3 class="post__title">
                                                        <a href="{{route('blog.show', $article->slug)}}">
                                                            {{$article->name}}
                                                        </a>
                                                    </h3>
                                                    <p class="m-0">
                                                        {!! mb_strlen($articles[0]->summary) > 100 ?
                                                        mb_substr($articles[0]->summary, 0, 100) . ' ...' :
                                                        $articles[0]->summary !!}
                                                    </p>
                                                    <div class="post__footer">
                                                        @if($article->user_id)
                                                            <div class="post__meta_tag">
                                                                <a href="{{route('author.show', $article->user->username)}}"
                                                                   class="post__meta_tag_text">
                                                                    <img src="{{$article->user->adjusted_avatar}}"
                                                                         width="34" height="34" alt="author"
                                                                         class="rounded-circle">
                                                                    {{$article->user->full_name}}
                                                                </a>
                                                            </div>
                                                        @endif
                                                        <div class="archive__grid_time">
                                                            <i class="flaticon-time"></i>
                                                            <span class="archive__grid_time__text">
                                                            {{jdate($article->updated_at)->format('%d %B %Y')}}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                        @php($i++)
                                        @if($i === 2)
                                            <?php break; ?>
                                        @endif
                                    @endforeach
                                @else
                                    <div class="font-weight-bolder mx-auto text-muted">
                                        هیچ پستی یافت نشد
                                    </div>
                                @endif
                            </div>
                        </div> <!-- end of top row of blog-->

                        <div class="col-lg-4 col-md-4 col-sm-12 asideWrapper">
                            <aside class="blog_archive__sidebar widget">
                                <div class="widget__single widget__single_populat_posts transparent_bg">
                                    <h3 class="widget__single_title">
                                        مطالب <span>پربازدید</span>
                                    </h3>
                                    <div class="sidebar__popular_posts">
                                        @foreach($best_articles as $article)
                                            <div class="sidebar__popular_single_post">
                                                <a href="{{route('blog.show', $article->slug)}}"
                                                   class="popular__post_thumb">
                                                    <img src="{{$article->thumbnail}}" alt="{{$article->thumbnail_alt}}"
                                                         class="img-fluid">
                                                </a>
                                                <div class="popular__post_content">
                                                    <a href="{{route('blog.show', $article->slug)}}"
                                                       class="popular__post_title">
                                                        {{$article->name}}
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </aside>
                        </div>

                    </div>

                    @if(count($articles) > 2)
                        <div class="row">
                            @php($i = 0)
                            @foreach($articles as $article)
                                @php($i++)
                                @if($i <= 2)
                                    <?php continue; ?>
                                @endif
                                <div class="single__post__grid col-lg-4 col-md-4 col-sm-12">
                                    <article class="post__single">
                                        <figure class="post__single_thumbnail">
                                            <a href="{{route('blog.show', $article->slug)}}">
                                                <img src="{{$article->thumbnail}}"
                                                     alt="{{$article->thumbnail_alt}}">
                                            </a>
                                        </figure>
                                        <div class="post__single_content post">
                                            <h3 class="post__title">
                                                <a href="{{route('blog.show', $article->slug)}}">
                                                    {{$article->name}}
                                                </a>
                                            </h3>
                                            <p class="m-0">
                                                {!! mb_strlen($articles[0]->summary) > 100 ?
                                                mb_substr($articles[0]->summary, 0, 100) . ' ...' :
                                                $articles[0]->summary !!}
                                            </p>
                                            <div class="post__footer">
                                                @if($article->user_id)
                                                    <div class="post__meta_tag">
                                                        <a href="{{route('author.show', $article->user->username)}}"
                                                           class="post__meta_tag_text">
                                                            <img src="{{$article->user->adjusted_avatar}}"
                                                                 width="34" height="34" alt="author"
                                                                 class="rounded-circle">
                                                            {{$article->user->full_name}}
                                                        </a>
                                                    </div>
                                                @endif
                                                <div class="archive__grid_time">
                                                    <i class="flaticon-time"></i>
                                                    <span class="archive__grid_time__text">
                                                            {{jdate($article->updated_at)->format('%d %B %Y')}}
                                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            @endforeach
                        </div> <!-- row -->
                    @endif
                    <div class="row mt-25">
                        <div class="col-xs-12 text-center margin_center view_all_blog_button">
                            {{$articles->links('front.layouts.pagination')}}
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".search_btn").on("click", function () {
                if ($(this).hasClass('search_open')) {
                    $(this).removeClass("search_open");
                    $(this).addClass("search_close");
                    $(this).parent().find(".search").addClass("search_active");
                    $(this).parent().find(".search .go_btn").addClass("search_active_go_btn");
                } else {
                    $(this).addClass("search_open");
                    $(this).removeClass("search_close");
                    $(this).parent().find(".search").removeClass("search_active");
                    $(this).parent().find(".search .go_btn").removeClass("search_active_go_btn");
                }
            });
        });
    </script>
@endsection
