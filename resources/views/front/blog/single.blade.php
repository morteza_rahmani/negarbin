@extends('front.layouts.app')

@section('meta')
    <meta name="description" content="{{$article->summary}}">
    <meta name="keyword" content="{{$article->keywords}}">
@endsection

@section('content')
    <section class="single_content mx-auto">
        <div class="container">
            <div class="blog_sub_navigation">
                <div class="blog_container">
                    <div class="row">
                        <div class="col-12 p-0">
                            <div class="blog_sub_navigation_content mb-3">
                                <div class="blog_sub_navigation_content_nav">
                                    <ul class="blog_sub_navigation_menu">
                                        <li class="blog_sub_menu_item blog_sub_menu_item_active">
                                            <a href="{{route('blog.index')}}">همه مطالب</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog_sub_navigation__search blog__search">
                                    <div class="search_container">
                                        <form class="search" action="{{route('blog.index')}}" method="get">
                                            <input type="search" name="s" value="{{$_GET['s'] ?? ''}}"
                                                   style="outline: 0;" placeholder="جستجو...">
                                            <button class="go_btn border-0 p-0">
                                                <i class="icon-arrow-right"></i>
                                            </button>
                                        </form>
                                        <div class="search_btn search_open"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-12 single_asl">
                    <div class="latest_post d-none d-lg-block">
                        <h4>آخرین مطالب</h4>
                        <div class="post_small">
                            <div class="row">
                                <div class="col-lg-12 col-md-4 col-sm-6">
                                    @foreach($latest_articles as $latest_article)
                                        <aside class="post_small_item">
                                            <a href="{{route('blog.show', $latest_article->slug)}}">
                                                <div class="whole_small_wrapper shadow rounded">
                                                    <div class="small_thumbnail">
                                                        <img class="img-fluid" width="140"
                                                             alt="{{$latest_article->thumbnail_alt}}"
                                                             src="{{$latest_article->thumbnail ?? asset('assets/front/images/bussiness.png')}}">
                                                    </div>
                                                    <div class="post_small_title">
                                                        <h6>{{$latest_article->name}}</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </aside>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="single_description rounded shadow">
                        <div class="single_thumbnail">
                            <img src="{{$article->thumbnail ?? asset('assets/front/images/bussiness.png')}}"
                                 class="img-fluid" alt="{{$article->thumbnail_alt ?? $article->name}}">
                        </div>
                        <div class="description_product">
                            <h1 class="text-center">{{$article->name}}</h1>
                            <div class="meta_single_mtlab">
                                <ul>
                                    @if($article->user_id)
                                        <li class="meta_matlab_item">
                                            <div class="author_matlab_avatar d-inline">
                                                <a href="{{route('author.show', $article->user->username)}}">
                                                    <img src="{{$article->user->adjusted_avatar}}"
                                                         width="34" height="34" alt="">
                                                    <small>{{$article->user->full_name}}</small>
                                                </a>
                                            </div>
                                        </li>
                                    @endif
                                    <li class="meta_matlab_item">
                                        <small>{{jdate($article->updated_at)->format('%d %B %Y')}}</small>
                                        <i class="icon-calendar"></i>
                                    </li>
                                    <li class="meta_matlab_item">
                                        <i class="icon-bubbles3"></i>
                                        <small>{{$article->comments->count()}}</small>
                                    </li>
                                </ul>
                            </div>

                            <div class="blog_text">
                                {!! $article->content !!}
                            </div>
                            @if($article->tags)
                                @php
                                    $tags = explode(',', $article->tags)
                                @endphp
                                <div class="suggested">
                                    @foreach($tags as $tag)
                                        <a href="#" onclick="return false">
                                            {{$tag}}
                                        </a>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div id="comments-section" class="single_comment shadow rounded">
                        <h2 class="comment_title">
                            <i class="icon-bubbles3"></i>
                            نظرات
                        </h2>
                        <ol class="commentlist">
                            @foreach($comments as $comment)
                                @if(empty($comment->user_id))
                                    @php
                                        $author = $comment->name;
                                        $avatar = asset('assets/front/images/no-photo.jpg');
                                    @endphp
                                @else
                                    @php
                                        $author = $comment->user->full_name;
                                        $avatar = $comment->user->adjusted_avatar;
                                    @endphp
                                @endif
                                <li class="comment" id="comment-{{$comment->id}}">
                                    <div class="comment-body">
                                        <div class="comment-author vcard">
                                            <img src="{{$avatar}}" height="32" width="32" class="avatar"
                                                 alt="آواتار نویسنده دیدگاه">
                                            <cite class="fn">{{$author}}</cite>
                                        </div>
                                        <div class="comment-meta commentmetadata">
                                            <a href="#" onclick="return false">
                                                {{jdate($comment->created_at)->format('%d %B %Y ساعت H:i')}}
                                            </a>
                                        </div>
                                        <p>{{$comment->content}}</p>
                                        <div class="reply">
                                            <a class="comment-reply-link" data-comment-ID="{{$comment->id}}"
                                               data-comment-author="{{$author}}" href="#respond">
                                                پاسخ
                                            </a>
                                        </div>
                                    </div>
                                    @if($comment->replies()->where('status', 1)->count() > 0)
                                        <ul class="children">
                                            @include('front.layouts.replies',
                                                ['replies' => $comment->replies()->where('status', 1)->latest()->get()])
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                        <div id="respond" class="comment-respond">
                            <h3 id="reply-title" class="comment-reply-title">
                                <span>دیدگاه خود را با ما در میان بگذارید</span>
                                <small>
                                    <a id="cancel-comment-reply-link" href="#" onclick="return false"
                                       style="display:none;">
                                        لغو پاسخ
                                    </a>
                                </small>
                            </h3>
                            @if(session()->has('success'))
                                <div class="alert alert-success mb-3">
                                    {{ session()->get('success') }}
                                </div>
                            @elseif(session()->has('error'))
                                <div class="alert alert-danger mb-3">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <form action="{{route('comment.add')}}" method="post" id="commentform"
                                  class="comment-form">
                                @csrf
                                <fieldset class="comments-rating">
                                    <span class="rating-container">
                                        <input type="radio" id="rating-5" name="rating" value="5"/>
                                        <label for="rating-5">5</label>
                                        <input type="radio" id="rating-4" name="rating" value="4"/>
                                        <label for="rating-4">4</label>
                                        <input type="radio" id="rating-3" name="rating" value="3"/>
                                        <label for="rating-3">3</label>
                                        <input type="radio" id="rating-2" name="rating" value="2"/>
                                        <label for="rating-2">2</label>
                                        <input type="radio" id="rating-1" name="rating" value="1"/>
                                        <label for="rating-1">1</label>
                                        <input type="radio" id="rating-0" class="star-cb-clear" name="rating"
                                               value="0"/>
                                        <label for="rating-0">0</label>
                                    </span>
                                </fieldset>
                                <p class="comment-form-comment">
                                    <label for="comment">دیدگاه</label>
                                    <textarea id="comment" name="comment" rows="8" placeholder="دیدگاه خود را بنویسید"
                                              required>{{old('comment')}}</textarea>
                                </p>
                                @if(!auth()->check())
                                    <p class="comment-form-author">
                                        <label for="author">نام</label>
                                        <input id="author" name="author" type="text" placeholder="نام" size="20"
                                               value="{{old('author')}}" required>
                                    </p>
                                    <p class="comment-form-email">
                                        <label for="email">ایمیل</label>
                                        <input id="email" name="email" type="email" placeholder="ایمیل" size="30"
                                               value="{{old('email')}}" required>
                                    </p>
                                @endif
                                <p class="form-submit">
                                    <input name="submit" type="submit" id="submit" class="submit"
                                           value="فرستادن دیدگاه">
                                    <input type="hidden" name="comment_post_ID" value="{{$article->id}}"
                                           id="comment_post_ID">
                                    <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                    <input type="hidden" name="comment_post_type" id="comment_post_type"
                                           value="article">
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            // Comments
            $('.comment-reply-link').on('click', function () {
                let author = $(this).attr('data-comment-author')
                let comment_id = $(this).attr('data-comment-id')
                let h3Text = `برای <a href="#comment-${comment_id}">${author}</a> پاسخی بگذارید`
                $('#reply-title > span').html(h3Text)
                $('#cancel-comment-reply-link').show()
                $('#comment_parent').val(comment_id)
                $('.comments-rating').hide()
            })
            $('#cancel-comment-reply-link').on('click', function () {
                $('#reply-title > span').text("دیدگاه خود را با ما در میان بگذارید")
                $('#cancel-comment-reply-link').hide()
                $('#comment_parent').val('0')
                $('.comments-rating').show()
            });

            // Search
            $(".search_btn").on("click", function () {
                if ($(this).hasClass('search_open')) {
                    $(this).removeClass("search_open");
                    $(this).addClass("search_close");
                    $(this).parent().find(".search").addClass("search_active");
                    $(this).parent().find(".search .go_btn").addClass("search_active_go_btn");
                } else {
                    $(this).addClass("search_open");
                    $(this).removeClass("search_close");
                    $(this).parent().find(".search").removeClass("search_active");
                    $(this).parent().find(".search .go_btn").removeClass("search_active_go_btn");
                }
            });
        })
    </script>
@endsection
