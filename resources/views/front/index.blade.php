@extends('front.layouts.app')
@section('meta')
    <title>صفحه اصلی | نگاربین</title>
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('assets/front/css/justifiedGallery.css')}}">
@endsection
@section('content')
    <div class="container">
        <div class="homepage-title text-center">
            <h2>
                <b>The Largest Graphic Files Market Place </b>In Iran With More Than 2,000 Files And 1,000 Active Users
            </h2>
        </div>
    </div>
    <div class="container">
        @if(isset($banners->banner2_image))
            <div class="text-center negarbin_banner negarbin_top_banner">
                <a href="{{$banners->banner2_link}}">
                    <img src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner2_image}}">
                </a>
            </div>
        @endif
        <div class="negarbin_banner negarbin_middle_banner text-right">
            <div class="row">
                @if(isset($banners->banner3_image))
                    <div class="col-md-4">
                        <a href="{{$banners->banner3_link}}">
                            <div class="overlay">
                                <img src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner3_image}}">
                                @if(isset($banners->banner3_icon))
                                    <span>
                                    <i class="{{$banners->banner3_icon}}"></i>
                                </span>
                                @endif
                            </div>
                            <div class="card_desc">
                                <h4>تصاویر استوک</h4>
                                <p>در میان بیش از هزار تصویر استوک که هر روز آپدیت میشوند جستجو کنید</p>
                            </div>
                        </a>
                    </div>
                @endif
                @if(isset($banners->banner4_image))
                    <div class="col-md-4">
                        <a href="{{$banners->banner4_link}}">
                            <div class="overlay">
                                <img src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner4_image}}">
                                @if(isset($banners->banner4_icon))
                                    <span>
                                    <i class="{{$banners->banner4_icon}}"></i>
                                </span>
                                @endif
                            </div>
                            <div class="card_desc">
                                <h4>وکتور ها</h4>
                                <p>آیکون ها ، کاراکتر های کارتونی ، لوگو ها و همه طرح هایی که قابل ویرایش اند و دچار افت
                                    کیفیت نمیشوند</p>
                            </div>
                        </a>
                    </div>
                @endif
                @if(isset($banners->banner5_image))
                    <div class="col-md-4">
                        <a href="{{$banners->banner5_link}}">
                            <div class="overlay">
                                <img src="{{env('STORAGE_ADDRESS').env('BANNERS').$banners->banner5_image}}">
                                @if(isset($banners->banner5_icon))
                                    <span>
                                    <i class="{{$banners->banner5_icon}}"></i>
                                </span>
                                @endif
                            </div>
                            <div class="card_desc">
                                <h4>ابزارهای گرافیکی</h4>
                                <p>براش ، اکشن ، فونت و هر ابزاری که برای طراحی گرافیک نیاز دارید در اختیار شماست</p>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <section class="description">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-right">
                    <div class="smarter">
                        <h3>نگاربین چیست ؟</h3>
                        <h5>هوشمند تر عکستو جستجو کن ...</h5>
                        <p>نگاربین یک پلتفرم خلاق در زمینه ارائه تصاویر استوک ، وکتور ها و محصولات دیگر است . در این
                            پلتفرم هر شخصی میتواند تصاویر مورد نظرش را بخرد یا برای فروش در سایت به اشتراک بگذارد .
                            نگاربین با داشتن بیش از 400 هنرمند و 2 میلیون عکس به طور حرفه ای به اشتراک گذاری عکس و منابع
                            گرافیکی دیگر میپردازد . این مجموعه فعالیت خود را از سال 97 آغاز نمود و هم اکنون پرطرفدارترین
                            سایت در زمینه خرید و فروش تصاویر استوک در ایران میباشد.</p>
                        <p>تصاویر استوک تصاویری با کیفیت بالا و تراکم پیکسلی بالا هستند که هر طراحی گرافیک ، معمار یا
                            ... به آن نیاز دارند . نگار بین سعی کرده با ابزاری ساده خلاقیت هنرمندان را به اشتراک گذاشته
                            و از آنها در زمینه های مختلف استفاده کند .</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 text-center">
                    <img src="{{asset('assets/front/images/hero.png')}}" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section class="featured_items">
        <div class="container">
            <div class="choice_items">
                @if(count($products) > 0)
                    @if($is_tag)
                        <h3 class="text-right font-weight-bold">آخرین محصولات به انتخاب نگاربین</h3>
                        <div id="basic" class="row">
                            @foreach($products as $product)
                                <div class="post_entry">
                                    @if($product->price > 0)
                                        <div id="top_post_info" class="top_post_info">
                                            <div class="role">
                                                <img src="{{asset('assets/front/images/crown.svg')}}" alt="on_sale">
                                            </div>
                                        </div>
                                    @endif
                                    <a href="{{route('products.show', [$product->id, $product->slug])}}">
                                        <img src="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}"
                                             class="img-fluid rounded shadow" alt="" loading="lazy">
                                        <div class="show_case_wrapper">
                                            <div class="showcase_entry">
                                                <figcaption>
                                                    <div class="samte_rast">
                                                        <h6>{{$product->name}}</h6>
                                                        <span class="avatar_small">
                                                            <img src="{{$product->user->adjusted_avatar}}"
                                                                 class="img-fluid" alt="">
                                                        </span>
                                                        <span class="author">
                                                            {{$product->user->username}}
                                                        </span>
                                                    </div>
                                                    <div class="samte_chap">
                                                        <span>
                                                            <i class="icon-download-index"></i>
                                                            {{abbreviateNumber($product->downloads)}}
                                                        </span>
                                                    </div>
                                                </figcaption>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <h3 class="text-right font-weight-bold">آخرین محصولات</h3>
                        <div id="basic" class="row">
                            @foreach($products as $product)
                                <div class="post_entry">
                                    @if($product->price > 0)
                                        <div id="top_post_info" class="top_post_info">
                                            <div class="role">
                                                <img src="{{asset('assets/front/images/crown.svg')}}" alt="on_sale">
                                            </div>
                                        </div>
                                    @endif
                                    <a href="{{route('products.show', [$product->id, $product->slug])}}">
                                        <img src="{{env('STORAGE_ADDRESS').env('PRODUCTS').$product->thumbnail}}"
                                             class="img-fluid rounded shadow" alt="" loading="lazy">
                                        <div class="show_case_wrapper">
                                            <div class="showcase_entry">
                                                <figcaption>
                                                    <div class="samte_rast">
                                                        <h6>{{$product->name}}</h6>
                                                        <span class="avatar_small">
                                                            <img src="{{$product->user->adjusted_avatar}}"
                                                                 class="img-fluid" alt="">
                                                        </span>
                                                        <span class="author">
                                                            {{$product->user->username}}
                                                        </span>
                                                    </div>
                                                    <div class="samte_chap">
                                                        <span>
                                                            <i class="icon-download-index"></i>
                                                            {{abbreviateNumber($product->downloads)}}
                                                        </span>
                                                    </div>
                                                </figcaption>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </section>
    <section class="weblog">
        <div class="container">
            <h3 class="text-right font-weight-bold">
                آخرین مطالب
            </h3>
            <div class="mataleb_index">
                @if(count($last_articles) > 0)
                    <div class="row">
                        @foreach($last_articles as $article)
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <article class="post__single" style="min-height: unset !important;">
                                    <a href="{{route('blog.show', $article->slug)}}">
                                        <figure class="post__single-thumbnail pb-0">
                                            <img src="{{$article->thumbnail_url}}" class="img-fluid">
                                        </figure>
                                        <div class="post__single-content py-0">
                                            <h5 class="post__title">
                                                {{$article->name}}
                                            </h5>
                                        </div>
                                    </a>
                                </article>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="text-center">
                        <i class="icon-doc text-muted" style="font-size: 100px"></i>
                        <h6 class="mt-3 text-muted">هنوز مطلبی ثبت نشده است</h6>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('assets/front/js/jquery.justifiedGallery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#basic").justifiedGallery({
                margins: 10,
                rowHeight: 230,
                rtl: false,
            });
        });
    </script>
@endsection
