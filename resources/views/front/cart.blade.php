@extends('front.layouts.app')
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="page_content rounded shadow">
                <div id="edd_checkout_wrap">
                    @include('front.layouts.notification')
                    @if(count($products) > 0)
                        <div id="edd_checkout_cart_wrap">
                            <table id="edd_checkout_cart">
                                <thead>
                                <tr class="edd_cart_header_row">
                                    <th class="edd_cart_item_name">نام محصول</th>
                                    <th class="edd_cart_item_price">قیمت</th>
                                    <th class="edd_cart_actions">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    @php($total_price += $product->product->price)
                                    <tr class="edd_cart_item" id="edd_cart_item_0_463">
                                        <td class="edd_cart_item_name">
                                            <div class="edd_cart_item_image">
                                                <img width="25" height="25"
                                                     src="{{env('STORAGE_ADDRESS').env('PRODUCTS_SMALL').$product->product->thumbnail}}"
                                                     class="attachment-25x25 size-25x25 wp-post-image"/>
                                            </div>
                                            <span
                                                class="edd_checkout_cart_item_title">{{$product->product->name}}</span>
                                        </td>
                                        <td class="edd_cart_item_price">
                                            {{number_format($product->product->price)}} تومان
                                        </td>
                                        <td class="edd_cart_actions">
                                            <form action="{{route('deleteFromCart', $product->product->id)}}"
                                                  method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit"
                                                        class="bg-transparent border-0 edd_cart_remove_item_btn p-0 text-primary">
                                                    حذف
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                <!-- Show any cart fees, both positive and negative fees -->
                                </tbody>
                                <tfoot>
                                <tr class="edd_cart_footer_row edd_cart_discount_row" style="display:none;">
                                    <th colspan="3" class="edd_cart_discount">
                                    </th>
                                </tr>
                                <tr class="edd_cart_footer_row">
                                    <th colspan="3" class="edd_cart_total">
                                        کل:
                                        <span class="edd_cart_amount" data-subtotal="3000" data-total="3000">
                                        {{number_format($total_price)}} تومان
                                    </span>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div id="edd_checkout_form_wrap" class="edd_clearfix">
                            <form id="edd_purchase_form" class="edd_form" action="{{route('invoice.store')}}"
                                  method="POST">
                                @csrf
                                <fieldset id="edd_checkout_user_info">
                                    <legend>اطلاعات شخصی</legend>
                                    <p id="edd-email-wrap">
                                        <label class="edd-label" for="edd-email">
                                            آدرس ایمیل <span class="edd-required-indicator">*</span>
                                        </label>
                                        <span class="edd-description" id="edd-email-description">
                                        ما رسید خرید را به این آدرس ارسال خواهیم کرد.
                                    </span>
                                        <input class="edd-input required" type="email" name="email"
                                               placeholder="آدرس ایمیل" id="edd-email" value="{{$product->user->email}}"
                                               aria-describedby="edd-email-description" required/>
                                    </p>
                                </fieldset>
                                <fieldset id="edd_purchase_submit">
                                    <p id="edd_final_total_wrap">
                                        <strong>مبلغ کل خرید:</strong>
                                        <span class="edd_cart_amount" data-subtotal="{{$total_price}}"
                                              data-total="{{$total_price}}">
                                            {{number_format($total_price)}} تومان
                                        </span>
                                    </p>
                                    <input type="hidden" name="cart_amount" value="{{$total_price}}">
                                    <input type="submit" class="edd-submit blue button btn btn-submit"
                                           id="edd-purchase-button" name="edd-purchase" value="خرید"/>
                                </fieldset>
                            </form>
                        </div>
                    @else
                        <div class="text-center">
                            <i class="icon-shopping-basket text-muted" style="font-size: 100px"></i>
                            <p class="text-muted my-3">سبد خرید شما خالی است.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
