@extends('front.layouts.app')
@section('meta')
    <meta name="keywords" content="{{$page->keywords}}">
    <meta name="description" content="{!! $page->summary !!}">
@endsection
@section('content')
    <section class="single_content">
        <div class="container">
            <div class="page_content rounded shadow">
                <h1 class="text-center pt-2 pb-4">{{$page->name}}</h1>
                {!! $page->content !!}
            </div>
        </div>
    </section>
@endsection
