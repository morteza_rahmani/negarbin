<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('assets/front/images/faveicon.ico')}}" title="Favicon">
    <title>سفارش پرداخت شده | {{config('app.name')}}</title>
</head>
<body>
<style>
    @font-face {
        font-family: iranyekan;
        font-style: normal;
        font-weight: normal;
        src: url('{{env('APP_URL')}}/assets/front/fonts/eot/iranyekanwebregularfanum.eot');
        src: url('{{env('APP_URL')}}/assets/front/fonts/eot/iranyekanwebregularfanum.eot?#iefix') format('embedded-opentype'),
            /* IE6-8 */ url('{{env('APP_URL')}}/assets/front/fonts/woff/iranyekanwebregularfanum.woff') format('woff'),
            /* FF3.6+, IE9, Chrome6+, Saf5.1+*/ url('{{env('APP_URL')}}/assets/front/fonts/ttf/iranyekanwebregularfanum.ttf') format('truetype');
    }

    body {
        margin: 0;
    }

    * {
        font-family: 'iranyekan', 'Roboto Condensed';
    }

    .single_content {
        padding-top: 60px;
        background-image: url({{env('APP_URL')}}/assets/front/images/bg_matlab.svg);
        background-repeat: no-repeat;
    }

    .page_content {
        background: #fff;
        padding: 30px 35px;
    }

    .rocket_title {
        margin-top: 8px;
        color: green;
        margin-bottom: 26px;
    }

    .text-center {
        text-align: center !important;
    }

    @media (min-width: 992px) {
        .container {
            padding: 0 50px;
        }
    }

    @media (max-width: 991px) {
        .container {
            padding: 0;
        }
    }

    .shadow {
        box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important;
    }

    .rounded {
        border-radius: .25rem !important;
    }

    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
    }

    .table-hover tbody tr:hover {
        color: #212529;
        background-color: rgba(0, 0, 0, .075);
    }

    .table td, .table th {
        border: 1px solid #dee2e6;
    }

    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #dee2e6;
    }

    a {
        color: #007bff;
        text-decoration: none;
        background-color: transparent;
    }

    a:hover {
        color: #0056b3;
        text-decoration: underline;
    }
</style>
<main>
    <section class="single_content">
        <div class="container">
            <div class="page_content rounded shadow">
                <div class="text-center">
                    @if($payment->status == 2)
                        <img src="{{asset('assets/front/images/rocket.svg')}}" width="70" alt="rocket">
                        <p class="rocket_title">پرداخت شما با موفقیت تکمیل شد!</p>
                    @elseif($payment->status == 1)
                        <img src="{{asset('assets/front/images/cancel-button.svg')}}" width="70" alt="cancel">
                        <p class="text-danger mt-2 mb-4">{{$payment->message ?? 'متاسفانه پرداخت شما تکمیل نشد!'}}</p>
                    @else
                        <img src="{{asset('assets/front/images/cancel-button.svg')}}" width="70" alt="cancel">
                        <p class="rocket_title">پرداخت انجام نشده است!</p>
                    @endif
                </div>

                <div class="edd_table">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <td>شناسه پرداخت :</td>
                            <td>{{$payment->transaction}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت پرداخت :</td>
                            <td>{{$payment->translateStatus()}}</td>
                        </tr>
                        <tr>
                            <td>روش پرداخت :</td>
                            <td>زرین پال</td>
                        </tr>
                        <tr>
                            <td>تاریخ :</td>
                            <td>{{jdate($payment->created_at)->format('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>کل :</td>
                            <td>{{number_format($payment->invoice->amount)}} تومان</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                @if($payment->status == \App\Models\Payment::STATUS_SUCCEED)
                    <h3 class="receipt_title">محصولات</h3>
                    <div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">نام</th>
                                <th scope="col">قیمت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payment->invoice->products as $product)
                                <tr>
                                    <td>
                                        <a href="{{route('products.show', [$product->id, $product->slug])}}"
                                           class="edd_purchase_receipt_product_name text-dark">
                                            {{$product->pivot->product_name}}
                                        </a>
                                        <ul class="edd_purchase_receipt_files">
                                            <li class="edd_download_file">
                                                <a href="{{route('products.download', $product->id)}}"
                                                   class="edd_download_file_link">
                                                    دانلود فایل
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        {{number_format($product->pivot->product_price)}} تومان
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </section>
</main>
</body>
</html>
