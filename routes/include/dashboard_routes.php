<?php

use App\Http\Controllers\Front\Dashboard\OrderController;
use App\Http\Controllers\Front\Dashboard\WithdrawController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\Dashboard\ProductController;
use App\Http\Controllers\Front\Dashboard\ProfileController;
use App\Http\Controllers\Front\Dashboard\DashboardController;
use App\Http\Controllers\Front\Dashboard\TicketController;

Route::name('dashboard.')->prefix('dashboard')->middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('index');
    Route::name('profile.')->prefix('profile')->group(function () {
        Route::get('/', [ProfileController::class, 'index'])->name('index');
        Route::put('/', [ProfileController::class, 'store'])->name('store');
        Route::get('phone', [ProfileController::class, 'editPhone'])->name('editPhone');
        Route::post('phone', [ProfileController::class, 'updatePhone'])->name('updatePhone');
        Route::get('verify-phone', [ProfileController::class, 'verfiyPhoneView'])->name('verfiyPhoneView');
        Route::post('verify-phone', [ProfileController::class, 'verifyPhone'])->name('verifyPhone');
    });
    Route::name('support.')->prefix('support')->group(function () {
        Route::get('/', [TicketController::class, 'index'])->name('index');
        Route::get('create', [TicketController::class, 'create'])->name('create');
        Route::post('/', [TicketController::class, 'store'])->name('store');
        Route::post('update/{ticket}', [TicketController::class, 'update'])->name('update');
        Route::get('{ticket}', [TicketController::class, 'show'])->name('show');
        Route::post('close/{ticket}', [TicketController::class, 'close'])->name('close');
    });
    Route::name('products.')->prefix('products')->group(function () {
        Route::get('/', [ProductController::class, 'index'])->name('index');
        Route::get('create', [ProductController::class, 'create'])->name('create');
        Route::post('/', [ProductController::class, 'store'])->name('store');
        Route::get('edit/{product}', [ProductController::class, 'edit'])->name('edit');
        Route::put('{product}', [ProductController::class, 'update'])->name('update');
        Route::get('remove/{product}', [ProductController::class, 'remove'])->name('remove');
        Route::delete('{product}', [ProductController::class, 'destroy'])->name('destroy');
//        Route::post('upload-image', [ProductController::class, 'uploadImage'])->name('uploadImage');
    });
    Route::get('withdraw', [WithdrawController::class, 'index'])->name('withdraw.index');
    Route::post('withdraw', [WithdrawController::class, 'store'])->name('withdraw.store');
    Route::get('orders', [OrderController::class, 'index'])->name('orders.index');
    Route::get('orders/{payment}', [OrderController::class, 'show'])->name('orders.show');
});
