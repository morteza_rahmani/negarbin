<?php

use App\Http\Controllers\Back\ArticleController;
use App\Http\Controllers\Back\AwardController;
use App\Http\Controllers\Back\BannerController;
use App\Http\Controllers\Back\CategoryController;
use App\Http\Controllers\Back\CommentController;
use App\Http\Controllers\Back\DashboardBannerController;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Back\PageController;
use App\Http\Controllers\Back\ProductController;
use App\Http\Controllers\Back\SearchController;
use App\Http\Controllers\Back\SettingController;
use App\Http\Controllers\Back\TagController;
use App\Http\Controllers\Back\TicketController;
use App\Http\Controllers\Back\TransactionController;
use App\Http\Controllers\Back\UserController;
use App\Http\Controllers\Back\WithdrawController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware('admin')->group(function () {
    // Dashboard
    Route::get('/', [DashboardController::class, 'index'])->name('admin.dashboard.index');
//    Route::get('/sales-chart', [DashboardController::class, 'chart'])->name('admin.dashboard.chart');
    // Users
    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('admin.users.index');
        Route::get('datatable', [UserController::class, 'datatable'])->name('admin.users.datatable');
        Route::get('{user}', [UserController::class, 'show'])->name('admin.users.show');
        Route::get('{user}/awards', [UserController::class, 'awards'])->name('admin.users.awards');
        Route::put('{user}/awards', [UserController::class, 'updateAwards'])->name('admin.users.updateAwards');
        Route::delete('{user}', [UserController::class, 'destroy'])->name('admin.users.destroy');
    });
    // Awards
    Route::prefix('awards')->group(function () {
        Route::get('/', [AwardController::class, 'index'])->name('admin.awards.index');
        Route::get('datatable', [AwardController::class, 'datatable'])->name('admin.awards.datatable');
        Route::get('create', [AwardController::class, 'create'])->name('admin.awards.create');
        Route::post('/', [AwardController::class, 'store'])->name('admin.awards.store');
        Route::get('{award}/edit', [AwardController::class, 'edit'])->name('admin.awards.edit');
        Route::put('{award}', [AwardController::class, 'update'])->name('admin.awards.update');
        Route::delete('{award}', [AwardController::class, 'destroy'])->name('admin.awards.destroy');
    });
    //Articles
    Route::prefix('articles')->group(function () {
        Route::get('/', [ArticleController::class, 'index'])->name('admin.articles.index');
        Route::get('datatable', [ArticleController::class, 'datatable'])->name('admin.articles.datatable');
        Route::get('create', [ArticleController::class, 'create'])->name('admin.articles.create');
        Route::post('/', [ArticleController::class, 'store'])->name('admin.articles.store');
        Route::get('{article}/edit', [ArticleController::class, 'edit'])->name('admin.articles.edit');
        Route::put('{article}', [ArticleController::class, 'update'])->name('admin.articles.update');
        Route::delete('{article}', [ArticleController::class, 'destroy'])->name('admin.articles.destroy');
    });
    // Pages
    Route::prefix('pages')->group(function () {
        Route::get('/', [PageController::class, 'index'])->name('admin.pages.index');
        Route::get('datatable', [PageController::class, 'datatable'])->name('admin.pages.datatable');
        Route::get('create', [PageController::class, 'create'])->name('admin.pages.create');
        Route::post('/', [PageController::class, 'store'])->name('admin.pages.store');
        Route::get('{page}/edit', [PageController::class, 'edit'])->name('admin.pages.edit');
        Route::put('{page}', [PageController::class, 'update'])->name('admin.pages.update');
        Route::delete('{page}', [PageController::class, 'destroy'])->name('admin.pages.destroy');
    });
    // Comments
    Route::prefix('comments')->group(function () {
        Route::get('/', [CommentController::class, 'index'])->name('admin.comments.index');
        Route::get('datatable', [CommentController::class, 'datatable'])->name('admin.comments.datatable');
        Route::post('{comment}/change-status', [CommentController::class, 'changeStatus'])->name('admin.comments.changeStatus');
        Route::get('{comment}/edit', [CommentController::class, 'edit'])->name('admin.comments.edit');
        Route::put('{comment}', [CommentController::class, 'update'])->name('admin.comments.update');
        Route::delete('{comment}', [CommentController::class, 'destroy'])->name('admin.comments.destroy');
    });
    // Support - Tickets
    Route::prefix('tickets')->group(function () {
        Route::get('/', [TicketController::class, 'index'])->name('admin.tickets.index');
        Route::get('datatable', [TicketController::class, 'datatable'])->name('admin.tickets.datatable');
        Route::get('create', [TicketController::class, 'create'])->name('admin.tickets.create');
        Route::post('create', [TicketController::class, 'store'])->name('admin.tickets.store');
        Route::get('{ticket}', [TicketController::class, 'show'])->name('admin.tickets.show');
        Route::post('{ticket}', [TicketController::class, 'update'])->name('admin.tickets.update');
        Route::delete('{ticket}', [TicketController::class, 'destroy'])->name('admin.tickets.destroy');
    });
    // Products
    Route::prefix('products')->group(function () {
        Route::get('/', [ProductController::class, 'index'])->name('admin.products.index');
        Route::get('datatable', [ProductController::class, 'datatable'])->name('admin.products.datatable');
        Route::post('/change-status/{product}', [ProductController::class, 'changeStatus'])->name('admin.products.changeStatus');
        Route::get('edit/{product}', [ProductController::class, 'edit'])->name('admin.products.edit');
        Route::put('{product}', [ProductController::class, 'update'])->name('admin.products.update');
        Route::delete('{product}', [ProductController::class, 'destroy'])->name('admin.products.destroy');
    });
    // Transactions
    Route::prefix('transactions')->group(function () {
        Route::get('/', [TransactionController::class, 'index'])->name('admin.transactions.index');
        Route::get('datatable', [TransactionController::class, 'datatable'])->name('admin.transactions.datatable');
        Route::get('{payment}', [TransactionController::class, 'show'])->name('admin.transactions.show');
    });
    // Withdraws
    Route::prefix('withdraws')->group(function () {
        Route::get('/', [WithdrawController::class, 'index'])->name('admin.withdraws.index');
        Route::get('datatable', [WithdrawController::class, 'datatable'])->name('admin.withdraws.datatable');
        Route::post('{withdraw}', [WithdrawController::class, 'changeStatus'])->name('admin.withdraws.changeStatus');
    });
    // Products Categories
    Route::prefix('categories')->group(function () {
        Route::get('/', [CategoryController::class, 'index'])->name('admin.categories.index');
        Route::post('/', [CategoryController::class, 'store'])->name('admin.categories.store');
        Route::get('edit/{category}', [CategoryController::class, 'edit'])->name('admin.categories.edit');
        Route::put('{category}', [CategoryController::class, 'update'])->name('admin.categories.update');
        Route::delete('{category}', [CategoryController::class, 'destroy'])->name('admin.categories.destroy');
    });
    // Settings
    Route::prefix('settings')->group(function () {
        Route::get('/general', [SettingController::class, 'index'])->name('admin.settings.general.index');
        Route::post('/general', [SettingController::class, 'store'])->name('admin.settings.general.store');
        Route::get('/banners', [BannerController::class, 'index'])->name('admin.settings.banners.index');
        Route::post('/banners', [BannerController::class, 'store'])->name('admin.settings.banners.store');
        Route::get('/dashboard-banners', [DashboardBannerController::class, 'index'])
            ->name('admin.settings.dashboard_banners.index');
        Route::post('/dashboard-banners', [DashboardBannerController::class, 'store'])
            ->name('admin.settings.dashboard_banners.store');
        Route::get('/searches', [SearchController::class, 'index'])->name('admin.settings.searches.index');
        Route::post('/searches', [SearchController::class, 'store'])->name('admin.settings.searches.store');
    });

    Route::get('suggestion-tags', [TagController::class, 'suggestionTags'])->name('admin.suggestionTags');
});
