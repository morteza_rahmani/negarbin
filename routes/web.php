<?php

use App\Http\Controllers\Front\ArticleController;
use App\Http\Controllers\Front\AuthController;
use App\Http\Controllers\Front\AuthorController;
use App\Http\Controllers\Front\CartController;
use App\Http\Controllers\Front\CKEditorController;
use App\Http\Controllers\Front\CommentController;
use App\Http\Controllers\Front\HeaderController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\InvoicesController;
use App\Http\Controllers\Front\PageController;
use App\Http\Controllers\Front\PaymentsController;
use App\Http\Controllers\Front\ProductController;
use App\Http\Controllers\Front\TagController;
use App\Jobs\UploadFile;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/render-header-categories/{category}', [HeaderController::class, 'renderHeaderCategories'])
    ->name('render-header-categories');

/* Authentication */
Route::prefix('ajax')->group(function () {
    Route::get('login-view', [AuthController::class, 'loginView'])->name('ajax.login.view');
    Route::post('login', [AuthController::class, 'doLogin'])->name('ajax.login');
    Route::get('register-view', [AuthController::class, 'registerView'])->name('ajax.register.view');
    Route::post('send-verify-code', [AuthController::class, 'sendVerifyCode'])->name('ajax.send_verify_code');
    Route::post('verify', [AuthController::class, 'doVerify'])->name('ajax.verify');
    Route::post('register', [AuthController::class, 'doRegister'])->name('ajax.register');
    Route::get('change-password.view', [AuthController::class, 'changePasswordView'])->name('ajax.change_password_view');
    Route::post('change-password-step-1', [AuthController::class, 'changePasswordStep1'])->name('ajax.change_password_1');
    Route::post('change-password-step-2', [AuthController::class, 'changePasswordStep2'])->name('ajax.change_password_2');
    Route::post('change-password-step-3', [AuthController::class, 'changePasswordStep3'])->name('ajax.change_password_3');
});
Route::get('email/verify/{token}', [AuthController::class, 'verifyEmail'])->name('email.verify');

/* Blog */
Route::prefix('blog')->group(function () {
    Route::get('/', [ArticleController::class, 'index'])->name('blog.index');
    Route::get('{article}', [ArticleController::class, 'show'])->name('blog.show');
});

/* Products */
Route::get('search', [ProductController::class, 'index'])->name('products.index');
Route::get('category/{category:slug}', [ProductController::class, 'archive'])->name('products.archive');
Route::get('p/{id}/{slug}', [ProductController::class, 'show'])->name('products.show');
Route::get('download/{product}', [ProductController::class, 'download'])->name('products.download');

/* CKEditor Upload Image */
Route::post('ckeditor/upload', [CKEditorController::class, 'upload'])->name('ckeditor.upload');

/* Purchase */
Route::middleware('auth')->group(function () {
    Route::get('cart', [CartController::class, 'index'])->name('cart');
    Route::post('add-to-cart/{product}', [CartController::class, 'addToCart'])->name('addToCart');
    Route::delete('delete-from-cart/{id}', [CartController::class, 'destroy'])->name('deleteFromCart');
    Route::post('invoices', [InvoicesController::class, 'store'])->name('invoice.store');
    Route::get('invoices/{invoice}/pay', [InvoicesController::class, 'pay'])->name('invoice.pay');
    Route::any('payments/{payment}/verify', [PaymentsController::class, 'verify'])->name('payment.verify');
    Route::get('receipt', [CartController::class, 'receipt'])->name('receipt');
});

/* Pages */
Route::get('page/{slug}', [PageController::class, 'show'])->name('page.show');
Route::get('empty-page', [PageController::class, 'emptyPage'])->name('emptyPage');
/* Author */
Route::get('author/{username}', [AuthorController::class, 'show'])->name('author.show');

/* Comments */
Route::post('comment/store', [CommentController::class, 'store'])->name('comment.add');

/* Tags */
Route::get('suggestion-tags', [TagController::class, 'suggestionTags'])->name('suggestionTags');

/* Clear Cache */
Route::get('/cache-clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:cache');
    Artisan::call('view:clear');
    Artisan::call('config:cache');

    dd("cache clear All");
});

/* Admin Panel */
include __DIR__ . '/include/admin_routes.php';

/* Dashboard */
include __DIR__ . '/include/dashboard_routes.php';

require __DIR__ . '/auth.php';
