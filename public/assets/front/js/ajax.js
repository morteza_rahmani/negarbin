$(document).ready(function () {
    /* Added By Morteza */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.primary-menu-item').each(function () {
        let itemID = $(this).attr('id')
        let headerCategoryRoute = $(this).data('route')
        $.ajax({
            url: headerCategoryRoute,
            type: 'get',
            success: function (data) {
                generateHeaderCategories(data.html, itemID)
            }
        })
    })

    const generateHeaderCategories = (html, itemID) => {
        let elements = $(html).children();
        for (let i = 1; i <= 4; i++) {
            let x = 20 * i / 4;
            $('#' + itemID + ' .megamenu-columns-wrapper div:nth-child(' + i + ') > ul').html(elements.slice(x - 5, x));
        }
        // let elements = $(html).children();
        // let length = elements.length;
        // let divided = Math.floor(length / 4); // 4.25
        // let remaining = length % 4
        // let num1 = 0, num2 = 0;
        // for (let i = 1; i <= 4 ; i++) {
        //     if (remaining > 0) {
        //         num2 = num1 + divided + 1
        //         remaining--
        //     } else {
        //         num2 = num1 + divided
        //     }
        //     let li_data = $(html).children().slice(num1, num2)
        //     $('#' + itemID + ' .megamenu-columns-wrapper div:nth-child(' + i + ') > ul').html(li_data)
        //     num1 = num2
        // }
    }

    var modalDescContent = $("#modal_desc_content");
    $(".showLoginPageButton").on('click', function () {
        if (modalDescContent.html().length <= 0) {
            const route = $(this).data('route');
            // AJAX request
            $.ajax({
                url: route,
                type: 'get',
                beforeSend: function () {
                    modalDescContent.addClass('disable-content')
                },
                complete: function () {
                    modalDescContent.removeClass('disable-content')
                },
                success: function (data) {
                    // Display Modal
                    $('#loginModalCenter').modal('show');
                    // Add response in Modal body
                    modalDescContent.html(data.html)
                }
            });
        } else {
            $('#loginModalCenter').modal('show');
        }
    });
    modalDescContent.on('submit', '#doLoginForm', function (e) {
        e.preventDefault()
        const route = $(this).data('route');
        let login_inputs = $('.login_inputs');
        let input_login = login_inputs.find('input[name="login"]').val()
        let input_pass = login_inputs.find('input[name="password"]').val()
        // AJAX request
        $.ajax({
            url: route,
            type: 'post',
            data: {
                'login': input_login,
                'password': input_pass
            },
            beforeSend: function () {
                $('.response-data').empty().removeClass('alert alert-success')
                modalDescContent.addClass('disable-content')
            },
            complete: function () {
                modalDescContent.removeClass('disable-content')
            },
            success: function (data) {
                modalDescContent.html('<div class="response-data alert alert-success"></div>')
                $('.response-data').html(data.success);
                location.reload();
            },
            error: function (data) {
                if (data.status === 422) {
                    let errors = $.parseJSON(data.responseText);
                    $('.response-data').show().addClass("alert alert-danger");
                    if ($.isPlainObject(errors['errors'])) {
                        $.each(errors['errors'], function (key, value) {
                            console.log(key + " " + value);
                            $('.response-data').append(value + "<br/>");
                        });
                    } else {
                        $('.response-data').append(errors['errors'] + "<br/>");
                    }
                }
            },
            fail: function (xhr, textStatus, errorThrown) {
                alert('عملیات با خطا مواجه شد!');
            }
        });
    });
    modalDescContent.on('click', '#changeLoginPageButton', function () {
        const route = $(this).data('route');
        // AJAX request
        $.ajax({
            url: route,
            type: 'get',
            beforeSend: function () {
                modalDescContent.addClass('disable-content')
            },
            complete: function () {
                modalDescContent.removeClass('disable-content')
            },
            success: function (data) {
                // Display Modal
                $('#loginModalCenter').modal('show');
                // Add response in Modal body
                modalDescContent.html(data.html)
            },
        });
    });
    modalDescContent.on('submit', '#verifyPhoneForm', function (e) {
        e.preventDefault()
        const route = $(this).data('route');
        let input_phone = $('.login_inputs').find('input[name="phone"]').val()
        // AJAX request
        $.ajax({
            url: route,
            type: 'post',
            data: {'phone': input_phone},
            beforeSend: function () {
                $('.response-data').empty().removeClass('alert alert-success')
                modalDescContent.addClass('disable-content')
            },
            complete: function () {
                modalDescContent.removeClass('disable-content')
            },
            success: function (data) {
                modalDescContent.html(data.html)
                let timer2 = "2:01"
                let interval = setInterval(function () {
                    // Get today's date and time
                    let timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    let minutes = parseInt(timer[0], 10);
                    let seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    if (minutes < 0) clearInterval(interval);
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    if (minutes < 0) {
                        clearInterval(interval);
                        $('.send-verify_code-again').attr('data-phone', data.phone)
                            .removeClass('disable-content');
                    } else {
                        $('.verify_phone-countdown').html(minutes + ':' + seconds);
                        timer2 = minutes + ':' + seconds;
                    }
                }, 1000);
            },
            error: function (data) {
                if (data.status === 422) {
                    let errors = $.parseJSON(data.responseText);
                    $('.response-data').show().addClass("alert alert-danger");
                    if ($.isPlainObject(errors['errors'])) {
                        console.log('errorsIsObject')
                        $.each(errors['errors'], function (key, value) {
                            $('.response-data').append(value + "<br/>");
                        });
                    } else {
                        console.log('errorsIsNotObject')
                        $('.response-data').append(errors['errors'] + "<br/>");
                    }
                }
            },
            fail: function (xhr, textStatus, errorThrown) {
                alert('عملیات با خطا مواجه شد!');
            }
        });
    });
    modalDescContent.on('click', '.send-verify_code-again', function () {
        const route = $(this).data('route');
        let input_phone = $(this).data('phone')
        // AJAX request
        $.ajax({
            url: route,
            type: 'post',
            data: {'phone': input_phone},
            beforeSend: function () {
                $('.response-data').empty().removeClass('alert alert-success')
                modalDescContent.addClass('disable-content')
            },
            complete: function () {
                modalDescContent.removeClass('disable-content')
            },
            success: function (data) {
                modalDescContent.html(data.html)
                let timer2 = "2:01"
                let interval = setInterval(function () {
                    // Get today's date and time
                    let timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    let minutes = parseInt(timer[0], 10);
                    let seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    if (minutes < 0) clearInterval(interval);
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    if (minutes < 0) {
                        clearInterval(interval);
                        $('.send-verify_code-again').attr('data-phone', data.phone)
                            .removeClass('disable-content');
                    } else {
                        $('.verify_phone-countdown').html(minutes + ':' + seconds);
                        timer2 = minutes + ':' + seconds;
                    }
                }, 1000);
            },
            error: function (data) {
                if (data.status === 422) {
                    let errors = $.parseJSON(data.responseText);
                    $('.response-data').show().addClass("alert alert-danger");
                    if ($.isPlainObject(errors['errors'])) {
                        $.each(errors['errors'], function (key, value) {
                            $('.response-data').append(value + "<br/>");
                        });
                    } else {
                        $('.response-data').append(errors['errors'] + "<br/>");
                    }
                }
            },
            fail: function (xhr, textStatus, errorThrown) {
                alert('عملیات با خطا مواجه شد!');
            }
        });
    });
    modalDescContent.on('submit', '#doVerifyForm, #changePasswordStep2', function (e) {
        e.preventDefault()
        const route = $(this).data('route');
        let input_verify_code = $('.login_inputs').find('input[name="verify_code"]').val()
        // AJAX request
        $.ajax({
            url: route,
            type: 'post',
            data: {'verify_code': input_verify_code},
            beforeSend: function () {
                $('.response-data').empty().removeClass('alert alert-success')
                modalDescContent.addClass('disable-content')
            },
            complete: function () {
                modalDescContent.removeClass('disable-content')
            },
            success: function (data) {
                modalDescContent.html(data.html)
            },
            error: function (data) {
                if (data.status === 422) {
                    let errors = $.parseJSON(data.responseText);
                    $('.response-data').show().addClass("alert alert-danger");
                    if ($.isPlainObject(errors['errors'])) {
                        $.each(errors['errors'], function (key, value) {
                            $('.response-data').append(value + "<br/>");
                        });
                    } else {
                        $('.response-data').append(errors['errors'] + "<br/>");
                    }
                }
            },
            fail: function (xhr, textStatus, errorThrown) {
                alert('عملیات با خطا مواجه شد!');
            }
        });
    });
    modalDescContent.on('submit', '#registerPasswordForm, #changePasswordStep3', function (e) {
        e.preventDefault()
        const route = $(this).data('route');
        let login_inputs = $('.login_inputs');
        let input_password = login_inputs.find('input[name="password"]').val()
        let input_password_confirmation = login_inputs.find('input[name="password_confirmation"]').val()
        // AJAX request
        $.ajax({
            url: route,
            type: 'post',
            data: {
                'password': input_password,
                'password_confirmation': input_password_confirmation,
            },
            beforeSend: function () {
                $('.response-data').empty().removeClass('alert alert-success')
                modalDescContent.addClass('disable-content')
            },
            complete: function () {
                modalDescContent.removeClass('disable-content')
            },
            success: function (data) {
                modalDescContent.html('<div class="response-data alert alert-success"></div>')
                $('.response-data').html(data.success);
                location.reload();
            },
            error: function (data) {
                if (data.status === 422) {
                    let errors = $.parseJSON(data.responseText);
                    $('.response-data').show().addClass("alert alert-danger");
                    if ($.isPlainObject(errors['errors'])) {
                        $.each(errors['errors'], function (key, value) {
                            $('.response-data').append(value + "<br/>");
                        });
                    } else {
                        $('.response-data').append(errors['errors'] + "<br/>");
                    }
                }
            },
            fail: function (xhr, textStatus, errorThrown) {
                alert('عملیات با خطا مواجه شد!');
            }
        });
    });
})
