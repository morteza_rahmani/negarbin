$(document).ready(function () {
    $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function () {
        $(this).toggleClass('open');
    });
    $("#nav-icon3").click(function () {
        $("#black-box").toggle();
    });


    $("#allcontent").on('click', function () {
        if ($(this).parent().hasClass("opened")) {
            $(this).parent().removeClass('opened');
        } else {
            $(this).parent().addClass('opened');
        }
        ;
    });


    $("#allcontentsrh").on('click', function () {
        if ($(this).parent().hasClass("opened")) {
            $(this).parent().removeClass('opened');
        } else {
            $(this).parent().addClass('opened');
        }
        ;
    });


    $(".sub_title_hook").on('click', function () {
        if ($(this).parent().hasClass('opened')) {
            $(".sub_title_hook").parent().removeClass('opened');
        } else {
            $(".sub_title_hook").parent().removeClass('opened');
            $(this).parent().addClass('opened');
        }
    });

    $("#menu_profile").on('click', function () {
        $("#dropdownlogin").toggle();
    });


    // all content click , click all
    $("#input0,#innerinput0").on('click', function () {
        if ($(this).hasClass('checkall')) {
            $(this).parents('ul').find('li input').removeAttr('checked');
            $(this).removeClass('checkall');
        } else {
            $(this).parents('ul').find('li input').attr('checked', 'checked');
            $(this).addClass('checkall');
        }
    });

    $(".burger_item>a").on('click', function () {
        if ($(this).parent().find("ul:eq(0)").hasClass('black_box_open')) {
            $(this).parent().find('ul:eq(0)').removeClass('black_box_open');
            $(this).parent().removeClass('burger_item_active');
        } else {
            $(this).parent().find('ul:eq(0)').addClass('black_box_open');
            $(this).parent().addClass('burger_item_active');
        }
    });

    $(".has_sub_group>a").click(function () {
        if ($(this).parent().hasClass('black_box_2_open')) {
            $(this).parent().removeClass('black_box_2_open');
        } else {
            $(this).parent().addClass('black_box_2_open');
        }
    })

}); //document.ready(function)

// File Upload for uploader
//
function ekUpload() {
    function Init() {
        console.log("Upload Initialised");

        var fileSelect = document.getElementById('file-upload'),
            fileDrag = document.getElementById('file-drag'),
            submitButton = document.getElementById('submit-button');

        if (fileSelect) {
            fileSelect.addEventListener('change', fileSelectHandler, false);

            // Is XHR2 available?
            var xhr = new XMLHttpRequest();
            if (xhr.upload) {
                // File Drop
                fileDrag.addEventListener('dragover', fileDragHover, false);
                fileDrag.addEventListener('dragleave', fileDragHover, false);
                fileDrag.addEventListener('drop', fileSelectHandler, false);
            }
        }
    }

    function fileDragHover(e) {
        var fileDrag = document.getElementById('file-drag');

        e.stopPropagation();
        e.preventDefault();

        fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var files = e.target.files || e.dataTransfer.files;

        // Cancel event and hover styling
        fileDragHover(e);

        // Process all File objects
        for (var i = 0, f; f = files[i]; i++) {
            parseFile(f);
            uploadFile(f);
        }
    }

// Output
    function output(msg) {
        // Response
        var m = document.getElementById('messages');
        m.innerHTML = msg;
    }

    function parseFile(file) {

        console.log(file.name);
        output(
            '<strong>' + encodeURI(file.name) + '</strong>'
        );

        // var fileType = file.type;
        // console.log(fileType);
        var imageName = file.name;

        var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
        if (isGood) {
            document.getElementById('start').classList.add("hidden");
            document.getElementById('response').classList.remove("hidden");
            document.getElementById('notimage').classList.add("hidden");
            // Thumbnail Preview
            document.getElementById('file-image').classList.remove("hidden");
            document.getElementById('file-image').src = URL.createObjectURL(file);
        } else {
            document.getElementById('file-image').classList.add("hidden");
            document.getElementById('notimage').classList.remove("hidden");
            document.getElementById('start').classList.remove("hidden");
            document.getElementById('response').classList.add("hidden");
            document.getElementById("file-upload-form").reset();
        }
    }

    function setProgressMaxValue(e) {
        var pBar = document.getElementById('file-progress');

        if (e.lengthComputable) {
            pBar.max = e.total;
        }
    }

    function updateFileProgress(e) {
        var pBar = document.getElementById('file-progress');

        if (e.lengthComputable) {
            pBar.value = e.loaded;
        }
    }

    function uploadFile(file) {

        var xhr = new XMLHttpRequest(),
            fileInput = document.getElementById('class-roster-file'),
            pBar = document.getElementById('file-progress'),
            fileSizeLimit = 1024; // In MB
        if (xhr.upload) {
            // Check if file is less than x MB
            if (file.size <= fileSizeLimit * 1024 * 1024) {
                // Progress bar
                pBar.style.display = 'inline';
                xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
                xhr.upload.addEventListener('progress', updateFileProgress, false);

                // File received / failed
                xhr.onreadystatechange = function (e) {
                    if (xhr.readyState == 4) {
                        // Everything is good!

                        // progress.className = (xhr.status == 200 ? "success" : "failure");
                        // document.location.reload(true);
                    }
                };

                // Start upload
                xhr.open('POST', document.getElementById('file-upload-form').action, true);
                xhr.setRequestHeader('X-File-Name', file.name);
                xhr.setRequestHeader('X-File-Size', file.size);
                xhr.setRequestHeader('Content-Type', 'multipart/form-data');
                xhr.send(file);
            } else {
                output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
            }
        }
    }

// Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    } else {
        document.getElementById('file-drag').style.display = 'none';
    }
}

ekUpload();
