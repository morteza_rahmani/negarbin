<?php


namespace App\Classes;


use Carbon\Carbon;

class GenerateSecureLink
{
    public function __construct($url, $time, $path, $ip)
    {
        $this->url = $url;
        $this->time = $time;
        $this->path = $path;
        $this->ip = $ip;
    }

    public function generateSecureLink(): string
    {
        $secret = env('SOME_SECRET');

        $hash = md5("{$this->time}{$this->path}{$this->ip} $secret", true);
        $hash = base64_encode($hash);
        $hash = strtr($hash, '+/', '-_');
        $hash = str_replace('=', '', $hash);

        return "{$this->url}?md5={$hash}&expires={$this->time}";
    }
}
