<?php


namespace App\Classes;


use App\Models\Category;

class AddCategories
{
    public function addCategories($cat_ids)
    {
        foreach ($cat_ids as $cat_id) {
            $cat = Category::where('id', $cat_id)->first();
            $cat_parent = $cat->parent();
            if (isset($cat_parent)) {
                if (!in_array($cat_parent->id, $cat_ids))
                    array_push($cat_ids, $cat_parent->id);
                $cat_grandpa = $cat_parent->parent();
                if (isset($cat_grandpa) and !in_array($cat_grandpa->id, $cat_ids)) {
                    array_push($cat_ids, $cat_grandpa->id);
                }
            }
        }
        return $cat_ids;
    }
}
