<?php


namespace App\Repository;


use Illuminate\Support\Facades\Storage;

class BannerRepository
{
    public function storeImage($file, $currentFile, $type): ?string
    {
        if (is_file($file)) {
            // Code for remove old file
            if (isset($currentFile) && Storage::disk('ftp')->exists(env($type) . $currentFile)) {
                Storage::disk('ftp')->delete(env($type) . $currentFile);
            }
            // Upload new file
            $fileExtension = $file->getClientOriginalExtension();
            $filename = uniqid() . '.' . $fileExtension;
            Storage::disk('ftp')->put(env($type) . $filename, fopen($file, 'r+'));
            return $filename;
        }
        return $currentFile;
    }
}
