<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DashboardBanner extends Model
{
    use HasFactory;

    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'banner1_image', 'banner1_link',
        'banner2_image', 'banner2_link',
        'banner3_image', 'banner3_link',
    ];
}
