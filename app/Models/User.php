<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'phone', 'avatar', 'IBAN',
        'state', 'city', 'is_admin', 'password', 'total_revenue', 'outstanding',
        'email_verified_at', 'phone_verified_at', 'is_email_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'total_revenue' => 'integer',
        'outstanding' => 'integer',
        'is_admin' => 'boolean'
    ];

    public function tokens(): HasMany
    {
        return $this->hasMany(Token::class);
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    public function getAdjustedAvatarAttribute(): string
    {
        return isset($this->avatar) ?
            env('STORAGE_ADDRESS') . env('AVATARS') . $this->avatar :
            asset('assets/front/images/no-photo.jpg');
    }

    public function tickets(): HasMany
    {
        return $this->hasMany(Ticket::class);
    }

    /* User's products */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function awards(): BelongsToMany
    {
        return $this->belongsToMany(Award::class, 'award_user');
    }

    public function cartProducts(): HasMany
    {
        return $this->hasMany(Cart::class);
    }

    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class);
    }

    /* User's purchased products */
    public function purchasedProducts(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_user')
            ->withPivot(['product_price', 'product_name']);
    }

    public function purchased($id): bool
    {
        return $this->purchasedProducts()->where('id', $id)->count() > 0;
    }

    public function withdraws(): HasMany
    {
        return $this->hasMany(Withdraw::class);
    }
}
