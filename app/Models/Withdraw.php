<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Withdraw extends Model
{
    use HasFactory;

    const STATUS_NEW = 0;
    const STATUS_UNDER = 1;
    const STATUS_FAILED = 2;
    const STATUS_SUCCEED = 3;

    protected $fillable = [
        'user_id', 'amount', 'status'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function translateStatus(): string
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'ثبت درخواست';
            case self::STATUS_UNDER:
                return 'در دست بررسی';
            case self::STATUS_FAILED:
                return 'واریز نشد';
            case self::STATUS_SUCCEED:
                return 'واریز شد';
            default:
                return 'نامشخص';
        }
    }

    public function colorStatus(): string
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'info';
            case self::STATUS_UNDER:
                return 'warning';
            case self::STATUS_FAILED:
                return 'danger';
            case self::STATUS_SUCCEED:
                return 'success';
            default:
                return 'dark';
        }
    }
}
