<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory, Sluggable;
    protected $fillable = [
        'parent_id', 'name', 'slug', 'icon', 'depth', 'priority'
    ];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'category_product');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        if (isset($this->parent_id)) {
            return Category::where('id', $this->parent_id)->first();
        }
        return null;
    }

    public function allChildrenCount()
    {
        $count1 = $this->children()->count();
        $count2 = 0;
        foreach ($this->children as $child) {
            $count2 += $child->children()->count();
        }
        return $count1 + $count2;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
