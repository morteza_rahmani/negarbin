<?php

namespace App\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Http;

class Token extends Model
{
    use HasFactory;

    const EXPIRATION_TIME = 15; // minutes

    protected $fillable = [
        'code',
        'user_id',
        'used'
    ];

    public function __construct(array $attributes = [])
    {
        if (! isset($attributes['code'])) {
            $attributes['code'] = $this->generateCode();
        }

        parent::__construct($attributes);
    }

    /**
     * Generate a six digits code
     *
     * @param int $codeLength
     * @return string
     */
    public function generateCode(int $codeLength = 5): string
    {
        $max = pow(10, $codeLength);
        $min = $max / 10 - 1;
        return mt_rand($min, $max);
    }

    /**
     * User tokens relation
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * True if the token is not used nor expired
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return ! $this->isUsed() && ! $this->isExpired();
    }

    /**
     * Is the current token used
     *
     * @return bool
     */
    public function isUsed(): bool
    {
        return $this->used;
    }

    /**
     * Is the current token expired
     *
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->created_at->diffInMinutes(Carbon::now()) > static::EXPIRATION_TIME;
    }

    /**
     * @throws Exception
     */
    public function sendCode()
    {
        if (! $this->user) {
            throw new Exception("هیچ کاربری با این توکن پیوست نشده است.");
        }

        if (! $this->code) {
            $this->code = $this->generateCode();
        }

        try {
            Http::withHeaders([
                'Authorization' => env('FARAZSMS_APIKEY')
            ])->post("http://rest.ippanel.com/v1/messages/patterns/send", [
                'pattern_code' => 'kuy2a5sme7',
                'originator' => env('FARAZSMS_ORIGINATOR'),
                'recipient' => $this->user->phone,
                'values' => [
                    'code' => $this->code
                ]
            ]);
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }
}
