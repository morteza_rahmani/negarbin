<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Article extends Model
{
    use HasFactory, Sluggable;

    protected $fillable = [
        'user_id', 'title', 'name', 'slug', 'thumbnail', 'thumbnail_alt', 'content', 'summary', 'tags',
        'keywords', 'views'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getThumbnailUrlAttribute()
    {
        return $this->thumbnail ?? asset('assets/front/images/bussiness.png');
    }
}
