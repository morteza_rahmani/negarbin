<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    const STATUS_NEW = 0;
    const STATUS_FAILED = 1;
    const STATUS_SUCCEED = 2;

    protected $table = "payments";

    protected $fillable = [
        'transaction', 'receipt', 'status', 'message', 'gateway', 'jdate'
    ];

    protected $casts = [
        'jdate' => 'datetime:Y-m-d',
    ];

    /**
     * set payment's status
     *
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        return tap($this->forceFill(['status' => $status]))->save();
    }

    /**
     * set payment's transaction
     *
     * @param string $transactionId
     *
     * @return self
     */
    public function setTransaction(string $transactionId): self
    {
        return tap($this->forceFill(['transaction' => $transactionId]))->save();
    }

    /**
     * set payment's receipt
     *
     * @param string $receipt
     *
     * @return self
     */
    public function setReceipt(string $receipt): self
    {
        return tap($this->forceFill(['receipt' => $receipt]))->save();
    }

    /**
     * Retrieve related invoice
     *
     * @return mixed
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function translateStatus(): string
    {
        switch ($this->status) {
            case self::STATUS_NEW :
                return 'پرداخت نشده';
            case self::STATUS_FAILED :
                return 'ناموفق';
            case self::STATUS_SUCCEED :
                return 'موفق';
            default:
                return 'نامشخص';
        }
    }
}
