<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'banner1_image', 'banner1_username', 'banner1_user_avatar', 'banner1_user_url',
        'banner2_image', 'banner2_link',
        'banner3_image', 'banner3_link', 'banner3_icon',
        'banner4_image', 'banner4_link', 'banner4_icon',
        'banner5_image', 'banner5_link', 'banner5_icon',
    ];
}
