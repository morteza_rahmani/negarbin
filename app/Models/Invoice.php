<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoices';

    protected $fillable = [
        'user_id', 'amount', 'email'
    ];

    protected $casts = [
        'amount' => 'integer',
    ];

    /**
     * Retrieve related payments
     *
     * @return mixed
     */
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * Determine if current invoice has paid.
     *
     * @return bool
     */
    public function hasPaid(): bool
    {
        return $this->payments()->where('status', Payment::STATUS_SUCCEED)->count() > 0;
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'invoice_product')
            ->withPivot(['product_price', 'product_name']);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
