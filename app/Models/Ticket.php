<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'parent_id', 'ticket_id', 'title', 'message', 'is_response', 'status'
    ];

    protected $casts = [
        'is_response' => 'boolean'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function responses(): HasMany
    {
        return $this->hasMany(Ticket::class, 'parent_id');
    }

    public function statusColor(): string
    {
        switch ($this->status) {
            case 'در انتظار پاسخ' :
                return 'warning';
            case 'بسته' :
                return 'danger';
            case 'پاسخ کاربر' :
                return 'info';
            case 'پاسخ پشتیبان' :
                return 'success';
            default :
                return 'primary';
        }
    }
}
