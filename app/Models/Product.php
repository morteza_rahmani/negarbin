<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    use HasFactory, Sluggable, Filterable;

    protected $fillable = [
        'user_id', 'name', 'slug', 'thumbnail', 'content', 'summary', 'file',
        'price', 'format', 'size', 'status', 'views', 'score', 'votes', 'purchases'
    ];

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'category_product');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
                'unique' => false
            ]
        ];
    }

    public function statusText(): string
    {
        switch ($this->status) {
            case 0:
                return 'رد شده';
            case 3:
            case 1:
                return 'در دست بررسی';
            case 2:
                return 'منتشر شده';
            default:
                return 'نامشخص';
        }
    }

    public function statusColor(): string
    {
        switch ($this->status) {
            case 0:
                return 'danger';
            case 1:
                return 'warning';
            case 2:
                return 'success';
            case 3:
                return 'info';
            default:
                return 'secondary';
        }
    }

    public function inCart(): bool
    {
        $count = Auth::user()->cartProducts()->where('product_id', $this->id)->count();
        if ($count > 0) {
            return true;
        }
        return false;
    }

    public function invoices(): BelongsToMany
    {
        return $this->belongsToMany(Invoice::class, 'invoice_product')
            ->withPivot(['product_price', 'product_name']);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'product_user')
            ->withPivot(['product_price', 'product_name']);
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'tag_product');
    }
}
