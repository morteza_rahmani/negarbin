<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class DashboardController extends Controller
{
    public function index()
    {
        $payments = Payment::where('status', Payment::STATUS_SUCCEED)->latest()->limit(5)->get();
        $tickets = Ticket::whereNull('parent_id')->latest('updated_at')->limit(5)->get();
        $chartData = $this->chart()->getData();
        $pDates = $chartData->pDates;
        $pCount = json_encode($chartData->pCount);
        $pAmount = json_encode($chartData->pAmount);
        return view('back.index', compact(['payments', 'tickets', 'pDates', 'pCount', 'pAmount']));
    }

    public function chart()
    {
        $md = jdate()->subYears()->format('Y-m-d');
        $payments = Payment::where('status', Payment::STATUS_SUCCEED)->whereDate('jdate', '>=', $md)
            ->latest()->select('id', 'invoice_id', 'jdate');

        $pDates = [];
        $pCount = [];
        $pAmount = [];

        for ($i = 0; $i < 12; $i++) {
            if ($i == 0) {
                $end = jdate()->format('Y-m-d');
                $start = jdate()->subDays(jdate()->getDay() - 1)->format('Y-m-d');
            } else {
                $end = Jalalian::fromFormat('Y-m-d', $start)->subDays()->format('Y-m-d');
                $start = Jalalian::fromFormat('Y-m-d', $start)->subMonths()->format('Y-m-d');
            }

            array_push($pDates, Jalalian::fromFormat('Y-m-d', $start)->format('%B'));
            $pim = $payments->whereBetween('jdate', [$start, $end]);
            $aim = 0; // Amount (price) given in this month.
            $salesNum = 0; // Sales number.
            foreach ($pim->get() as $p) {
                $aim += $p->invoice->amount;
                $salesNum += $p->invoice->products->count();
            }
            array_push($pAmount, $aim);
            array_push($pCount, $salesNum);
        }

        return response()->json([
            'pDates' => array_reverse($pDates),
            'pCount' => array_reverse($pCount),
            'pAmount' => array_reverse($pAmount)
        ]);
    }
}
