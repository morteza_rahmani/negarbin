<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Search;
use Exception;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        $searches = Search::first();
        return view('back.settings.searches', compact('searches'));
    }

    public function store(Request $request)
    {
        $searches = Search::first();
        try {
            $searches->update($request->all());
        } catch (Exception $exception) {
            dd($exception);
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا بعدا دوباره امتحان کنید.']);
        }
        return back()->with(['success' => 'تنظیمات جستجو با موفقیت ثبت گردید.']);
    }
}
