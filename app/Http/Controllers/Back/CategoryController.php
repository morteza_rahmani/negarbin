<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Ticket;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    /**
     * Display list of categories and create category form
     */
    public function index()
    {
        $categories = Category::where('parent_id', null)->orderBy('priority')->get();
        return view('back.categories.index', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => ['required', 'unique:categories'],
            'slug' => ['unique:categories'],
        ]);

        $slug = empty($request->input('slug')) ?
            SlugService::createSlug(Category::class, 'slug', $request->input('name')) :
            SlugService::createSlug(Category::class, 'slug', $request->input('slug'));

        if (empty($request->parent_id)) {
            $depth = 0;
        } else {
            $parent_category = Category::find($request->parent_id);
            $depth = $parent_category->depth + 1;
        }

        if ($depth > 2)
            return back()->with(['warning' => 'عمق دسته بندی خارج از محدوده!']);

        try {
            Category::create([
                'name' => $request->name,
                'slug' => $slug,
                'parent_id' => $request->parent_id,
                'icon' => $request->icon,
                'depth' => $depth,
                'priority' => $request->priority
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->back()->with('success', 'مطلب جدید با موفقیت ذخیره شد.');
    }

    public function edit(Category $category)
    {
        $categories = Category::whereNull('parent_id')->get();
        return view('back.categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @throws ValidationException
     */
    public function update(Request $request, Category $category): RedirectResponse
    {
        $this->validate($request, [
            'name' => ['required', 'unique:categories,name,' . $category->id . ''],
            'slug' => ['unique:categories,slug,' . $category->id . ''],
        ]);

        if (empty($request->input('slug'))) {
            $slug = SlugService::createSlug(Category::class, 'slug', $request->input('name'));
        } elseif ($request->input('slug') == $category->slug) {
            $slug = $request->input('slug');
        } else {
            $slug = SlugService::createSlug(Category::class, 'slug', $request->input('slug'));
        }

        if (empty($request->parent_id)) {
            $depth = 0;
        } else {
            $parent_category = Category::find($request->parent_id);
            $depth = $parent_category->depth + 1;
        }

        if ($depth > 2)
            return back()->with(['warning' => 'عمق دسته بندی خارج از محدوده!']);

        try {
            $category->update([
                'name' => $request->name,
                'slug' => $slug,
                'parent_id' => $request->parent_id,
                'icon' => $request->icon,
                'depth' => $depth,
                'priority' => $request->priority
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.categories.index')->with('success', 'مطلب جدید با موفقیت ذخیره شد.');
    }

    public function destroy(Category $category): RedirectResponse
    {
        try {
            if ($category->children->count() > 0) {
                foreach ($category->children as $child) {
                    $child->decrement('depth');
                    if ($child->children->count() > 0) {
                        foreach ($child->children as $child) {
                            $child->decrement('depth');
                        }
                    }
                }
            }
            $category->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.categories.index')->with('success', 'دسته بندی مورد نظر با موفقیت حذف شد.');
    }
}
