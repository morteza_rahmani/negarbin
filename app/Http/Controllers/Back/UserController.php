<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Award;
use App\Models\User;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('back.users.index');
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = User::where('is_admin', 0)->latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('details', function ($row) {
                    $thumbnail = $row->adjusted_avatar;
                    $name = $row->full_name;
                    $email = $row->phone;
                    return '<div class="d-flex">
                                <div>
                                    <a href="' . route('author.show', $row->username) . '" title="نمایش">
                                        <img class="avatar d-block" alt="avatar" src="' . $thumbnail . '">
                                    </a>
                                </div>
                                <div class="d-flex flex-column" style="margin-right:12px;">
                                    <a href="' . route('author.show', $row->username) . '"><strong>' . $name . '</strong></a>
                                    <small>' . $email . '</small>
                                </div>
                            </div>';
                })
                ->addColumn('products', function ($row) {
                    return $row->products()->where('status', 2)->count();
                })
                ->addColumn('sales', function ($row) {
                    return $row->products->sum('purchases');
                })
                ->addColumn('action', function ($row) {
                    return '<div class="widget-options">' .
                        '<div class="dropdown">' .
                        '<button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">' .
                        '<i class="la la-ellipsis-v"></i>' .
                        '</button>' .
                        '<div class="dropdown-menu px-0" x-placement="bottom-start">' .
                        '<a href="' . route('admin.users.show', $row->id) . '" class="dropdown-item px-4">' .
                        '<i class="la la-eye"></i>مشاهده' .
                        '</a>' .
                        '<a href="' . route('admin.users.awards', $row->id) . '" class="dropdown-item px-4">' .
                        '<i class="la la-certificate"></i>جوایز' .
                        '</a>' .
                        '<form action="' . route('admin.users.destroy', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0" onsubmit="return confirm(\'آیا از حذف این کاربر اطمینان دارید؟ در صورت حذف کاربر، تمام محصولات آن نیز حذف می شوند.\')">' .
                        csrf_field() .
                        method_field('delete') .
                        '<button type="submit"/>' .
                        '<i class="la la-trash-o"></i>حذف' .
                        '</button>' .
                        '</form>' .
                        '</div>' .
                        '</div>' .
                        '</div>';
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d');
                })
                ->rawColumns(['details', 'products', 'sales', 'action', 'created_at'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function show(User $user)
    {
        return view('back.users.show', compact('user'));
    }

    public function awards(User $user)
    {
        $awards = Award::all();
        return view('back.users.awards', compact('user', 'awards'));
    }

    public function updateAwards(Request $request, User $user): RedirectResponse
    {
        try {
            $user->awards()->sync($request->awards);
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.users.index')->with('success', 'جوایز کاربر مورد نظر با موفقیت ویرایش شد.');
    }

    public function destroy(User $user): RedirectResponse
    {
        try {
            $user->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.users.index')->with('success', 'کاربر مورد نظر با موفقیت حذف شد.');
    }
}
