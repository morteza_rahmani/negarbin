<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\DashboardBanner;
use App\Repository\BannerRepository;
use Exception;
use Illuminate\Http\Request;

class DashboardBannerController extends Controller
{
    public function index()
    {
        $banners = DashboardBanner::firstOrFail();
        return view('back.settings.dashboard_banners', compact('banners'));
    }

    public function store(Request $request)
    {
        $banners = DashboardBanner::firstOrFail();
        $banners_image = [];
        for ($i = 1; $i <= 3; $i++) {
            $banners_image[$i - 1] = resolve(BannerRepository::class)
                ->storeImage($request->file("banner{$i}_file"), $banners->value("banner{$i}_image"), 'BANNERS');
        }
        $request->merge([
            'banner1_image' => $banners_image[0],
            'banner2_image' => $banners_image[1],
            'banner3_image' => $banners_image[2],
        ]);
        try {
            $banners->update($request->all());
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا بعدا دوباره امتحان کنید.']);
        }
        return back()->with(['success' => 'تغییرات با موفقیت ثبت گردید.']);
    }
}
