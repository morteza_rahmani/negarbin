<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::firstOrFail();
        return view('back.settings.general', compact('settings'));
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'commission' => ['required', 'numeric', 'min:0', 'max:100'],
            'min_settlement_amount' => ['required', 'numeric', 'min:0'],
        ]);
        try {
            Setting::first()->update($request->all());
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا دوباره تلاش کنید.']);
        }
        return back()->with(['success' => 'تنظیمات با موفقیت ذخیره شد.']);
    }
}
