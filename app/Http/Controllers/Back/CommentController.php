<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('back.comments.index');
    }

    /**
     * Return datatable of the resource.
     *
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Comment::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('author', function ($row) {
                    if (empty($row->user_id)) {
                        $thumbnail = asset('assets/front/images/no-photo.jpg');
                        $name = $row->name;
                        $email = $row->email;
                    } else {
                        $thumbnail = $row->user->adjusted_avatar;
                        $name = $row->user->full_name;
                        $email = $row->user->email ?? $row->user->phone;
                    }
                    return '<div class="d-flex">
                                <div>
                                    <a href="#" title="نمایش">
                                        <img class="avatar d-block" alt="avatar" src="' . $thumbnail . '">
                                    </a>
                                </div>
                                <div class="d-flex flex-column" style="margin-right:12px;">
                                    <a href="#"><strong>' . $name . '</strong></a>
                                    <small>' . $email . '</small>
                                </div>
                            </div>';
                })
                ->addColumn('content', function ($row) {
                    return '<a href="#">' . $row->content . '</a>';
                })
                ->addColumn('post', function ($row) {
                    if ($row->commentable_type == 'App\Models\Product') {
                        $route = route('products.show', [$row->commentable->id, $row->commentable->slug]). '#comments-section';
                    } elseif ($row->commentable_type == 'App\Models\Article') {
                        $route = route('blog.show', $row->commentable->slug). '#comments-section';
                    }
                    return '<a href="' . $route . '">' . $row->commentable->name . '</a>';
                })
                ->addColumn('status', function ($row) {
                    if ($row->status == 2) {
                        $status = 'در انتظار';
                        $color = 'info';
                    } elseif ($row->status == 1) {
                        $status = 'تایید شده';
                        $color = 'success';
                    } else {
                        $status = 'رد شده';
                        $color = 'danger';
                    }
                    return '<span class="badge-text badge-text-small ' . $color . '">' . $status . '</span>';
                })
                ->addColumn('action', function ($row) {
                    $confirm = $row->status != 1 ?
                        '<form action="' . route('admin.comments.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="1">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-thumbs-up"></i>تایید' .
                        '</button>' .
                        '</form>' : null;
                    $reject = $row->status != 0 ?
                        '<form action="' . route('admin.comments.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="0">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-thumbs-down"></i>رد کردن' .
                        '</button>' .
                        '</form>' : null;
                    return '<div class="widget-options">' .
                        '<div class="dropdown">' .
                        '<button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">' .
                        '<i class="la la-ellipsis-v"></i>' .
                        '</button>' .
                        '<div class="dropdown-menu px-0" x-placement="bottom-start">' .
                        $confirm .
                        $reject .
                        '<form action="' . route('admin.comments.destroy', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0" onsubmit="return confirm(\'آیا از حذف این کامنت اطمینان دارید؟\')">' .
                        csrf_field() .
                        method_field('delete') .
                        '<button type="submit"/>' .
                        '<i class="la la-trash-o"></i>حذف' .
                        '</button>' .
                        '</form>' .
                        '</div>' .
                        '</div>' .
                        '</div>';
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d');
                })
                ->rawColumns(['author', 'content', 'post', 'status', 'action', 'created_at'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function changeStatus(Request $request, Comment $comment): RedirectResponse
    {
        $request->validate([
            'status' => 'required|numeric'
        ]);
        try {
            $comment->update(['status' => $request->status]);
            if (empty($comment->parent_id)) {
                $post = $comment->commentable;
                $commentsWithScore = $post->comments()->whereNull('parent_id')->where('status', 1);
                $avgScore = (int)round($commentsWithScore->avg('score'));
                $votes = $commentsWithScore->count();
                $post->update([
                    'score' => $avgScore,
                    'votes' => $votes
                ]);
            }
        } catch (Exception $exception) {
            dd($exception);
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.']);
        }
        $phrase = $request->status == 0 ? 'رد' : 'تایید';
        return redirect()->route('admin.comments.index')->with([
            'success' => "کامنت مورد نظر با موفقیت {$phrase} شد."
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        try {
            $comment->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.comments.index')->with('success', 'دیدگاه مورد نظر با موفقیت حذف شد.');
    }
}
