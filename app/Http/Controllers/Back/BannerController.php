<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Repository\BannerRepository;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::first();
        return view('back.settings.banners', compact('banners'));
    }

    public function store(Request $request): RedirectResponse
    {
        $banners = Banner::firstOrFail();
        $banners_image = [];
        for ($i = 1; $i <= 5; $i++) {
            if ($request->has("remove_banner{$i}")) {
                if ($banners->value("banner{$i}_image") !== null &&
                    Storage::disk('ftp')->exists(env('BANNERS') . $banners->value("banner{$i}_image"))) {
                    Storage::disk('ftp')->delete(env('BANNERS') . $banners->value("banner{$i}_image"));
                }
                $banners_image[$i - 1] = null;
            } else {
                $banners_image[$i - 1] = resolve(BannerRepository::class)
                    ->storeImage($request->file("banner{$i}_file"), $banners->value("banner{$i}_image"), 'BANNERS');
            }
        }
        $banner1_user_avatar = resolve(BannerRepository::class)
            ->storeImage($request->file("banner1_user_avatar_file"), $banners->value('banner1_user_avatar'), 'AVATARS');
        $request->merge([
            'banner1_image' => $banners_image[0],
            'banner2_image' => $banners_image[1],
            'banner3_image' => $banners_image[2],
            'banner4_image' => $banners_image[3],
            'banner5_image' => $banners_image[4],
            'banner1_user_avatar' => $banner1_user_avatar
        ]);
        try {
            $banners->update($request->all());
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا بعدا دوباره امتحان کنید.']);
        }
        return back()->with(['success' => 'تغییرات با موفقیت ثبت گردید.']);
    }
}
