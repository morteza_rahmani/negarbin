<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function suggestionTags(Request $request): JsonResponse
    {
        if (!$request->has('term') || !isset($request->term)) {
            return response()->json([]);
        }
        $tags = Tag::where('title', 'LIKE', "%{$request->term}%")->limit('10')->select('id', 'title')->get()->toArray();
        return response()->json($tags);
    }
}
