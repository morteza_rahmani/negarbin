<?php

namespace App\Http\Controllers\Back;

use App\Classes\AddCategories;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tag;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Exception;
use HungCP\PhpSimpleHtmlDom\HtmlDomParser;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    public function index()
    {
        return view('back.products.index');
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::latest('updated_at')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('thumbnail', function ($row) {
                    return '<a href="#" title="نمایش">' .
                        '<img class="avatar d-block" alt="thumbnail"
                         src="' . env('STORAGE_ADDRESS') . '/' . env('PRODUCTS') . $row->thumbnail . '">' .
                        '</a>';
                })
                ->addColumn('product_name', function ($row) {
                    return '<a href="#" target="_blank">' . $row->name . '</a>';
                })
                ->addColumn('author', function ($row) {
                    return '<a href="#">' . $row->user->username . '</a>';
                })
                ->addColumn('price', function ($row) {
                    return number_format($row->price) . ' تومان';
                })
                ->addColumn('status', function ($row) {
                    return '<span class="badge-text badge-text-small bg-' . $row->statusColor() . '">' .
                        $row->statusText() .
                        '</span>';
                })
                ->addColumn('action', function ($row) {
                    $confirm = $row->status != 2 ?
                        '<form action="' . route('admin.products.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="2">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-thumbs-up"></i>تایید' .
                        '</button>' .
                        '</form>' : null;
                    $reject = $row->status != 0 ?
                        '<form action="' . route('admin.products.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="0">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-thumbs-down"></i>رد کردن' .
                        '</button>' .
                        '</form>' : null;
                    return '<div class="widget-options">' .
                        '<div class="dropdown">' .
                        '<button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">' .
                        '<i class="la la-ellipsis-v"></i>' .
                        '</button>' .
                        '<div class="dropdown-menu px-0" x-placement="bottom-start">' .
                        '<a href="' . route('admin.products.edit', $row->id) . '" ' .
                        'class="dropdown-item px-4">' .
                        '<i class="la la-edit"></i>ویرایش' .
                        '</a> ' .
                        $confirm .
                        $reject .
                        '<form action="' . route('admin.products.destroy', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0" ' .
                        'onsubmit="return confirm(\'آیا از حذف این محصول (' . $row->name . ') اطمینان دارید؟\')">' .
                        csrf_field() .
                        method_field('delete') .
                        '<button type="submit"/> ' .
                        '<i class="la la-trash-o"></i>حذف' .
                        '</button>' .
                        '</form>' .
                        '</div>' .
                        '</div>' .
                        '</div>';
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d');
                })
                ->rawColumns(['thumbnail', 'author', 'product_name', 'price', 'status', 'action', 'created_at'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function edit(Product $product)
    {
        $categories = Category::whereNull('parent_id')->get();
        $tags_title = [];
        foreach ($product->tags as $tag) {
            array_push($tags_title, $tag->title);
        }
        $tags = implode(',', $tags_title);
        return view('back.products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, Product $product): RedirectResponse
    {
        $this->validate($request, [
            'product_name' => ['required'],
            'thumbnail' => ['nullable', 'image', 'max:1024'],
            'file' => ['nullable', 'file', 'mimes:jpg,jpeg,png,zip,rar', 'max:100000'],
            'product_price' => ['required', 'numeric', 'min:0'],
            'format' => ['required'],
        ]);
        if ($request->hasFile('thumbnail')) {
            // Remove last thumbnail
            if (isset($product->thumbnail) && Storage::disk('ftp')->exists(env('PRODUCTS') . $product->thumbnail)) {
                Storage::disk('ftp')->delete(env('PRODUCTS') . $product->thumbnail);
            }
            // Save new thumbnail
            $thumbnailFile = $request->file('thumbnail');
            $uniqueID = uniqid();
            $thumbnailFileExtension = $thumbnailFile->getClientOriginalExtension();
            $userID = $product->user_id;
            $thumbnailFileName = "u{$userID}-t{$uniqueID}.{$thumbnailFileExtension}";
            Storage::disk('ftp')->put(env('PRODUCTS') . $thumbnailFileName, fopen($request->file('thumbnail'), 'r+'));
        }

        if ($request->hasFile('file')) {
            // Remove last file
            if (isset($product->file) && Storage::disk('ftp')->exists(env('FILES') . $product->file)) {
                Storage::disk('ftp')->delete(env('FILES') . $product->file);
            }
            // Save new file
            $file = $request->file('file');
            $uniqueID = uniqid();
            $fileExtension = $file->getClientOriginalExtension();
            $userID = $product->user_id;
            $fileName = "u{$userID}-p{$uniqueID}.{$fileExtension}";
            Storage::disk('ftp')->put(env('Files') . $fileName, fopen($request->file('file'), 'r+'));
        }

        $slug = isset($request->product_slug) ?
            SlugService::createSlug(Product::class, 'slug', $request->product_slug) :
            SlugService::createSlug(Product::class, 'slug', $request->product_name);

        try {
            $product->update([
                'name' => $request->product_name,
                'slug' => $slug,
                'thumbnail' => $thumbnailFileName ?? $product->thumbnail,
                'content' => $request->product_content,
                'summary' => $request->product_summary,
                'price' => (int)$request->product_price,
                'file' => $fileName ?? $product->file,
                'format' => $request->input('format'),
                'size' => isset($fileName) ? $request->file('file')->getSize() : $product->size,
            ]);
            // Sync Categories
            $cat_ids = (new AddCategories())->addCategories($request->categories);
            $product->categories()->sync($cat_ids);
            // Sync Tags
            $tags_ids = [];
            if (isset($request->tags)) {
                foreach (explode(',', $request->tags) as $tag_title) {
                    $tag = Tag::where('title', $tag_title)->first();
                    if (!isset($tag)) {
                        $tag = Tag::create(['title' => $tag_title]);
                    }
                    array_push($tags_ids, $tag->id);
                }
            }
            $product->tags()->sync($tags_ids);
        } catch (Exception $exception) {
            return redirect()->back()->with([
                'error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.'
            ]);
        }
        return redirect()->route('admin.products.index')->with([
            'success' => 'محصول مورد نظر با موفقیت ویرایش شد.'
        ]);
    }

    public function changeStatus(Request $request, Product $product): RedirectResponse
    {
        $request->validate([
            'status' => 'required|numeric'
        ]);
        try {
            $product->update(['status' => $request->status]);
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.']);
        }
        $phrase = $request->status == 0 ? 'رد' : 'تایید';
        return redirect()->route('admin.products.index')->with([
            'success' => "محصول مورد نظر با موفقیت {$phrase} شد."
        ]);
    }

    public function destroy(Product $product): RedirectResponse
    {
        // Delete all media content from downloading host
        if (isset($product->content)) {
            $dom = HtmlDomParser::str_get_html($product->content);
            $elements = $dom->find('img, source, audio');
            foreach ($elements as $e) {
                $media_src = $e->attr['src'];
                if (strpos($media_src, env('STORAGE_ADDRESS') !== false)) {
                    $media_path = str_replace(env('STORAGE_ADDRESS'), '', $media_src);
                    if (Storage::disk('ftp')->exists($media_path)) {
                        Storage::disk('ftp')->delete($media_path);
                    }
                }
            }
        }

        // Delete Thumbnail
        if (isset($product->thumbnail)) {
            if (Storage::disk('ftp')->exists(env('PRODUCTS') . $product->thumbnail)) {
                Storage::disk('ftp')->delete(env('PRODUCTS') . $product->thumbnail);
            }
            if (Storage::disk('ftp')->exists(env('PRODUCTS_SMALL') . $product->thumbnail)) {
                Storage::disk('ftp')->delete(env('PRODUCTS_SMALL') . $product->thumbnail);
            }
        }

        try {
            $product->comments()->delete();
            $product->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.products.index')->with('success', 'محصول مورد نظر با موفقیت حذف شد.');
    }
}
