<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Award;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class AwardController extends Controller
{
    public function index()
    {
        return view('back.awards.index');
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Award::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('icon', function ($row) {
                    return '<img alt="icon" width="50" height="50" src="' . env('STORAGE_ADDRESS') . env('AWARDS') . $row->icon . '">';
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d');
                })
                ->addColumn('action', function ($row) {
                    return '<div class="btn-group">
                                <button class="btn btn-primary btn-sm rounded-right td-actions">
                                    <a href="' . route('admin.awards.edit', $row->id) . '">
                                        <i class="la la-edit ml-0 p-1 text-white"></i>
                                    </a>
                                </button>
                                <form action="' . route('admin.awards.destroy', $row->id) . '" method="post"
                                    onsubmit="return confirm(\'آیا برای حذف این ریکورد مطمئن هستید؟\')">
                                    ' . csrf_field() . '
                                    ' . method_field('delete') . '
                                    <button class="btn btn-primary btn-sm rounded-left td-actions h-100">
                                        <i class="la la-remove p-1 ml-0 text-white"></i>
                                    </button>
                                </form>
                            </div>';
                })
                ->rawColumns(['icon', 'created_at', 'action'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function create()
    {
        return view('back.awards.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'icon' => ['required', 'image', 'max:512']
        ]);

        //upload new file
        $file = $request->file('icon');
        $fileType = $file->getClientOriginalExtension();
        $uniqueID = uniqid();
        $filename = "i{$uniqueID}.{$fileType}";
        Storage::disk('ftp')->put(env('AWARDS') . $filename, fopen($file, 'r+'));

        try {
            Award::create([
                'name' => $request->name,
                'icon' => $filename,
                'description' => $request->description
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی رخ داده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->route('admin.awards.index')->with(['success' => 'جایزه جدید با موفقیت ذخیره شد.']);
    }

    public function edit(Award $award)
    {
        return view('back.awards.edit', compact('award'));
    }

    public function update(Request $request, Award $award): RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'icon' => ['nullable', 'image', 'max:512']
        ]);

        if ($request->hasFile('icon')) {
            // Remove old file
            if (isset($award->icon) && Storage::disk('ftp')->exists(env('AWARDS') . $award->icon)) {
                Storage::disk('ftp')->delete(env('AWARDS') . $award->icon);
            }
            // Upload new file
            $file = $request->file('icon');
            $fileType = $file->getClientOriginalExtension();
            $uniqueID = uniqid();
            $filename = "i{$uniqueID}.{$fileType}";
            Storage::disk('ftp')->put(env('AWARDS') . $filename, fopen($file, 'r+'));
        }
        try {
            $award->update([
                'name' => $request->name,
                'icon' => $filename ?? $award->icon,
                'description' => $request->description
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی رخ داده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->route('admin.awards.index')->with([
            'success' => 'جایزه مورد نظر با موفقیت ویرایش شد.'
        ]);
    }

    public function destroy(Award $award): RedirectResponse
    {
        try {
            $award->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.awards.index')->with('success', 'آیتم مورد نظر با موفقیت حذف شد.');
    }
}
