<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('back.tickets.index');
    }

    /**
     * Return datatable of the resource.
     *
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Ticket::whereNull('parent_id')->orderByDesc('updated_at')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('ID', function ($row) {
                    return "<span class='text-primary'>#{$row->id}</span>";
                })
                ->addColumn('author', function ($row) {
                    $thumbnail = $row->user->adjusted_avatar;
                    $name = $row->user->full_name;
                    $email = $row->user->email ?? $row->user->phone;
                    return '<div class="d-flex">
                                <div>
                                    <a href="#" title="نمایش">
                                        <img class="avatar d-block rounded-circle" alt="avatar" src="' . $thumbnail . '"
                                             width="50" height="50" style="object-fit: cover">
                                    </a>
                                </div>
                                <div class="d-flex flex-column mt-2" style="margin-right:12px;">
                                    <a href="#"><strong>' . $name . '</strong></a>
                                    <small>' . $email . '</small>
                                </div>
                            </div>';
                })
                ->addColumn('title', function ($row) {
                    $url = route('admin.tickets.show', $row->id);
                    return "<a href='{$url}' target='_blank'>{$row->title}</a>";
                })
                ->addColumn('updated_at', function ($row) {
                    return '<div class="text-primary">' .
                        jdate($row->updated_at)->format('Y/m/d - H:i')
                        . '</div>';
                })
                ->addColumn('status', function ($row) {
                    return '<span class="text-' . $row->statusColor() . '">' . $row->status . '</span>';
                })
                ->rawColumns(['ID', 'author', 'title', 'updated_at', 'status'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function create()
    {
        return view('back.tickets.create');
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'min:5', 'string'],
            'to' => ['required', 'numeric', 'exists:users,id'],
            'message' => ['required', 'min:12'],
        ]);

        try {
            Ticket::create([
                'user_id' => Auth::id(),
                'title' => $request->title,
                'message' => $request->message,
                'status' => 'در انتظار پاسخ',
                'is_response' => 1,
                'to_user_id' => $request->to
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی پیش آمده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->route('admin.tickets.index')->with(['success' => 'تیکت شما با موفقیت ثبت گردید.']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Ticket $ticket)
    {
        return view('back.tickets.show', compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @throws ValidationException
     */
    public function update(Request $request, Ticket $ticket): RedirectResponse
    {
        $this->validate($request, [
            'message' => ['required', 'min:12']
        ]);

        if ($ticket->status == 'بسته') {
            return back()->with(['warning' => 'این تیکت بسته شده است.']);
        }

        try {
            Ticket::create([
                'user_id' => Auth::id(),
                'parent_id' => $ticket->id,
                'title' => $ticket->title,
                'message' => $request->message,
                'is_response' => 1
            ]);
            $ticket->update(['status' => 'پاسخ پشتیبان']);
            $ticket->touch();
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی پیش آمده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->back()->with(['success' => 'تیکت شما با موفقیت ثبت گردید.']);
    }
}
