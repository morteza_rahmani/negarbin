<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TransactionController extends Controller
{
    public function index()
    {
        return view('back.transactions.index');
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Payment::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('user', function ($row) {
                    $thumbnail = $row->invoice->user->adjusted_avatar;
                    $name = $row->invoice->user->full_name;
                    $email = $row->invoice->user->phone;
                    return '<div class="d-flex">
                                <div>
                                    <a href="' . route('author.show', $row->invoice->user->username) . '" title="نمایش">
                                        <img class="avatar d-block" alt="avatar" src="' . $thumbnail . '">
                                    </a>
                                </div>
                                <div class="d-flex flex-column" style="margin-right:12px;">
                                    <a href="' . route('author.show', $row->invoice->user->username) . '"><strong>' . $name . '</strong></a>
                                    <small>' . $email . '</small>
                                </div>
                            </div>';
                })
                ->addColumn('user_name', function ($row) {
                    return $row->invoice->user->full_name;
                })
                ->addColumn('amount', function ($row) {
                    return number_format($row->invoice->amount);
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d - H:i');
                })
                ->addColumn('status_styled', function ($row) {
                    switch ($row->status) {
                        case Payment::STATUS_NEW:
                            $text = 'پرداخت نشده';
                            $color = 'dark';
                            break;
                        case Payment::STATUS_SUCCEED:
                            $text = 'موفق';
                            $color = 'success';
                            break;
                        case Payment::STATUS_FAILED:
                            $text = 'ناموفق';
                            $color = 'danger';
                            break;
                        default:
                            $text = 'نامشخص';
                            $color = 'light';
                    }
                    return '<span class="badge-text badge-text-small bg-' . $color . '">' . $text . '</span>';
                })
                ->addColumn('action', function ($row) {
                    return '<a href="' . route('admin.transactions.show', $row->id) . '">' .
                        '<span class="btn btn-gradient-03 p-3">مشاهده جزئیات</span></a>';
                })
                ->rawColumns(['user', 'user_name', 'amount', 'status_styled', 'action', 'created_at'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function show(Payment $payment)
    {
        return view('back.transactions.show', compact('payment'));
    }

}
