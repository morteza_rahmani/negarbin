<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Exception;
use HungCP\PhpSimpleHtmlDom\HtmlDomParser;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('back.articles.index');
    }

    /**
     * Return datatable of the resource.
     *
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Article::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('thumbnail', function ($row) {
                    $image_url = $row->thumbnail ?? asset('assets/front/images/no-photo.jpg');
                    return '<div style="width: 60px; height: 60px;">
                                <img src="' . $image_url . '" style="width: 100%; height: 100%;object-fit: cover;"
                                alt="' . $row->thumbnail_alt . '" loading="lazy">
                            </div>';
                })
                ->addColumn('author', function ($row) {
                    return isset($row->user_id) ? $row->user->full_name : '-';
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d');
                })
                ->addColumn('tags', function ($row) {
                    $result = '';
                    $tags = explode(',', $row->tags);
                    foreach ($tags as $tag) {
                        if ($tag)
                            $result .= '<span class="badge-text badge-text-small mr-1 mb-1 text-muted">' . $tag . '</span>';
                    }
                    return empty($result) ? '-' : $result;
                })
                ->addColumn('keywords', function ($row) {
                    $result = '';
                    $keywords = explode(',', $row->keywords);
                    foreach ($keywords as $keyword) {
                        if ($keyword)
                            $result .= '<span class="badge-text badge-text-small mr-1 mb-1 bg-secondary">' . $keyword . '</span>';
                    }
                    return empty($result) ? '-' : $result;
                })
                ->addColumn('action', function ($row) {
                    return '<div class="btn-group">
                                <button class="btn btn-primary btn-sm rounded-right td-actions">
                                    <a href="' . route('admin.articles.edit', $row->id) . '">
                                        <i class="la la-edit ml-0 p-1 text-white"></i>
                                    </a>
                                </button>
                                <form action="' . route('admin.articles.destroy', $row->id) . '" method="post"
                                    onsubmit="return confirm(\'آیا برای حذف این ریکورد مطمئن هستید؟\')">
                                    ' . csrf_field() . '
                                    ' . method_field('delete') . '
                                    <button class="btn btn-primary btn-sm rounded-left td-actions h-100">
                                        <i class="la la-remove p-1 ml-0 text-white"></i>
                                    </button>
                                </form>
                            </div>';
                })
                ->rawColumns(['thumbnail', 'action', 'author', 'tags', 'keywords', 'created_at'])
                ->make(true);
        }
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'unique:articles'],
            'name' => ['required', 'unique:articles'],
            'slug' => ['unique:articles'],
            'content' => ['required'],
            'keywords' => ['required'],
        ]);

        $slug = empty($request->input('slug')) ?
            SlugService::createSlug(Article::class, 'slug', $request->input('name')) :
            SlugService::createSlug(Article::class, 'slug', $request->input('slug'));
        $summary = empty($request->input('summary')) ?
            mb_substr(strip_tags($request->input('content')), 0, 160) : $request->input('summary');

        $request->merge([
            'slug' => $slug,
            'content' => $request->input('content'),
            'summary' => $summary,
            'user_id' => Auth::id()
        ]);

        try {
            Article::create($request->all());
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.articles.index')->with('success', 'مطلب جدید با موفقیت ذخیره شد.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        $article = Article::findOrFail($id);
        return view('back.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Article $article
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, Article $article): RedirectResponse
    {
        $this->validate($request, [
            'title' => ['required', 'unique:articles,title,' . $article->id . ''],
            'name' => ['required', 'unique:articles,name,' . $article->id . ''],
            'slug' => ['unique:articles,slug,' . $article->id . ''],
            'content' => ['required'],
            'keywords' => ['required']
        ]);

        if (empty($request->input('slug'))) {
            $slug = SlugService::createSlug(Article::class, 'slug', $request->input('name'));
        } elseif ($request->input('slug') == $article->slug) {
            $slug = $request->input('slug');
        } else {
            $slug = SlugService::createSlug(Article::class, 'slug', $request->input('slug'));
        }

        $summary = empty($request->input('summary')) ?
            mb_substr(strip_tags($request->input('content')), 0, 160) : $request->input('summary');

        $request->merge([
            'slug' => $slug,
            'summary' => $summary,
        ]);

        try {
            $article->update($request->all());
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.articles.index')->with('success', 'مطلب مورد نظر با موفقیت ویرایش شد.');
    }

    /**
     * Remove the specified resource from storage.
     * @param Article $article
     * @return RedirectResponse
     */
    public function destroy(Article $article): RedirectResponse
    {
        // Delete all media content from downloading host
        if (isset($article->content)) {
            $dom = HtmlDomParser::str_get_html($article->content);
            $elements = $dom->find('img, source, audio');
            foreach ($elements as $e) {
                $media_src = $e->attr['src'];
                if (strpos($media_src, env('STORAGE_ADDRESS') !== false)) {
                    $media_path = str_replace(env('STORAGE_ADDRESS'), '', $media_src);
                    if (Storage::disk('ftp')->exists($media_path)) {
                        Storage::disk('ftp')->delete($media_path);
                    }
                }
            }
        }

        // Delete Thumbnail
        $thumb_path = str_replace(env('STORAGE_ADDRESS'), '', $article->thumbnail);
        if (isset($article->thumbnail) && Storage::disk('ftp')->exists($thumb_path)) {
            Storage::disk('ftp')->delete($thumb_path);
        }
        try {
            $article->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.articles.index')->with('success', 'مطلب مورد نظر با موفقیت حذف شد.');
    }
}
