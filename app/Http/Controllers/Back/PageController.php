<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PageController extends Controller
{
    public function index()
    {
        return view('back.pages.index');
    }

    /**
     * Return datatable of the resource.
     *
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Page::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d');
                })
                ->addColumn('keywords', function ($row) {
                    $result = '';
                    $keywords = explode(',', $row->keywords);
                    foreach ($keywords as $keyword) {
                        if ($keyword)
                            $result .= '<span class="badge-text badge-text-small mr-1 mb-1 bg-secondary">' . $keyword . '</span>';
                    }
                    return empty($result) ? '-' : $result;
                })
                ->addColumn('action', function ($row) {
                    return '<div class="btn-group">
                                <button class="btn btn-primary btn-sm rounded-right td-actions">
                                    <a href="' . route('admin.pages.edit', $row->id) . '">
                                        <i class="la la-edit ml-0 p-1 text-white"></i>
                                    </a>
                                </button>
                                <button class="btn btn-primary btn-sm rounded-0 td-actions">
                                    <a href="' . route('page.show', $row->slug) . '" target="_blank">
                                        <i class="la la-eye ml-0 p-1 text-white"></i>
                                    </a>
                                </button>
                                <form action="' . route('admin.pages.destroy', $row->id) . '" method="post"
                                    onsubmit="return confirm(\'آیا برای حذف این ریکورد مطمئن هستید؟\')">
                                    ' . csrf_field() . '
                                    ' . method_field('delete') . '
                                    <button class="btn btn-primary btn-sm rounded-left td-actions h-100">
                                        <i class="la la-remove p-1 ml-0 text-white"></i>
                                    </button>
                                </form>
                            </div>';
                })
                ->rawColumns(['keywords', 'created_at', 'action'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function create()
    {
        return view('back.pages.create');
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'unique:pages'],
            'name' => ['required', 'unique:pages'],
            'slug' => ['unique:pages'],
            'content' => ['required'],
        ]);

        $slug = empty($request->input('slug')) ?
            SlugService::createSlug(Page::class, 'slug', $request->input('name')) :
            SlugService::createSlug(Page::class, 'slug', $request->input('slug'));

        $summary = empty($request->input('summary')) ?
            mb_substr(strip_tags($request->input('content')), 0, 160) : $request->input('summary');

        $request->merge([
            'slug' => $slug,
            'summary' => $summary,
        ]);

        try {
            Page::create($request->all());
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.pages.index')->with('success', 'برگه جدید با موفقیت ذخیره شد.');
    }

    public function edit(Page $page)
    {
        return view('back.pages.edit', compact('page'));
    }

    public function update(Request $request, Page $page): RedirectResponse
    {
        $request->validate([
            'title' => ['required', 'unique:pages,title,' . $page->id . ''],
            'name' => ['required', 'unique:pages,name,' . $page->id . ''],
            'slug' => ['unique:pages,slug,' . $page->id . ''],
            'content' => ['required'],
        ]);

        if (empty($request->input('slug'))) {
            $slug = SlugService::createSlug(Page::class, 'slug', $request->input('name'));
        } elseif ($request->input('slug') == $page->slug) {
            $slug = $request->input('slug');
        } else {
            $slug = SlugService::createSlug(Page::class, 'slug', $request->input('slug'));
        }

        $summary = empty($request->input('summary')) ?
            mb_substr(strip_tags($request->input('content')), 0, 160) : $request->input('summary');

        $request->merge([
            'slug' => $slug,
            'summary' => $summary,
        ]);

        try {
            $page->update($request->all());
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.pages.index')->with('success', 'برگه مورد نظر با موفقیت ویرایش شد.');
    }

    public function destroy(Page $page): RedirectResponse
    {
        try {
            $page->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است. لطفا اندکی بعد دوباره امتحان کنید.');
        }
        return redirect()->route('admin.pages.index')->with('success', 'برگه مورد نظر با موفقیت حذف شد.');
    }
}
