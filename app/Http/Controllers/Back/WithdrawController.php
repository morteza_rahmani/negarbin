<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Withdraw;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('back.withdraws.index');
    }

    public function datatable(Request $request)
    {
        if ($request->ajax()) {
            $data = Withdraw::latest('updated_at')->get();
            return Datatables::of($data)
                ->addColumn('username', function ($row) {
                    return $row->user->username;
                })
                ->addColumn('full_name', function ($row) {
                    return $row->user->full_name;
                })
                ->addColumn('created_at', function ($row) {
                    return jdate($row->created_at)->format('Y/m/d - H:i');
                })
                ->addColumn('status', function ($row) {
                    return '<span class="badge badge-pill h5 m-0 px-3 py-2 bg-' . $row->colorStatus() . '">' .
                        $row->translateStatus() . '</span>';
                })
                ->addColumn('action', function ($row) {
                    $confirm = $row->status != Withdraw::STATUS_SUCCEED ?
                        '<form action="' . route('admin.withdraws.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="' . Withdraw::STATUS_SUCCEED . '">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-check"></i>واریز شد' .
                        '</button>' .
                        '</form>' : null;
                    $reject = $row->status != Withdraw::STATUS_FAILED ?
                        '<form action="' . route('admin.withdraws.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="' . Withdraw::STATUS_FAILED . '">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-close"></i>واریز نشد' .
                        '</button>' .
                        '</form>' : null;
                    $under = $row->status != Withdraw::STATUS_UNDER ?
                        '<form action="' . route('admin.withdraws.changeStatus', $row->id) . '" ' .
                        'method="post" class="dropdown-item p-0">' .
                        csrf_field() .
                        '<input type="hidden" name="status" value="' . Withdraw::STATUS_UNDER . '">' .
                        '<button type="submit"/> ' .
                        '<i class="la la-hand-stop-o"></i>در دست بررسی' .
                        '</button>' .
                        '</form>' : null;
                    return '<div class="widget-options">' .
                        '<div class="dropdown">' .
                        '<button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">' .
                        '<i class="la la-ellipsis-v"></i>' .
                        '</button>' .
                        '<div class="dropdown-menu px-0" x-placement="bottom-start">' .
                        $confirm .
                        $reject .
                        $under .
                        '</div>' .
                        '</div>' .
                        '</div>';
                })
                ->rawColumns(['username', 'full_name', 'created_at', 'status', 'action'])
                ->make(true);
        }
        return redirect()->back();
    }

    public function changeStatus(Request $request, Withdraw $withdraw): RedirectResponse
    {
        $request->validate([
            'status' => 'required|numeric'
        ]);
        try {
            if ($request->status == Withdraw::STATUS_SUCCEED) {
                $withdraw->user()->decrement('outstanding', $withdraw->amount);
            }
            if ($request->status != Withdraw::STATUS_SUCCEED && $withdraw->status == Withdraw::STATUS_SUCCEED) {
                $withdraw->user()->increment('outstanding', $withdraw->amount);
            }
            $withdraw->update(['status' => $request->status]);
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.']);
        }
        return redirect()->route('admin.withdraws.index')
            ->with(['success' => "وضعیت تسویه حساب مورد نظر با موفقیت تغییر یافت."]);
    }

}
