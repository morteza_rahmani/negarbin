<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        $page_title = 'سفارشات';
        $invoices = Auth::user()->invoices()->latest()->paginate(15);
        return view('front.dashboard.orders.index', compact('page_title', 'invoices'));
    }

    public function show(Payment $payment)
    {
        $page_title = 'مشاهده سفارش';
        return view('front.dashboard.orders.show', compact('page_title', 'payment'));
    }
}
