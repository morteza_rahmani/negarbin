<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class TicketController extends Controller
{
    public function index()
    {
        $page_title = 'پشتیبانی';
        $tickets = User::find(Auth::id())->tickets()->whereNull('parent_id')->orderByDesc('updated_at')->paginate(10);
        return view('front.dashboard.tickets.index', compact('page_title', 'tickets'));
    }

    public function create()
    {
        $page_title = 'تیکت جدید';
        return view('front.dashboard.tickets.create', compact('page_title'));
    }

    /**
     * Store ticket
     *
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'title' => ['required', 'min:5', 'string'],
            'message' => ['required', 'min:12']
        ]);

        try {
            Ticket::create([
                'user_id' => Auth::id(),
                'title' => $request->title,
                'message' => $request->message,
                'status' => 'در انتظار پاسخ'
            ]);
        } catch (Exception $exception) {
            return redirect()->route('dashboard.support.index')
                ->with(['error' => 'خطایی پیش آمده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->route('dashboard.support.index')
            ->with(['success' => 'پاسخ شما به این تیکت با موفقیت ثبت گردید.']);
    }

    /**
     * Store new response to an existing ticket
     *
     * @throws ValidationException
     */
    public function update(Request $request, Ticket $ticket): RedirectResponse
    {
        $this->validate($request, [
            'message' => ['required', 'min:12']
        ]);
        if ($ticket->user_id != Auth::id()) {
            return redirect()->to(url()->previous() . '#respond')
                ->with(['error' => 'شما دسترسی لازم را ندارید!']);
        }
        try {
            Ticket::create([
                'user_id' => Auth::id(),
                'parent_id' => $ticket->id,
                'title' => $ticket->title,
                'message' => $request->message,
            ]);
            $ticket->update(['status' => 'پاسخ کاربر']);
            $ticket->touch();
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی پیش آمده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->back()->with(['success' => 'تیکت شما با موفقیت ثبت گردید.']);

    }

    public function show(Ticket $ticket)
    {
        $page_title = "تیکت شماره {$ticket->id}";
        if ($ticket->user->id == Auth::id() && empty($ticket->parent_id)) {
            return view('front.dashboard.tickets.show', compact('page_title', 'ticket'));
        }
        return redirect()->route('dashboard.support.index');
    }

    public function close(Ticket $ticket)
    {
        try {
            $ticket->update([
                'status' => 'بسته'
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی پیش آمده است! لطفا دوباره امتحان کنید.']);
        }
        return redirect()->back()->with(['success' => 'تیکت مورد نظر با موفقیت بسته شد.']);
    }
}
