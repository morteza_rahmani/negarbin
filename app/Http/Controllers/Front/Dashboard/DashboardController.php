<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\DashboardBanner;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;

class DashboardController extends Controller
{
    public function index()
    {
        $page_title = 'داشبورد';
        $banners = DashboardBanner::first() ?? null;
        $tickets = Auth::user()->tickets()->latest()->whereNull('parent_id')->limit(5)->get();
        $chartData = $this->chart()->getData();
        $pDates = $chartData->pDates;
        $pCount = json_encode($chartData->pCount);
        $pAmount = json_encode($chartData->pAmount);
        return view('front.dashboard.index', compact(
            ['page_title', 'banners', 'tickets', 'pDates', 'pCount', 'pAmount']
        ));
    }

    public function chart()
    {
        $md = jdate()->subYears()->format('Y-m-d');
        $payments = Payment::where('status', Payment::STATUS_SUCCEED)->whereDate('jdate', '>=', $md)
            ->latest()->select('id', 'invoice_id', 'jdate');

        $pDates = [];
        $pCount = [];
        $pAmount = [];

        for ($i = 0; $i < 12; $i++) {
            if ($i == 0) {
                $end = jdate()->format('Y-m-d');
                $start = jdate()->subDays(jdate()->getDay() - 1)->format('Y-m-d');
            } else {
                $end = Jalalian::fromFormat('Y-m-d', $start)->subDays()->format('Y-m-d');
                $start = Jalalian::fromFormat('Y-m-d', $start)->subMonths()->format('Y-m-d');
            }

            array_push($pDates, Jalalian::fromFormat('Y-m-d', $start)->format('%B'));
            $pim = $payments->whereBetween('jdate', [$start, $end]);
            $aim = 0; // Amount (price) given in this month.
            $salesNum = 0; // Sales number.
            foreach ($pim->get() as $p) {
                foreach ($p->invoice->products as $product) {
                    if ($product->user_id == Auth::id()) {
                        $aim += $product->pivot->product_price;
                        $salesNum++;
                    }
                }
            }
            array_push($pAmount, $aim);
            array_push($pCount, $salesNum);
        }

        return response()->json([
            'pDates' => array_reverse($pDates),
            'pCount' => array_reverse($pCount),
            'pAmount' => array_reverse($pAmount)
        ]);
    }
}
