<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Classes\AddCategories;
use App\Http\Controllers\Controller;
use App\Jobs\UploadFile;
use App\Models\Category;
use App\Models\ContentImage;
use App\Models\Product;
use App\Models\Tag;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Exception;
use HungCP\PhpSimpleHtmlDom\HtmlDomParser;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $page_title = 'محصولات من';
        $products = Auth::user()->products()->orderByDesc('updated_at')->paginate(15);
        return view('front.dashboard.products.index', compact('page_title', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $page_title = 'محصول جدید';
        $categories = Category::whereNull('parent_id')->get();
        return view('front.dashboard.products.create', compact('page_title', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'product_name' => ['required'],
            'thumbnail' => ['required', 'file', 'mimes:jpg,jpeg', 'max:1024'],
            'file' => ['required', 'file', 'mimes:jpg,jpeg,png,zip,rar'],
            'product_price' => ['nullable', 'numeric', 'min:0'],
            'format' => ['required'],
        ], [
            'thumbnail.mimes' => 'فرمت تصویر شاخص باید jpg/jpeg باشد.'
        ]);
        // Save Thumbnail
        $thumbnailFile = $request->file('thumbnail');
        $resizedThumbnail = Image::make($thumbnailFile)->resize(835, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');
        $smallThumbnail = Image::make($thumbnailFile)->fit(100, 100)->encode('jpg', 80);
        $uniqueID = uniqid();
        $userID = Auth::id();
        $thumbnailFileName = "u{$userID}-t{$uniqueID}.jpg";
        Storage::disk('ftp')->put(env('PRODUCTS') . $thumbnailFileName, $resizedThumbnail);
        Storage::disk('ftp')->put(env('PRODUCTS_SMALL') . $thumbnailFileName, $smallThumbnail);
        // Save File
        $file = $request->file('file');
        $uniqueID = uniqid();
        $fileExtension = $file->getClientOriginalExtension();
        $userID = Auth::id();
        $fileName = "u{$userID}-p{$uniqueID}.{$fileExtension}";
        Storage::disk('ftp')->put(env('FILES') . $fileName, fopen($file, 'r+'));
        // Create Slug
        $slug = SlugService::createSlug(Product::class, 'slug', $request->product_name);

        if ($request->has('product_content')) {
            $dom = HtmlDomParser::str_get_html($request->product_content);
            $elements = $dom->find('img, source, audio');
            foreach ($elements as $e) {
                $media_src = $e->attr['src'];
                if (strpos($media_src, env('STORAGE_ADDRESS')) !== false) {
                    ContentImage::where('url', $media_src)->update(["used" => true]);
                }
            }
        }

        try {
            $product = Product::create([
                'user_id' => Auth::id(),
                'name' => $request->product_name,
                'slug' => $slug,
                'thumbnail' => $thumbnailFileName,
                'content' => $request->product_content,
                'summary' => $request->product_summary,
                'price' => isset($request->product_price) ? (int)$request->product_price : 0,
                'file' => $fileName,
                'format' => $request->input('format'),
                'size' => $request->file('file')->getSize(),
            ]);
            // Attach Categories
            if ($request->has('categories')) {
                $cat_ids = (new AddCategories())->addCategories($request->categories);
                $product->categories()->attach($cat_ids);
            }
            // Attach Tags
            if (isset($request->tags)) {
                $tags_ids = [];
                foreach (explode(',', $request->tags) as $tag_title) {
                    $tag = Tag::where('title', $tag_title)->first();
                    if (!isset($tag)) {
                        $tag = Tag::create(['title' => $tag_title]);
                    }
                    array_push($tags_ids, $tag->id);
                }
                $product->tags()->attach($tags_ids);
            }
        } catch (Exception $exception) {
            return redirect()->back()->with([
                'error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.'
            ]);
        }
        return redirect()->route('dashboard.products.index')->with([
            'success' => 'محصول شما با موفقیت ثبت شد. پس از تایید در سایت قرار خواهد گرفت.'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        $page_title = 'ویرایش محصول';
        $categories = Category::whereNull('parent_id')->get();
        $tags = $product->tags()->pluck('title')->toArray();
        $tags = implode(',', $tags);
        return view('front.dashboard.products.edit', compact(['page_title', 'product', 'categories', 'tags']));
    }

    /**
     * Update the specified resource in storage.
     * @throws ValidationException
     */
    public function update(Request $request, Product $product): RedirectResponse
    {
        $this->validate($request, [
            'product_name' => ['required'],
            'thumbnail' => ['nullable', 'file', 'mimes:jpg,jpeg', 'max:1024'],
            'file' => ['nullable', 'file', 'mimes:jpg,jpeg,png,zip,rar'],
            'product_price' => ['nullable', 'numeric', 'min:0'],
            'format' => ['required'],
        ], [
            'thumbnail.mimes' => 'فرمت تصویر شاخص باید jpg/jpeg باشد.'
        ]);
        // Save Thumbnail
        if ($request->hasFile('thumbnail')) {
            // Remove last thumbnail
            if (isset($product->thumbnail) && Storage::disk('ftp')->exists(env('PRODUCTS') . $product->thumbnail)) {
                Storage::disk('ftp')->delete(env('PRODUCTS') . $product->thumbnail);
            }
            // Save new thumbnail
            $thumbnailFile = $request->file('thumbnail');
            $resizedThumbnail = Image::make($thumbnailFile)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 60);
            $uniqueID = uniqid();
            $userID = Auth::id();
            $thumbnailFileName = "u{$userID}-t{$uniqueID}.{$thumbnailFileExtension}";
            Storage::disk('ftp')->put(env('PRODUCTS') . $thumbnailFileName, fopen($resizedThumbnail->__toString(), 'r+'));
        }
        if ($request->hasFile('file')) {
            // Remove last file
            if (isset($product->file) && Storage::disk('ftp')->exists(env('FILES') . $product->file)) {
                Storage::disk('ftp')->delete(env('FILES') . $product->file);
            }
            // Save new file
            $file = $request->file('file');
            $uniqueID = uniqid();
            $fileExtension = $file->getClientOriginalExtension();
            $userID = Auth::id();
            $fileName = "u{$userID}-p{$uniqueID}.{$fileExtension}";
            Storage::disk('ftp')->put(env('FILES') . $fileName, fopen($file, 'r+'));
        }
        // Create Slug
        if ($product->name != $request->product_name)
            $slug = SlugService::createSlug(Product::class, 'slug', $request->product_name);

        if ($request->has('product_content')) {
            $dom = HtmlDomParser::str_get_html($request->product_content);
            $elements = $dom->find('img, source, audio');
            foreach ($elements as $e) {
                $media_src = $e->attr['src'];
                if (strpos($media_src, env('STORAGE_ADDRESS')) !== false) {
                    ContentImage::where('url', $media_src)->update(["used" => true]);
                }
            }
        }

        try {
            $product->update([
                'user_id' => Auth::id(),
                'name' => $request->product_name,
                'slug' => $slug ?? $product->slug,
                'thumbnail' => $thumbnailFileName ?? $product->thumbnail,
                'content' => $request->product_content,
                'summary' => $request->product_summary,
                'price' => isset($request->product_price) ? (int)$request->product_price : 0,
                'file' => $fileName ?? $product->file,
                'format' => $request->input('format'),
                'size' => isset($thumbnailFileName) ? $request->file('file')->getSize() : $product->size,
                'status' => 3
            ]);
            // Sync Categories
            if ($request->has('categories')) {
                $cat_ids = (new AddCategories())->addCategories($request->categories);
                $product->categories()->sync($cat_ids);
            } else {
                $product->categories()->sync([]);
            }
            // Sync Tags
            $tags_ids = [];
            if (isset($request->tags)) {
                foreach (explode(',', $request->tags) as $tag_title) {
                    $tag = Tag::where('title', $tag_title)->first();
                    if (!isset($tag)) {
                        $tag = Tag::create(['title' => $tag_title]);
                    }
                    array_push($tags_ids, $tag->id);
                }
            }
            $product->tags()->sync($tags_ids);
        } catch (Exception $exception) {
            return redirect()->back()->with([
                'error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.'
            ]);
        }
        return redirect()->route('dashboard.products.index')->with([
            'success' => 'محصول شما با موفقیت ویرایش شد. پس از تایید در سایت قرار خواهد گرفت.'
        ]);
    }

    /**
     * Show the button for destroying the specified resource.
     */
    public function remove(Product $product)
    {
        $page_title = 'حذف محصول';
        return view('front.dashboard.products.remove', compact('page_title', 'product'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product): RedirectResponse
    {
        // Delete all media content from downloading host
        if (isset($product->content)) {
            $dom = HtmlDomParser::str_get_html($product->content);
            $elements = $dom->find('img, source, audio');
            foreach ($elements as $e) {
                $media_src = $e->attr['src'];
                if (strpos($media_src, env('STORAGE_ADDRESS') !== false)) {
                    $media_path = str_replace(env('STORAGE_ADDRESS'), '', $media_src);
                    if (Storage::disk('ftp')->exists($media_path)) {
                        Storage::disk('ftp')->delete($media_path);
                    }
                }
            }
        }

        // Delete Thumbnail
        if (isset($product->thumbnail)) {
            if (Storage::disk('ftp')->exists(env('PRODUCTS') . $product->thumbnail)) {
                Storage::disk('ftp')->delete(env('PRODUCTS') . $product->thumbnail);
            }
            if (Storage::disk('ftp')->exists(env('PRODUCTS_SMALL') . $product->thumbnail)) {
                Storage::disk('ftp')->delete(env('PRODUCTS_SMALL') . $product->thumbnail);
            }
        }

        try {
            $product->comments()->delete();
            $product->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with([
                'error' => 'مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.'
            ]);
        }
        return redirect()->route('dashboard.products.index')->with([
            'success' => 'محصول مورد نظر با موفقیت حذف گردید.'
        ]);
    }
}
