<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Jobs\VerifyEmail;
use App\Models\User;
use App\Models\UserVerify;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function index()
    {
        $page_title = 'پروفایل';
        return view('front.dashboard.profile', compact('page_title'));
    }

    /**
     * Store and update user profile information
     *
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $user = User::findOrFail(auth()->id());
        $this->validate($request, [
            'first_name' => 'required',
            'profile_avatar' => ['nullable', 'image', 'mimes:jpg,png,jpeg', 'dimensions:max_width=120,max_height=120'],
            'email' => ['required', 'email', 'unique:users,email,' . $user->id],
            'username' => ['required', 'regex:/^[a-zA-Z0-9\-]+$/', 'unique:users,username,' . $user->id],
            'IBAN' => ['nullable', 'regex:/^(?=.{24}$)[0-9]*$/', 'unique:users,IBAN,' . $user->id],
        ], [
            'IBAN.regex' => 'شماره شبا وارد شده نادرست است.',
            'profile_avatar.dimensions' => 'طول و عرض تصویر شما باید 120px یا کمتر باشد.'
        ]);
        if ($request->hasFile('profile_avatar')) {
            //code for remove old file
            if (isset($user->avatar) && Storage::disk('ftp')->exists(env('AVATARS') . $user->avatar)) {
                Storage::disk('ftp')->delete(env('AVATARS') . $user->avatar);
            }
            //upload new file
            $file = $request->file('profile_avatar');
            $resizedFile = Image::make($file)->encode('jpg');
            $uniqueID = uniqid();
            $filename = "u{$user->id}-a{$uniqueID}.jpg";
            Storage::disk('ftp')->put(env('AVATARS') . $filename, $resizedFile);
            //for update in table
            $request->request->add([
                'avatar' => $filename
            ]);
        }
        // Send verification email.
        if (!$user->is_email_verified || $request->email != $user->email) {
            $token = Str::random(64);
            UserVerify::where('user_id', $user->id)->delete();
            UserVerify::create([
                'user_id' => $user->id,
                'token' => $token
            ]);
            $appName = env('APP_NAME');
            Mail::send('mail.email-verification', [
                'token' => $token
            ], function ($message) use ($appName, $request) {
                $message->to($request->email);
                $message->subject("تایید ایمیل | $appName");
            });
            $user->update([
                'email_verified_at' => null,
                'is_email_verified' => 0
            ]);
        }
        try {
            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'avatar' => $request->avatar,
                'email' => $request->email,
                'username' => $request->username,
                'IBAN' => 'IR' . $request->IBAN,
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with(['error' => 'خطایی رخ داده است! لطفا دوباره امتحان کنید.']);
        }
        if (isset($token)) {
            return redirect()->back()->with([
                'success' => 'اطلاعات پروفایل شما با موفقیت ذخیره شد.',
                'info' => 'لینک تایید ایمیل برای شما ارسال شد. لطفا ایمیل خود را بررسی کنید.'
            ]);
        }
        return redirect()->back()->with(['success' => 'اطلاعات پروفایل شما با موفقیت ذخیره شد.']);
    }

    public function editPhone()
    {
        $page_title = 'ویرایش شماره موبایل';
        return view('front.dashboard.edit-phone', compact('page_title'));
    }

    public function updatePhone(Request $request)
    {
        $request->validate([
            'phone' => ['required', 'numeric', 'unique:users'],
        ]);

        if (session()->has('verifyUserPhone') &&
            session('verifyUserPhone.created_at')->diffInMinutes(Carbon::now()) > 2) {

            $code = rand(10000, 99999);
            Http::withHeaders([
                'Authorization' => env('FARAZSMS_APIKEY')
            ])->post("http://rest.ippanel.com/v1/messages/patterns/send", [
                'pattern_code' => 'kuy2a5sme7',
                'originator' => env('FARAZSMS_ORIGINATOR'),
                'recipient' => $request->input('phone'),
                'values' => [
                    'code' => (string)$code
                ]
            ]);
            session([
                'verifyUserPhone' => [
                    'phoneNumber' => $request->input('phone'),
                    'verifyCode' => Hash::make($code),
                    'created_at' => Carbon::now()
                ]
            ]);
        }
        return redirect()->route('dashboard.profile.verfiyPhoneView');
    }

    public function verfiyPhoneView()
    {
        if (session()->has('verifyUserPhone') &&
            session('verifyUserPhone.created_at')->diffInMinutes(Carbon::now()) <= 15) {
            $page_title = 'تایید شماره موبایل';
            return view('front.dashboard.verify-phone', compact('page_title'));
        }
        return redirect()->route('dashboard.profile.editPhone');
    }

    public function verifyPhone(Request $request)
    {
        $request->validate([
            'verify_code' => 'required'
        ]);

        if (session()->has('verifyUserPhone') &&
            session('verifyUserPhone.created_at')->diffInMinutes(Carbon::now()) <= 15) {
            if (Hash::check($request->verify_code, session('verifyUserPhone.verifyCode'))) {
                User::where('id', Auth::id())->update([
                    'phone' => session()->get('verifyUserPhone.phoneNumber'),
                    'phone_verified_at' => Carbon::now(),
                ]);
                return redirect()->route('dashboard.profile.index')
                    ->with(['success' => 'شماره موبایل شما با موفقیت ویرایش شد.']);
            } else {
                return back()->with(['error' => 'کد تایید وارد شده مطابقت ندارد.']);
            }
        } else {
            return redirect()->route('dashboard.editPhone')
                ->with(['error' => 'کد تایید منقضی شده است.']);
        }
    }
}
