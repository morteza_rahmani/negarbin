<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Withdraw;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WithdrawController extends Controller
{
    public function index()
    {
        $page_title = 'تسویه حساب';
        $user = Auth::user();
        $withdraw = $user->withdraws()->latest()->first();
        return view('front.dashboard.withdraw', compact('page_title', 'user', 'withdraw'));
    }

    public function store(Request $request): RedirectResponse
    {
        $min_settlement_amount = Setting::first()->min_settlement_amount;
        $outstanding = $request->user()->outstanding;
        // Check Shomare Shaba
        if (empty($request->user()->IBAN)) {
            return back()->with(['info' => "لطفا ابتدا شماره شبا حساب خود را در صفحه پروفایل ثبت کنید."]);
        }
        // Check Minimum Settlement Amount
        if ($outstanding < $min_settlement_amount) {
            $msa = number_format($min_settlement_amount);
            return back()->with(['info' => "مقدار موجودی حساب شما کمتر از حداقل هزینه قابل تسویه ({$msa} تومان) است."]);
        }
        $withdraws_count = Withdraw::where('user_id', $request->user()->id)->where('status', 0)
            ->orWhere('status', 1)->count();
        if ($withdraws_count > 0) {
            return back()->with(['info' => 'شما قبلا یک درخواست تسویه حساب داشته اید! لطفا منتظر بمانید.']);
        }
        try {
            Withdraw::create([
                'user_id' => $request->user()->id,
                'amount' => $request->user()->outstanding
            ]);
        } catch (Exception $exception) {
            return back()->with(['error' => 'خطایی پیش آمده است! لطفا دوباره امتحان کنید.']);
        }
        return back()->with([
            'success' => 'درخواست تسویه حساب با موفقیت ثبت گردید. نتیجه تسویه حساب در همین صفحه قابل مشاهده خواهد بود.'
        ]);
    }
}
