<?php

namespace App\Http\Controllers\Front;

use App\Classes\GenerateSecureLink;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $page_title = 'جستجوی تصاویر';
        if ($request->has('sort')) {
            $products = Product::where('status', 2)->filter($request->all())->paginateFilter(30);
        } else {
            $products = Product::where('status', 2)->orderByDesc('views')->filter($request->all())->paginateFilter(30);
        }
        $categories = Category::whereNull('parent_id')->get();
        return view('front.products.index', compact('page_title', 'products', 'categories'));
    }

    public function show($id)
    {
        $product = Product::where('status', 2)->findOrFail($id);
        $page_title = $product->name;
        $comments = $product->comments()->whereNull('parent_id')->where('status', 1)->latest()->paginate(10);
        $category = $product->categories()->whereNull('parent_id')->first();
        $category_ids = $product->categories()->pluck('id')->toArray();

        $similar_products = new Collection;
        if ($product->tags->count() > 0) {
            foreach ($product->tags as $tag) {
                $cp = $tag->products()->where('status', 2)->whereNotIn('id', [$product->id])->get();
                $similar_products = $similar_products->merge($cp);
            }
        }
        if ($product->categories()->where('depth', 1)->orWhere('depth', 2)->count() > 0) {
            $ids = $product->categories()->whereIn('depth', [1, 2])->pluck('id')->toArray();
            foreach ($ids as $id) {
                $cp = Category::where('id', $id)->first()->products()->where('status', 2)
                    ->whereNotIn('id', [$product->id])->get();
                $similar_products = $similar_products->merge($cp);
            }
        }
        $similar_products = $similar_products->take(30)->sortByDesc('views');
        if (Auth::id() != $product->user_id)
            $product->increment('views');

        return view('front.products.show', compact([
            'page_title', 'product', 'comments', 'category', 'similar_products', 'category_ids'
        ]));
    }

    public function archive(Request $request, $slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $page_title = $category->name;
        if ($request->has('sort')) {
            $products = $category->products()->where('status', 2)->filter($request->all())->paginateFilter(30);
        } else {
            $products = $category->products()->orderByDesc('views')->where('status', 2)->filter($request->all())
                ->paginateFilter(30);
        }
        return view('front.products.index', compact('page_title', 'products', 'category'));
    }

    // Download product file
    public function download(Request $request, Product $product)
    {
        if ($product->price == 0 || (Auth::check() && (Auth::user()->purchased($product->id) || Auth::id() == $product->user_id || Auth::user()->is_admin))) {
            $client_ip = $request->ip();
            $file_url = env('STORAGE_ADDRESS') . env('FILES') . $product->file;
            $expire_time = Carbon::now('UTC')->addDay()->timestamp;
            $secureLink = new GenerateSecureLink(
                $file_url, $expire_time, '/storage/' . env('FILES') . $product->file, $client_ip
            );
            $url = $secureLink->generateSecureLink();
            Product::where('id', $product->id)->increment('downloads');
            return redirect($url);
        } else {
            abort(403, 'شما دسترسی لازم را ندارید');
        }
    }
}
