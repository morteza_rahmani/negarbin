<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Exceptions\PurchaseFailedException;
use Shetabit\Multipay\Invoice as MultipayInvoice;
use Shetabit\Payment\Facade\Payment as MultipayPayment;

class InvoicesController extends Controller
{
    /**
     * Create new payment and redirect to payment page.
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email'],
            'cart_amount' => ['required', 'numeric']
        ]);

        /* Check if shopping cart is changed return back */
        $total_price = 0;
        foreach (Auth::user()->cartProducts as $cartProduct) {
            $total_price += $cartProduct->product->price;
        }
        if ($request->cart_amount != $total_price)
            return redirect()->back()->with(['warning' => 'گویا سبد خرید شما تغییر کرده است! لطفا مجددا بررسی و خرید نمایید.']);

        $invoice = Invoice::create([
            'user_id' => Auth::id(),
            'amount' => $request->cart_amount,
            'email' => $request->email
        ]);


        $cartProducts = Auth::user()->cartProducts;

        foreach ($cartProducts as $cartProduct) {
            $invoice->products()->attach([$cartProduct->product->id => [
                'product_price' => $cartProduct->product->price,
                'product_name' => $cartProduct->product->name,
            ]]);
        }

        return redirect()->route('invoice.pay', [$invoice]);
    }

    /**
     * Pay invoice
     *
     * @param Invoice $invoice
     * @return mixed
     */
    public function pay(Invoice $invoice)
    {
        if ($invoice->hasPaid()) {
            return 'قبلا پرداخت شده است.';
        }

        $payment = $this->createPayment($invoice);
        $callbackUrl = route('payment.verify', [$payment]);
        $multipayInvoice = (new MultipayInvoice)->amount($invoice->amount);

        try {
            // Purchase the given invoice.
            $payment = MultipayPayment::callbackUrl($callbackUrl)
                ->purchase(
                    $multipayInvoice,
                    function ($driver, $transactionId) use ($payment) {
                        $payment->setTransaction($transactionId);
                        $payment->forceFill(['gateway' => 'زرین پال']);
                    }
                );

            return $payment->pay()->render();

        } catch (PurchaseFailedException $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * Create payment log for given invoice.
     *
     * @param Invoice $invoice
     * @return Payment
     */
    protected function createPayment(Invoice $invoice): Payment
    {
        $now = Carbon::now()->format('Y-m-d');
        return $invoice->payments()->create([
            'status' => Payment::STATUS_NEW,
            'jdate' => jdate($now)->format('Y-m-d')
        ]);
    }
}
