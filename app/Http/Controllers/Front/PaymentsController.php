<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Jobs\SendMail;
use App\Mail\OrderPaid;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Shetabit\Payment\Facade\Payment as MultipayPayment;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;

class PaymentsController extends Controller
{
    /**
     * verify payment.
     *
     * @param Payment $payment
     * @return mixed
     */
    public function verify(Payment $payment)
    {
        $invoice = $payment->invoice;

        // You need to verify the payment to ensure the invoice has been paid successfully.
        // We use transaction id to verify payments
        // It is a good practice to add invoice amount as well.
        try {
            $receipt = MultipayPayment::amount($invoice->amount)->transactionId($invoice->transaction)->verify();
            // You can show payment referenceId to the user.
            $payment->setReceipt($receipt->getReferenceId());
            $payment->setStatus(Payment::STATUS_SUCCEED);
            // Increment number of product purchases
            $products_ids = Auth::user()->cartProducts()->pluck('product_id')->toArray();
            Product::whereIn('id', $products_ids)->increment('purchases');
            // Remove products from user's shopping cart
            Auth::user()->cartProducts()->delete();
            // Increment seller revenue
            $setting = Setting::first();
            foreach ($invoice->products as $product) {
                $sellersShare = vendorRevenue($setting->commission, $product->price);
                User::where('id', $product->user_id)->update([
                    'total_revenue' => DB::raw("total_revenue + {$sellersShare}"),
                    'outstanding' => DB::raw("outstanding + {$sellersShare}"),
                ]);
            }
            // Add this products purchased to user's products list
            $user = Auth::user();
            foreach ($invoice->products as $product) {
                $user->purchasedProducts()->attach([$product->id => [
                    'product_price' => $product->price,
                    'product_name' => $product->name
                ]]);
            }

            // Send email for order payment to user
            SendMail::dispatch($invoice->email, $payment);
        } catch (InvalidPaymentException $exception) {
            /**
             * when payment is not verified, it will throw an exception.
             * We can catch the exception to handle invalid payments.
             * getMessage method, returns a suitable message that can be used in user interface.
             **/
            $payment->forceFill(['message' => $exception->getMessage()]);
            $payment->setStatus(Payment::STATUS_FAILED);
        }
        return redirect()->route('receipt', ['transaction' => $payment->transaction]);
    }
}
