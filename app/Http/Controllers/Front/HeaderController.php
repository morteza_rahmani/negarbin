<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HeaderController extends Controller
{
    public function renderHeaderCategories(Request $request, Category $category): JsonResponse
    {
        if ($request->ajax()) {
            $view = view('front.layouts.header-category-items', compact('category'))->render();
            return response()->json(['html' => $view]);
        }
        return response()->json(['error' => 'You have not right access!'], 422);
    }
}
