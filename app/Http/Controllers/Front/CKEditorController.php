<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\ContentImage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class CKEditorController extends Controller
{
    public function upload(Request $request): JsonResponse
    {
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            $img = Image::make($file);
            $img->height() > 1920 ? $img->resize(1920, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 80) : $img->encode('jpg', 80);
            $uniqueID = uniqid();
            $userID = Auth::id();
            $str = Str::random(4);
            $fileName = "u{$userID}-{$uniqueID}-{$str}.jpg";
            Storage::disk('ftp')->put(env('PRODUCTS_CONTENT') . $fileName, $img);

            $url = env('STORAGE_ADDRESS') . env('PRODUCTS_CONTENT') . $fileName;

            // Add to content_images table for manage unused media
            ContentImage::create([
                "fileName" => $fileName,
                "url" => $url
            ]);
        }

        return response()->json(['url' => $url]);
    }
}
