<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $page_title = 'لیست مطالب';
        $query = Article::query();
        if ($request->has('s')) {
            $query->where('title', 'LIKE', "%{$request->input('s')}%")
                ->orWhere('name', 'LIKE', "%{$request->input('s')}%")
                ->orWhere('content', 'LIKE', "%{$request->input('s')}%")
                ->orWhere('tags', 'LIKE', "%{$request->input('s')}%");
        }
        $articles = $query->orderByDesc('updated_at')->paginate(11);
        $best_articles = Article::orderBy('views', 'desc')->take(5)->get();
        return view('front.blog.index', compact('page_title', 'articles', 'best_articles'));
    }

    public function show($slug)
    {
        $article = Article::where('slug', $slug)->firstOrFail();
        $page_title = $article->title ?? $article->name;
        $latest_articles = Article::orderByDesc('updated_at')->take(6)->get();
        $comments = $article->comments()->whereNull('parent_id')->where('status', 1)->latest()->paginate(10);
        return view('front.blog.single', compact('page_title', 'article', 'latest_articles', 'comments'));
    }
}
