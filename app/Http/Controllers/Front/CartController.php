<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Payment;
use App\Models\Product;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index()
    {
        $page_title = 'سبد خرید';
        $products = Auth::user()->cartProducts;
        $total_price = 0;
        return view('front.cart', compact('page_title', 'products', 'total_price'));
    }

    public function addToCart(Request $request, Product $product): RedirectResponse
    {
        if (Auth::user()->cartProducts()->where('product_id', $product->id)->count() > 0) {
            return back()->with(['info' => 'این محصول در سبد خرید شما موجود است.']);
        }
        try {
            Cart::create([
                'product_id' => $product->id,
                'user_id' => Auth::id()
            ]);
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی به وجود آمده است! لطفا بعدا دوباره امتحان کنید.']);
        }
        return back()->with(['success' => 'محصول با موفقیت به سبد خرید شما اضافه شد.']);
    }

    public function destroy($id): RedirectResponse
    {
        $cartProduct = Auth::user()->cartProducts()->where('product_id', $id)->firstOrFail();
        try {
            $cartProduct->delete();
        } catch (Exception $exception) {
            return back()->with(['error' => 'مشکلی به وجود آمده است! لطفا بعدا دوباره امتحان کنید.']);
        }
        return back()->with(['success' => 'محصول مورد نظر از سبد خرید شما حذف گردید.']);
    }

    public function receipt(Request $request)
    {
        if (!$request->has('transaction')) {
            return back()->with(['info' => 'شما دسترسی لازم به این صفحه را ندارید!']);
        }
        $payment = Payment::where('transaction', $request->input('transaction'))->firstOrFail();
        if ($payment->invoice->user_id != Auth::id()) {
            return back()->with(['info' => 'شما دسترسی لازم به این صفحه را ندارید!']);
        }
        $page_title = 'رسید تراکنش';
        return view('front.receipt', compact('payment', 'page_title'));
    }
}
