<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request): RedirectResponse
    {
        if (auth()->check()) {
            $request->validate([
                'comment' => 'required',
                'comment_post_ID' => 'required'
            ]);
        } else {
            $request->validate([
                'comment' => 'required',
                'comment_post_ID' => 'required',
                'author' => 'required',
                'email' => 'required'
            ]);
        }

        $comment = new Comment();
        $comment->content = $request->input('comment');

        $setScore = false;

        if ($request->input('comment_parent') == 0) {
            if ($request->has('rating') && $request->input('rating') != 0) {
                $comment->score = $request->input('rating');
                $setScore = true;
            }
        } else {
            $comment->parent_id = $request->input('comment_parent');
        }

        if (auth()->check()) {
            $comment->user_id = auth()->id();
            if (auth()->user()->is_admin) {
                $comment->status = 1;
            } else $comment->status = 2;
        } else {
            $comment->status = 2;
            $comment->name = $request->input('author');
            $comment->email = $request->input('email');
        }

        if ($request->input('comment_post_type') == 'article') {
            $post = Article::findOrFail($request->input('comment_post_ID'));
        } elseif ($request->input('comment_post_type') == 'product') {
            $post = Product::findOrFail($request->input('comment_post_ID'));
        } else {
            return redirect()->to(url()->previous() . '#respond')->with('error', 'مشکلی پیش آمده است! لطفا دوباره امتحان کنید.');
        }
        try {
            $post->comments()->save($comment);
            if ($setScore) {
                $commentsWithScore = $post->comments()->whereNull('parent_id')->where('status', 1);
                $avgScore = (int)round($commentsWithScore->avg('score'));
                $votes = $commentsWithScore->count();
                $post->update([
                    'score' => $avgScore,
                    'votes' => $votes
                ]);
            }
        } catch (\Exception $exception) {
            $message = "مشکلی پیش آمده است! لطفا اندکی بعد دوباره امتحان کنید.";
            return redirect()->to(url()->previous() . '#respond')->with('error', $message);
        }
        $message = "از اینکه نظر خود را با ما در میان گذاشتید متشکریم. پس از تایید نمایش داده خواهد شد.";
        return redirect()->to(url()->previous() . '#respond')->with('success', $message);
    }
}
