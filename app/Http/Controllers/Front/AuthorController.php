<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function show(Request $request, $username)
    {
        $user = User::where('username', $username)->firstOrFail();
        $page_title = $user->full_name;
        $products = $user->products()->where('status', 2)->filter($request->all())->paginateFilter(30);
        $productsCount = $user->products()->where('status', 2)->count();
        return view('front.author', compact('page_title', 'user', 'products', 'productsCount'));
    }
}
