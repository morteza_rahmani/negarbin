<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Token;
use App\Models\User;
use App\Models\UserVerify;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    const EXPIRATION_TIME = 15; // minutes

    /**
     * Send login view to client using ajax
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function loginView(Request $request)
    {
        $page = 'login';
        if ($request->ajax()) {
            $view = view('front.layouts.modal-login', compact('page'))->render();
            return response()->json(['html' => $view]);
        }
        return redirect()->route('home');
    }

    /**
     * Send register view to client using ajax
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function registerView(Request $request)
    {
        $page = 'register';
        if ($request->ajax()) {
            $view = view('front.layouts.modal-login', compact('page'))->render();
            return response()->json(['html' => $view]);
        }
        return redirect()->route('home');
    }

    /**
     * Login user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function doLogin(Request $request): JsonResponse
    {
        $request->validate([
            'login' => 'required',
            'password' => 'required',
        ]);
        $login = $request->get('login');
        $pass = $request->get('password');
        if (is_numeric($login)) {
            if (Auth::attempt(['phone' => $login, 'password' => $pass], true)) {
                return response()->json(['success' => 'ورود با موفقیت انجام شد.']);
            }
            return response()->json(['errors' => 'شماره تلفن یا رمز عبور وارد شده مطابقت ندارد.'], 422);
        } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
            if (Auth::attempt(['email' => $login, 'password' => $pass], true)) {
                return response()->json(['success' => 'ورود با موفقیت انجام شد.']);
            }
            return response()->json(['errors' => 'آدرس ایمیل یا رمز عبور وارد شده مطابقت ندارد.'], 422);
        }
        return response()->json(['errors' => 'اطلاعات وارد شده نادرست است.'], 422);
    }

    /**
     * Register user using phone number
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendVerifyCode(Request $request): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required|numeric|digits:11|starts_with:0|unique:users',
        ], [
            'starts_with' => 'شماره موبایل باید با 0 شروع شود.'
        ]);
        $code = rand(10000, 99999);
        Http::withHeaders([
            'Authorization' => env('FARAZSMS_APIKEY')
        ])->post("http://rest.ippanel.com/v1/messages/patterns/send", [
            'pattern_code' => 'kuy2a5sme7',
            'originator' => env('FARAZSMS_ORIGINATOR'),
            'recipient' => $request->input('phone'),
            'values' => [
                'code' => (string)$code
            ]
        ]);
        session([
            'verifyUserPhone' => [
                'phoneNumber' => $request->input('phone'),
                'verifyCode' => Hash::make($code),
                'created_at' => Carbon::now()
            ]
        ]);
        $page = 'verify';
        $view = view('front.layouts.modal-login', compact('page'))->render();
        return response()->json(['html' => $view, 'phone' => $request->input('phone')]);
    }

    /**
     * Verify user phone number
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function doVerify(Request $request): JsonResponse
    {
        $request->validate([
            'verify_code' => 'required'
        ]);

        $page = 'password';

        if (session()->has('verifyUserPhone') &&
            session('verifyUserPhone.created_at')->diffInMinutes(Carbon::now()) <= static::EXPIRATION_TIME) {
            if (Hash::check($request->verify_code, session('verifyUserPhone.verifyCode'))) {
                $view = view('front.layouts.modal-login', compact('page'))->render();
                return response()->json(['html' => $view, 'phone' => session('verifyUserPhone.phoneNumber')]);
            } else {
                return response()->json(['errors' => 'کد تایید وارد شده مطابقت ندارد.'], 422);
            }
        } else {
            return response()->json(['errors' => 'کد تایید منقضی شده است.'], 422);
        }
    }

    /**
     * Register new user
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function doRegister(Request $request)
    {
        $request->validate([
            'password' => ['required', 'confirmed', Password::min(8)],
        ]);

        if (!session()->has('verifyUserPhone.phoneNumber')) {
            $page = 'register';
            if ($request->ajax()) {
                $view = view('front.layouts.modal-login', compact('page'))->render();
                return response()->json(['html' => $view]);
            } else {
                return redirect()->route('home');
            }
        }

        $user = User::create([
            'first_name' => 'کاربر ' . uniqid(),
            'username' => 'user-' . uniqid(),
            'phone' => session()->get('verifyUserPhone.phoneNumber'),
            'password' => Hash::make($request->password),
            'phone_verified_at' => Carbon::now(),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return response()->json(['success' => 'خوش آمدید! ثبت نام شما با موفقیت انجام شد.']);
    }

    public function changePasswordView(Request $request)
    {
        $page = 'change_password_1';
        if ($request->ajax()) {
            $view = view('front.layouts.modal-login', compact('page'))->render();
            return response()->json(['html' => $view]);
        }
        return redirect()->route('home');
    }

    /**
     * Send verification code to user for verifying and changing password
     *
     * @throws ValidationException
     */
    public function changePasswordStep1(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|exists:users',
        ]);

        $user = User::where('phone', $request->input('phone'))->first();

        $token = Token::create([
            'user_id' => $user->id
        ]);

        if ($token->sendCode()) {
            session()->put("code_id", $token->id);
            session()->put("user_id", $user->id);
            $page = 'change_password_2';
            if ($request->ajax()) {
                $view = view('front.layouts.modal-login', compact('page'))->render();
                return response()->json(['html' => $view, 'phone' => $request->phone]);
            } else {
                return redirect()->route('home');
            }
        }

        $token->delete();
        return response()->json(['errors' => 'ارسال کد تایید با مشکل مواجه شد! لطفا دوباره امتحان کنید.']);
    }

    /**
     * Verify user phone number
     *
     * @throws ValidationException
     */
    public function changePasswordStep2(Request $request)
    {
        $this->validate($request, [
            'verify_code' => 'required'
        ]);

        if (!session()->has('code_id') || !session()->has('user_id')) {
            $page = 'change_password_1';
            if ($request->ajax()) {
                $view = view('front.layouts.modal-login', compact('page'))->render();
                return response()->json(['html' => $view]);
            } else {
                return redirect()->route('home');
            }
        }

        $token = Token::where('user_id', session()->get('user_id'))->find(session()->get('code_id'));

        if (!$token || empty($token->id)) {
            $page = 'change_password_1';
            if ($request->ajax()) {
                $view = view('front.layouts.modal-login', compact('page'))->render();
                return response()->json(['html' => $view]);
            } else {
                return redirect()->route('home');
            }
        }

        if (!$token->isValid())
            return response()->json(['errors' => 'کد تایید منقضی یا قبلا استفاده شده است.'], 422);

        if ($token->code !== $request->input('verify_code'))
            return response()->json(['errors' => 'کد تایید وارد شده مطابقت ندارد.'], 422);

        $token->update([
            'used' => true
        ]);

        $page = 'change_password_3';
        if ($request->ajax()) {
            $view = view('front.layouts.modal-login', compact('page'))->render();
            return response()->json(['html' => $view]);
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Change user password
     *
     * @throws ValidationException
     */
    public function changePasswordStep3(Request $request)
    {
        $this->validate($request, [
            'password' => ['required', 'confirmed', Password::min(8)],
        ]);

        if (!session()->has('user_id')) {
            $page = 'change_password_1';
            if ($request->ajax()) {
                $view = view('front.layouts.modal-login', compact('page'))->render();
                return response()->json(['html' => $view]);
            } else {
                return redirect()->route('home');
            }
        }

        $user = User::find(session()->get('user_id'));
        try {
            $user->update(['password' => Hash::make($request->password)]);
            Auth::login($user);
        } catch (\Exception $exception) {
            return response()->json(['errors' => 'خطایی پیش آمده است. لطفا دوباره امتحان کنید.']);
        }
        return response()->json(['success' => 'رمز عبور شما با موفقیت بازیابی شد.']);
    }

    public function verifyEmail($token): RedirectResponse
    {
        $verifyUser = UserVerify::where('token', $token)->first();

        $appName = env('APP_NAME');
        $info = "متاسفانه $appName ایمیل شما را نمیشناسد.";

        if (!is_null($verifyUser)) {
            $user = $verifyUser->user;

            if (!$user->is_email_verified) {

                $verifyUser->user->is_email_verified = 1;
                $verifyUser->user->email_verified_at = Carbon::now()->format('Y-m-d H:i:s');
                $verifyUser->user->save();

                UserVerify::where('user_id', $user->id)->delete();

                $info = "ایمیل شما با موفقیت تایید شد.";
            } else {
                $info = "ایمیل شما قبلا تایید شده است.";
            }

            if (!Auth::check()) {
                Auth::login($user->id);
            }

            return redirect()->route('dashboard.profile.index')->with('info', $info);
        }

        return redirect()->route('emptyPage')->with('warning', $info);
    }
}
