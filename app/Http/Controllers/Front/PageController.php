<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();
        $page_title = $page->title ?? $page->name;
        return view('front.page', compact('page_title', 'page'));
    }

    public function emptyPage()
    {
        $page_title = 'برگه';
        return view('front.empty', compact('page_title'));
    }
}
