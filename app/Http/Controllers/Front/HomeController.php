<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Product;
use App\Models\Search;
use App\Models\Setting;
use App\Models\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $page_title = 'صفحه اصلی';
        $banners = Banner::first();
        $searches = Search::first();
        $last_articles = Article::orderByDesc('updated_at')->take(4)->get();
        $tag_id = Setting::first()->tag_id;
        if (isset($tag_id)) {
            $is_tag = true;
            $products = Tag::find($tag_id)->products()->where('status', 2)->latest('updated_at')->limit(30)->get();
        }
        if (!isset($tag_id) || count($products) == 0) {
            $is_tag = false;
            $products = Product::where('status', 2)->latest('updated_at')->limit(30)->get();
        }
        return view('front.index', compact([
            'page_title', 'last_articles', 'banners', 'searches', 'products', 'is_tag'
        ]));
    }
}
