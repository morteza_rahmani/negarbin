<?php

if (!function_exists('abbreviateNumber')) {
    function abbreviateNumber($num): string
    {
        if ($num >= 0 && $num < 1000) {
            $format = round($num);
            $suffix = '';
        }
        else if ($num >= 1000 && $num < 1000000) {
            $format = round($num / 1000, 1);
            $suffix = 'K';
        }
        else if ($num >= 1000000 && $num < 1000000000) {
            $format = round($num / 1000000, 1);
            $suffix = 'M';
        }
        else if ($num >= 1000000000 && $num < 1000000000000) {
            $format = round($num / 1000000000, 1);
            $suffix = 'B';
        }
        else if ($num >= 1000000000000) {
            $format = round($num / 1000000000000, 1);
            $suffix = 'T';
        }
        return !empty($format . $suffix) ? $format . $suffix : 0;
    }
}

if (!function_exists('vendorRevenue')) {
    function vendorRevenue($commission, $amount): string
    {
        return round($amount * ((100 - $commission) / 100));
    }
}

if (!function_exists('checkRemoteFile')) {
    function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        if ($result !== FALSE) {
            return true;
        } else {
            return false;
        }
    }
}
