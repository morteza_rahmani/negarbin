<?php

namespace App\ModelFilters;

use App\Models\Category;
use EloquentFilter\ModelFilter;

class ProductFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function s($s)
    {
        return $this->whereLike('name', $s)->whereLike('summary', $s, 'or');
    }

    public function sort($sort)
    {
        if ($sort == 'sale') {
            return $this->orderByDesc('downloads');
        }
        if ($sort == 'new') {
            return $this->orderByDesc('updated_at');
        }
        return $this->orderByDesc('views');
    }

    public function category($ids)
    {
        if (in_array(-1, $ids)) {
            return $this;
        }
        $children_ids = Category::whereIn('parent_id', $ids)->pluck('id')->toArray();
        return $this->whereHas('categories', function($query) use ($ids, $children_ids)
        {
            return $query->whereIn('id', $ids)->orWhereIn('parent_id', $ids)->orWhereIn('parent_id', $children_ids);
        });
    }

    public function filterby($value)
    {
        if ($value == 'free') {
            return $this->where('price', 0);
        }
        if ($value == 'pricing') {
            return $this->where('price', '>',  0);
        }
        return $this;
    }
}
