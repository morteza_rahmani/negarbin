<?php

namespace App\Console\Commands;

use App\Models\ContentImage;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class RemoveUnusedImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unusedimages:remove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove unused images from product contents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $images = ContentImage::where('used', false)
            ->where('created_at', '<', Carbon::now()->subHours(3)->format("Y-m-d H:i:s"))->get();
        foreach ($images as $image) {
            $address = env('PRODUCTS_CONTENT') . $image->fileName;
            if (Storage::disk('ftp')->exists($address)) {
                Storage::disk('ftp')->delete($address);
            }
            ContentImage::where('id', $image->id)->delete();
        }
    }
}
